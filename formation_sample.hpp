#ifndef SOCCER_REGISTER_FORMATION_SAMPLE_HPP
#define SOCCER_REGISTER_FORMATION_SAMPLE_HPP

#include <soccer/register/object.hpp>

namespace soccer
{
    struct formation_sample_tag { };
}

#ifndef DOXYGEN_NO_SPECIALIZATIONS

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_TAG_TRAITS(FormationSample) \
    SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(FormationSample, soccer::formation_sample_tag)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_BALL_TRAITS(FormationSample, BallType) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(ball_type, FormationSample, BallType)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_TRAITS(FormationSample, PlayerType) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(player_type, FormationSample, PlayerType)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ID_TRAITS(FormationSample, PlayerIdType) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(player_id_type, FormationSample, PlayerIdType)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ITERATOR_TRAITS(FormationSample, PlayerIteratorType) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(player_iterator_type, FormationSample, PlayerIteratorType)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_INFO_TRAITS(FormationSample, InfoType) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(info_type, FormationSample, InfoType)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_TRAITS(FormationSample, BallType, \
                                                         PlayerIdType, PlayerIteratorType, PlayerType, \
                                                         InfoType) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_TAG_TRAITS(FormationSample) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_BALL_TRAITS(FormationSample, BallType) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_TRAITS(FormationSample, PlayerType) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ID_TRAITS(FormationSample, PlayerIdType) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ITERATOR_TRAITS(FormationSample, PlayerIteratorType) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_INFO_TRAITS(FormationSample, InfoType)

#endif // DOXYGEN_NO_SPECIALIZATIONS


//
// BALL TYPE TRAITS
//
#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_BALL_ACCESS_METHOD_GET_SET(FormationSample, Type, Get, Set) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(ball_access, FormationSample, Type, Get, Set)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_BALL_ACCESS_FREE_GET_SET(FormationSample, Type, Get, Set) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(ball_access, FormationSample, Type, Get, Set)

//
// PLAYER ID ITERATOR TRAITS
//
#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ITERATORS_ACCESS_METHOD_GET(FormationSample, PlayerIteratorType, Get) \
    template<> struct player_iterators_access<FormationSample> \
    { \
        static inline std::pair< PlayerIteratorType, PlayerIteratorType > \
        get(FormationSample const& obj) { return  obj.Get(); } \
    };

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ITERATORS_ACCESS_FREE_GET(FormationSample, PlayerIteratorType, Get) \
    template<> struct player_iterators_access<FormationSample> \
    { \
        static inline std::pair< PlayerIteratorType, PlayerIteratorType > \
        get(FormationSample const& obj) { return Get( obj ); } \
    };

//
// PLAYER TYPE TRAITS
//

// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ACCESS_METHOD_GET_SET(FormationSample, PlayerIdType, PlayerType, Get, Set) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET_WITH_KEY(player_access, FormationSample, \
                                                                          PlayerIdType, PlayerType, Get, Set)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ACCESS_FREE_GET_SET(FormationSample, PlayerIdType, PlayerType, Get, Set) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET_WITH_KEY(player_access, FormationSample, \
                                                                        PlayerIdType, PlayerType, Get, Set)
//
// INFO TYPE TRAITS
//

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_INFO_ACCESS_METHOD_GET_SET(FormationSample, Type, Get, Set) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(info_access, FormationSample, Type, Get, Set)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_INFO_ACCESS_FREE_GET_SET(FormationSample, Type, Get, Set) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(info_access, FormationSample, Type, Get, Set)

//
// FORMATION SAMPLE REGISTRATION
//

#define SOCCER_REGISTER_FORMATION_SAMPLE_GET_SET(FormationSample, \
                                                 BallType, GetBall, SetBall, \
                                                 PlayerIdType, PlayerIteratorType, GetPlayerIds, \
                                                 PlayerType, GetPlayer, SetPlayer, \
                                                 InfoType, GetInfo, SetInfo) \
namespace soccer { namespace traits { \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_TRAITS(FormationSample, BallType, \
                                                     PlayerIdType, PlayerIteratorType, PlayerType, InfoType) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_BALL_ACCESS_METHOD_GET_SET(FormationSample, BallType, GetBall, SetBall) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ACCESS_METHOD_GET_SET(FormationSample, PlayerIdType, PlayerType, GetPlayer, SetPlayer) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ITERATORS_ACCESS_METHOD_GET(FormationSample, PlayerIteratorType, GetPlayerIds) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_INFO_ACCESS_METHOD_GET_SET(FormationSample, InfoType, GetInfo, SetInfo) \
}}


#define SOCCER_REGISTER_FORMATION_SAMPLE_GET_SET_FREE(FormationSample, \
                                                      BallType, GetBall, SetBall, \
                                                      PlayerIdType, PlayerIteratorType, GetPlayerIds, \
                                                      PlayerType, GetPlayer, SetPlayer, \
                                                      InfoType, GetInfo, SetInfo) \
namespace soccer { namespace traits { \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_TRAITS(FormationSample, BallType, \
                                                     PlayerIdType, PlayerIteratorType, PlayerType, InfoType) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_BALL_ACCESS_FREE_GET_SET(FormationSample, BallType, GetBall, SetBall) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ACCESS_FREE_GET_SET(FormationSample, PlayerIdType, PlayerType, GetPlayer, SetPlayer) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_PLAYER_ITERATORS_ACCESS_FREE_GET(FormationSample, PlayerIteratorType, GetPlayerIds) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_INFO_ACCESS_FREE_GET_SET(FormationSample, InfoType, GetInfo, SetInfo) \
}}

// #define SOCCER_REGISTER_FORMATION_SAMPLE_GET_SET_FREE()


#endif // SOCCER_REGISTER_FORMATION_SAMPLE_HPP
