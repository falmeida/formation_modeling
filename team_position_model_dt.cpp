// -*-c++-*-

/*!
  \file team_position_model_dt.cpp
  \brief team position modeling based on Delaunay Triangulation.
*/

/*
 *Copyright:

 Copyright (C) Fernando ALMEIDA

 This code is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

// Boost geometry Point2D adaptors
#include <soccer/stl/pair_as_point.hpp>

#include <soccer/CGAL/triangulation.hpp>
#include <soccer/rcsc/vector_2d.h>

#include "modeling_parameters.h"

#include <soccer/rcsslogplayer/rcsslogplayer_disp_info.h>
// #include <soccer/rcsslogplayer/types.h>
#include <soccer/rcsc/rcsc_formation_sample_dataset_adaptor.hpp>

#include "rcsslogplayer/rcg_position_modeler.h"

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "formation_sample_storage.hpp"
#include "formation_sample_storage_policy.hpp"
#include "formation_sample_storage_algorithms.hpp"

#include "modeling_strategies.h"

#include "formation_sample_evaluation.hpp"
#include "formation_sample_similarity.hpp"

#include "formation_sample_generator.hpp"
#include "formation_sample_storage_debug.hpp"

#include "formation_classifier.hpp"

#include "formation_state_selector.hpp"
#include "sample_matching.h"

#include "formation_sample_transform.hpp"
#include "formation_sample_storage_transform.hpp"

#include "formation_sample_writer.hpp"

#include "state_processors.hpp"

#include <soccer/algorithms/pitch_algorithms.hpp>

#include <rcsslogplayer/parser.h>

// librc2dlogfilter
#include <robocup/2d/game_log_finder.h>

#include <boost/regex.hpp>
#include <boost/tuple/tuple.hpp>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <cstring>
#include <cstdlib>



std::ostream& operator<<( std::ostream& os,
                          const std::pair< robocup::soccer::GameLog, rcss::rcg::DispInfoT >& info )
{
    os << info.first.path() << ",";
    os << info.second.show_.time_;
    return os;
}



/*!
 * \brief Convert rcss_log_player_id to formation role id
 */
struct rcss_state_pid_to_role_id
    : public std::unary_function< int, unsigned int > {

    unsigned int operator()( const int pid )
    {
        // left_players pids=[0..10] right_players_pids=[11..21]
        // Players from different teams are mapped to roles 0..10
        return pid % 11;
    }
};


#include <soccer/core/access.hpp>
#include "Core/access.hpp"

// Setup types for type inference
typedef typename rcss::rcg::RcgHandler::state_type state_type;
typedef typename soccer::side_type< typename soccer::player_type< state_type >::type >::type side_t;
typedef typename rcss::rcg::RcgHandler::soccer_model_type soccer_model_type;
typedef typename soccer::formation_sample_generator< state_type >::type formation_sample_type;

typedef std::map< typename soccer::player_id_type< state_type >::type,
                  typename soccer::player_id_type< state_type >::type
                 > player_id_map_type;

typedef typename formation_sample_type::ball_t formation_sample_ball_type;
typedef typename formation_sample_type::player_t formation_sample_player_type;
typedef typename formation_sample_type::player_id_t formation_sample_player_id_type;
typedef typename formation_sample_type::player_iterator_t formation_sample_player_iterator_type;
typedef typename formation_sample_type::info_type formation_sample_info_type;

/* SOCCER_REGISTER_FORMATION_SAMPLE_GET_SET( formation_sample_type, \
                                          formation_sample_ball_type, get_ball, set_ball, \
                                          formation_sample_player_id_type, formation_sample_player_iterator_type, get_players, \
                                          formation_sample_player_type, get_player, set_player, \
                                          formation_sample_info_type, get_info, set_info ) */

///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////



typedef typename soccer::CGAL::triangulation_formation_sample_storage_generator< formation_sample_type, float >::type
        formation_sample_storage_type;

typedef soccer::formation_sample_storage_policy< formation_sample_type,
                                                 std::function< float( const formation_sample_type& ) >,
                                                 formation_sample_storage_type
                                               >
        formation_sample_storage_policy_type;

typedef typename soccer::formation_modeler_generator< state_type,
                                                      formation_sample_storage_type
                                                    >::type
        formation_modeler_type;

// using namespace soccer;

int
main( int argc, char** argv )
{
    soccer::modeling::modeling_parameters _modeling_params;
    if ( ! ( _modeling_params.parse( MODELING_CONFIG ) ||
             _modeling_params.parse( argc, argv ) ) )
    {
        return -1;
    }

    // Display chosen modeling parameters
    std::clog << "CHOSEN MODELING PARAMETERS" << std::endl;
    std::clog << _modeling_params << std::endl;


    std::vector< robocup::soccer::GameLog* > _game_logs;
    auto _game_log_inserter = std::back_inserter( _game_logs );
    for ( auto it = _modeling_params.logFileSearchPaths().begin();
          it != _modeling_params.logFileSearchPaths().end(); it++ )
    {
        robocup::soccer::GameLogFinder::getLogFiles( _modeling_params.logfileNameRegex().c_str(),
                                                     it->c_str(),
                                                     _game_log_inserter,
                                                     _modeling_params.recursiveLookup() );
    }

    std::cout << "Log files found " << _game_logs.size() << std::endl;

    // create rcg handler instance
    rcss::rcg::RcgHandler handler;
    rcss::rcg::Parser rcg_parser( handler );


    boost::regex _teamname_regex( _modeling_params.teamNameRegex() );

    soccer::state_preprocessor< state_type, rcss::rcg::ServerParamT > _state_preprocessor;
    _state_preprocessor.set_use_vertical_symmetry( true );

    // Create formation sample storage
    formation_sample_storage_type _formation_sample_storage;
    formation_sample_storage_policy_type
            _formation_sample_storage_policy( _formation_sample_storage,
                                              soccer::formation_sample_adjacent_player_ball_dist_significance_evaluator<formation_sample_type>( 10.0f ),
                                              rcsc::formation::RCSC_FORMATION_SAMPLE_MAX_DIST2_THR );

    formation_modeler_type _formation_modeler;
    _formation_modeler.set_formation_sample_storage( _formation_sample_storage_policy );
    _formation_modeler.set_formation_sample_significant_predicate( [] ( const formation_sample_type& ) { return true; } );

    // soccer::formation_description1< typename soccer::player_id_type< state_type >::type > _last_formation;
    soccer::formation_triangulation_classifier< state_type > _formation_classifier;
    handler.registerObserver( [&_formation_modeler] ( state_type state ) { _formation_modeler(state); } );
    handler.registerObserver( [&_formation_classifier] ( state_type state )
                {
                    /*const auto _formation_description = _formation_classifier( state );
                    std::cout << "Time=" << soccer::get_time( state ) << " "
                              << _formation_classifier.M_side
                              << " Formation " << _formation_description << std::endl;*/
                }
            );

    // Analyse each game
    for ( auto itgl = _game_logs.begin(); itgl != _game_logs.end(); itgl++ )
    {

        if ( !handler.openLogFile( (*itgl)->path() ) )
        {
            std::cerr << "Could not open game log file " << (*itgl)->path() << std::endl;
            continue;
        }

        std::cout << "ANALYSING game log " << (*itgl)->path() << std::endl;
        std::istream* log_is = handler.getLogFileStream();

        // Determine relevant team side from which to retrieve samples
        auto _team_side = boost::regex_search( (*itgl)->teamLeft(),
                                               _teamname_regex )
                            ? soccer::get_left_side< side_t >()
                            : boost::regex_search( (*itgl)->teamRight(),
                                                           _teamname_regex )
                                  ? soccer::get_right_side< side_t >()
                                  : soccer::get_unknown_side< side_t >();

        if ( _team_side == soccer::get_unknown_side< side_t >() )
        {
            std::cerr << "TeamSide not found. Ignoring game log " << **itgl << std::endl;
            continue;
        }

        _formation_classifier.setSide( _team_side );
        // auto _opponent_side = soccer::opposite_side( _relevant_team_side );

        auto _state_selector_predicate
                = soccer::formation_state_selector_factory::createDefenseSampleSelector( _team_side,
                                                                                         handler.serverParams(),
                                                                                         handler.playerTypes() );
        // TODO Hide the storage to be used in the storage policy

        _state_preprocessor.set_team_side( _team_side );

        _formation_modeler.set_state_preprocessor( _state_preprocessor );
        _formation_modeler.set_state_selector_predicate( _state_selector_predicate );
        _formation_modeler.set_formation_sample_creator( [&] ( state_type const& _state )
                                                        {
                                                            typename soccer::player_filter_iterator_generator< state_type >::type _player_begin, _player_end;
                                                            boost::tie( _player_begin, _player_end ) = soccer::make_teammate_iterator( soccer::get_left_side< side_t >(), _state );
                                                            for ( auto itp = _player_begin; itp != _player_end; itp++ )
                                                            {
                                                                const auto _player = get_player( *itp, _state );
                                                                assert( get_side( _player ) == soccer::get_left_side< side_t >() );
                                                            }
                                                            auto _new_sample = soccer::create_formation_sample<formation_sample_type>( rcss_state_pid_to_role_id(),
                                                                                                                                       _player_begin,
                                                                                                                                       _player_end,
                                                                                                                                       _state );
                                                            auto _pinfo = typename formation_sample_type::info_type( **itgl, _state );
                                                            soccer::set_info( _pinfo, _new_sample );
                                                            return _new_sample;
                                                        } );
        // _formation_modeler.set_formation_sample_significant_predicate( _formation_sample_evaluator );

        // Parse the game log
        while ( rcg_parser.parse( *log_is ) );
    }

    auto _num_samples = soccer::num_samples( _formation_sample_storage );
    std::cout << " END GAME LOG ANALYSYS "
              << " samples=" << _num_samples
              << std::endl;

    if ( _num_samples < 10 )
    {
        std::cerr << "Not enough samples="
                  << _num_samples
                  << std::endl;
        return -1;
    }


    // BEGIN ARTIFICIAL SAMPLE GENERATION
    std::clog << "BEGIN ARTIFICIAL SAMPLE GENERATION" << std::endl;

    const auto& _pitch = handler.serverParams();
    typedef typename soccer::position_type< soccer::ball_type< state_type >::type >::type ball_position_t;
    std::vector< ball_position_t > artificial_ball_positions;
    artificial_ball_positions.push_back( soccer::get_pitch_top_left_position< ball_position_t >( _pitch ) );
    artificial_ball_positions.push_back( soccer::get_pitch_top_right_position< ball_position_t >( _pitch ) );
    artificial_ball_positions.push_back( soccer::get_left_goal_center_position< ball_position_t >( _pitch ) );
    artificial_ball_positions.push_back( soccer::get_right_goal_center_position< ball_position_t >( _pitch ) );
    // artificial_ball_positions.push_back( soccer::get_pitch_bottom_left_position< ball_position_t >( _pitch ) );
    // artificial_ball_positions.push_back( soccer::get_pitch_bottom_right_position< ball_position_t >( _pitch ) );
    for ( auto it = artificial_ball_positions.begin(); it != artificial_ball_positions.end(); it++ )
    {
        auto _nearest_sample_id = soccer::nearest_sample_id( *it, _formation_sample_storage );
        const auto _nearest_sample = soccer::get_sample( _nearest_sample_id, _formation_sample_storage );
        const auto _nearest_sample_dist = soccer::distance( *it, soccer::get_position( soccer::get_ball( _nearest_sample )));
        formation_sample_type _artificial_sample;
        soccer::generate_sample_from_sample( *it,
                                             _pitch,
                                             _nearest_sample,
                                             _artificial_sample );
        std::cout << "Sample generated for position(" << soccer::get_x( *it ) << "," << soccer::get_y( *it ) << ") "
                     " nearest_sample id=" << _nearest_sample_id << " dist=" << _nearest_sample_dist << ")="
                  << _nearest_sample << std::endl;
        _formation_sample_storage_policy( _artificial_sample );
    }

    /* auto _top_right_corner = std::make_pair( soccer::model::pitch_top_left_x( _soccer_model + soccer::model::pitch_length( handler.serverParams() ) ),
                                             soccer::model::pitch_top_left_y( _soccer_model ));
    auto _sample_nearest_top_right_corner = soccer::nearest_sample_id( _top_left_corner, _formation_sample_storage );
    const auto _top_right_corner_dist = std::sqrt( std::pow( soccer::get_x( _top_right_corner ) - soccer::get_x( soccer::get_ball( _sample_nearest_top_right_corner )), 2 ) +
                                                   std::pow( soccer::get_y( _top_right_corner ) - soccer::get_y( soccer::get_ball( _sample_nearest_top_right_corner )), 2 )); */


    // TODO Check if distance to top left corner is negligible or can be derived. If so, do not generate a new sample

    std::clog << "END ARTIFICIAL SAMPLE GENERATION" << std::endl;
    // END ARTIFICIAL SAMPLE GENERATION



    // DEBUG Formation Model Graph
    auto _formation_sample_evaluator_fn = soccer::formation_sample_max_speed_evaluator< formation_sample_type>( handler.serverParams().ball_speed_max_,
                                                                                                      handler.serverParams().player_speed_max_ );
    debug( _formation_sample_storage, _formation_sample_evaluator_fn );

    // BEGIN POST-PROCESSING
    std::clog << "BEGIN POST-PROCESSING" << std::endl;
    // remove all samples below a given threshold
    typedef typename soccer::formation_sample_id_type< formation_sample_storage_type >::type formation_sample_id_t;
    // soccer::formation_sample_total_players_significance_evaluator< formation_sample_type > _formation_sample_evaluator;
    soccer::formation_sample_keep_best_N< formation_sample_storage_type >
        _formation_sample_keep_best_N( _formation_sample_storage,
                                       [&] ( const formation_sample_id_t sid )//  -> float
                                             { return _formation_sample_evaluator_fn( soccer::get_sample( sid, _formation_sample_storage ) ); },
                                       128 / 2 );
    soccer::formation_sample_storage_remover< std::function< bool( const formation_sample_id_t) >,
                                              formation_sample_storage_type  >
        _formation_sample_data_remover( _formation_sample_keep_best_N );
    _formation_sample_data_remover( _formation_sample_storage );
    // Remove samples too close with boost breadth first search

    std::clog << "END POST-PROCESSING" << std::endl;
    // END POST-PROCESSING


    // BEGIN Run similarity tests
    std::clog << "BEGIN SIMILARITY TEST" << std::endl;
    typedef typename soccer::formation_sample_id_type< formation_sample_storage_type >::type formation_sample_id_t;
    for ( auto itif = _modeling_params.inputFormationPaths().begin();
          itif!= _modeling_params.inputFormationPaths().end(); itif++ )
    {
        typedef std::pair< int, formation_sample_id_t > sample_matching_pair_type;
        typedef std::map< sample_matching_pair_type, long > sample_matching_cost_map_type;
        sample_matching_cost_map_type _sample_matching_cost_map;
        boost::associative_property_map< sample_matching_cost_map_type >
                _sample_matching_cost_pmap( _sample_matching_cost_map );

        // HACK assuming only formations of type rcsc::Formation
        std::ifstream _formation_ifs( *itif );
        if ( !_formation_ifs.is_open() )
        {
            std::cerr << "Could not open file " << *itif << std::endl;
            continue;
        }

        // HACK Assuming _rcsc_formation_ptr is rcsc::FormationDT
        auto _rcsc_formation_ptr = rcsc::Formation::create( _formation_ifs );
        auto _rcsc_formation_dt_ptr = boost::dynamic_pointer_cast< rcsc::FormationDT >( _rcsc_formation_ptr );
        if ( !_rcsc_formation_dt_ptr )
        {
            std::cerr << "Could not import rcsc::formation " << *itif << std::endl;
            continue;
        }

        if ( !_rcsc_formation_ptr->read( _formation_ifs ) )
        {
            std::cerr << "Could not parse rcsc::formation " << *itif << std::endl;
            continue;
        }

        // Run similarity test
        auto _formation_dissimilarity = soccer::similarity( *_rcsc_formation_dt_ptr,
                                                            _formation_sample_storage,
                                                            _sample_matching_cost_pmap );
        const auto _avg_sample_dissimilarity = _formation_dissimilarity / soccer::num_samples( _formation_sample_storage ) * 1.0;
        auto _sort_fn = [] ( const sample_matching_cost_map_type::value_type& kvp1,
                             const sample_matching_cost_map_type::value_type& kvp2)
                        { return kvp1.second < kvp2.second; };
        std::cout << "Formation " << *itif;
        std::cout << " samples(" << _rcsc_formation_dt_ptr->samples()->dataCont().size() << ","
                                 << soccer::num_samples( _formation_sample_storage ) << ")";
        std::cout << " total_dissimilarity=" << _formation_dissimilarity;
        std::cout << " avg_sample_dissimilarity=" << _avg_sample_dissimilarity;
        const auto _min_sample_dissimilarity = std::min_element( _sample_matching_cost_map.begin(),
                                                                 _sample_matching_cost_map.end(),
                                                                 _sort_fn );
        auto _stddev_sample_dissimilarity = 0.0;
        std::for_each( _sample_matching_cost_map.begin(),
                       _sample_matching_cost_map.end(),
                       [&_stddev_sample_dissimilarity,&_avg_sample_dissimilarity] ( const sample_matching_cost_map_type::value_type& kvp )
                        { _stddev_sample_dissimilarity += std::pow( kvp.second - _avg_sample_dissimilarity, 2 ); }
                       );
        _stddev_sample_dissimilarity = std::sqrt( _stddev_sample_dissimilarity / soccer::num_samples( _formation_sample_storage ));
        std::cout << " stdev_sample_dissimilarity=" << _stddev_sample_dissimilarity;
        std::cout << " min_sample_dissimilarity=(" << _min_sample_dissimilarity->first.first << "," << _min_sample_dissimilarity->second << ")";
        const auto _max_sample_dissimilarity = std::max_element( _sample_matching_cost_map.begin(),
                                                                 _sample_matching_cost_map.end(),
                                                                 _sort_fn );
        std::cout << " max_sample_dissimilarity=(" << _max_sample_dissimilarity->first.first << "," << _max_sample_dissimilarity->second << ")";
        std::cout << std::endl;
    }
    std::clog << "END SIMILARITY TEST" << std::endl;
    // END Run similarity tests


    // BEGIN Convert formation
    rcsc::FormationDT _formation;
    soccer::convert( _formation_sample_storage,
                     [] ( const typename formation_sample_type::player_id_t fs_pid )
                            { return fs_pid; },
                     _formation );
    // TODO Create role ids
    std::ofstream ofs_formation( _modeling_params.outputFormationFile() );
    _formation.print( ofs_formation );
    // END Convert formation



    // TEST Make offensive formation from existing formation samples
    const char* _offensive_formation_file_test = "input_formation_files/433_OFF_playon_2D_Dutch.conf";
    std::ifstream _formation_ifs( _offensive_formation_file_test );
    if ( !_formation_ifs.is_open() )
    {
        std::cerr << "Could not open file " << _offensive_formation_file_test << std::endl;
        exit(-1);
    }

    // HACK Assuming _rcsc_formation_ptr is rcsc::FormationDT
    auto _rcsc_formation_ptr = rcsc::Formation::create( _formation_ifs );
    auto _rcsc_formation_dt_ptr = boost::dynamic_pointer_cast< rcsc::FormationDT >( _rcsc_formation_ptr );
    if ( !_rcsc_formation_dt_ptr )
    {
        std::cerr << "Could not import rcsc::formation " << _offensive_formation_file_test << std::endl;
        exit(-2);
    }

    if ( !_rcsc_formation_ptr->read( _formation_ifs ) )
    {
        std::cerr << "Could not parse rcsc::formation " << _offensive_formation_file_test << std::endl;
        exit(-3);
    }

    const unsigned _bowner_id = 9;
    make_offensive_sample( *_rcsc_formation_dt_ptr->samples(), _bowner_id );

    const char* _out_offensive_formation_file = "offensive_formation.conf";
    std::ofstream ofs_offensive_formation( _out_offensive_formation_file );
    _rcsc_formation_dt_ptr->print( ofs_offensive_formation );

    // END Test make offensive formation from existing formation samples

    /*
     * // Comprovar com a realização dos seguintes testes
     * 1) Formação estática (não muda durante o jogo) e usa como referência a bola
     * 2) Formação dinâmica alterações predefinidas e conhecidas (e.g. 4-3-3[0-100], 4-4-2[100-250]) VER COMO CLASSIFICAR/DETECTAR ALTERAÇÕES DE FORMAÇÃO ) e usa como referência a bola => incorporar na estratégia de modelação
     * 3) Formação dinâmica alterações desconhecidas
     */
  
    return 0;
}
