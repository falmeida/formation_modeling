#ifndef SOCCER_FORMATION_MODELING_SAMPLE_DATA_H
#define SOCCER_FORMATION_MODELING_SAMPLE_DATA_H

#include "formation_sample_traits.hpp"
#include "register/formation_sample.hpp"

#include <rcsslogplayer/types.h>

#include <boost/tuple/tuple.hpp> //!< for boost::tie
#include <boost/iterator/transform_iterator.hpp> //!< for boost::transform_iterator

#include <boost/functional.hpp>
#include <boost/shared_ptr.hpp>

#include <map>
#include <functional>

namespace soccer {

struct no_info { };

/*!
 * \brief The formation_sample struct
 * \tparam PlayerId
 * \tparam Coordinate
 * \tparam Significance
 * \tparam Info
 */
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info = soccer::no_info,
           typename FormationSampleCategory = soccer::formation_sample_tag
         >
struct formation_sample
{
    typedef FormationSampleCategory category;
    typedef PlayerId player_id_t;
    typedef Player player_t;
    typedef Ball ball_t;
    typedef Info info_type;
    typedef boost::transform_iterator< std::function< player_id_t( typename std::map< PlayerId, Player >::value_type ) >,
                                       typename std::map< PlayerId, Player >::const_iterator > player_iterator_t;

    /*!
     * \brief Default constructor
     */
    formation_sample( )
        { }

    /*!
     * \brief Copy constructor
     */
    formation_sample( const formation_sample& other )
        : M_ball( other.M_ball )
        , M_players( other.M_players )
        , M_info( other.M_info )
    {

    }

    /*!
     * \brief Destructor
     */
    ~formation_sample()
    {

    }

    Ball get_ball() const { return M_ball; }
    void set_ball( const Ball& ball ) { M_ball = ball; }

    std::pair< player_iterator_t, player_iterator_t >
    get_players( ) const
    {
        typedef typename formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory >::player_iterator_t player_iterator_t;
        auto _transform_fn = [] ( const typename std::map< PlayerId, Player >::value_type& kvp ) { return kvp.first; };
        return std::make_pair(  player_iterator_t( M_players.begin(), _transform_fn ),
                                player_iterator_t( M_players.end(), _transform_fn ) );
    }

    Player get_player( const PlayerId pid ) const { return M_players.at( pid ); }
    void set_player( const PlayerId pid, const Player& player ) { M_players[pid] = player; }

    Info get_info() const { return M_info; }
    void set_info( const Info& info) { M_info = info; }

    formation_sample& operator=( const formation_sample& other )
    {
        if ( this != &other )
        {
            this->M_ball = other.M_ball;
            this->M_players = other.M_players ;
            this->M_info = other.M_info;
        }
        return *this;
    }

    Ball M_ball;
    std::map< PlayerId, Player > M_players;

    Info M_info;
};


template < typename... T >
struct formation_sample_generator { };

//
// Formation Sample Significance
//
template < typename PlayerId,
           typename BallSignificance = float,
           typename PlayerSignificance = BallSignificance
         >
struct formation_sample_significance {
    typedef BallSignificance ball_significance_type;
    typedef PlayerSignificance player_significance_type;

    formation_sample_significance() { }

    BallSignificance M_ball_significance;
    std::map< PlayerId, PlayerSignificance > M_players_significance;
};

// FormationSampleSignificance
template < typename PlayerId,
           typename BallSignificance = float,
           typename PlayerSignificance = BallSignificance
           >
BallSignificance
ball_significance( const formation_sample_significance< PlayerId, BallSignificance, PlayerSignificance >& _sample_significance )
{
    return _sample_significance.M_ball_significance;
}

template < typename PlayerId,
           typename BallSignificance = float,
           typename PlayerSignificance = BallSignificance
           >
PlayerSignificance
player_significance( const PlayerId pid,
                     const formation_sample_significance< PlayerId, BallSignificance, PlayerSignificance >& _sample_significance )
{
    return _sample_significance.M_players_significance.at(pid);
}

// MutableFormationSampleSignificance
template < typename PlayerId,
           typename BallSignificance = float,
           typename PlayerSignificance = BallSignificance
           >
void
set_ball_significance( const BallSignificance _significance,
                       formation_sample_significance< PlayerId, BallSignificance, PlayerSignificance >& _sample_significance )
{
    _sample_significance.M_ball_significance = _significance;
}

template < typename PlayerId,
           typename BallSignificance = float,
           typename PlayerSignificance = BallSignificance
           >
void
set_player_significance( const PlayerId pid,
                         const PlayerSignificance _significance,
                         formation_sample_significance< PlayerId, BallSignificance, PlayerSignificance >& _sample_significance )
{
    _sample_significance.M_players_significance[pid] = _significance;
}


template < typename FormationSample,
           typename BallSignificanceEvaluator,
           typename PlayerIdSignificanceEvaluator >
formation_sample_significance< typename soccer::player_id_type< FormationSample >::type,
                               typename boost::unary_traits< BallSignificanceEvaluator >::result_type,
                               typename boost::unary_traits< PlayerIdSignificanceEvaluator >::result_type >
create_formation_sample_significance( const FormationSample& _formation_sample,
                                      BallSignificanceEvaluator _ball_significance_evaluator,
                                      PlayerIdSignificanceEvaluator _pid_significance_evaluator )
{
    typedef formation_sample_significance< typename soccer::player_id_type< FormationSample >::type,
                                           typename boost::unary_traits< BallSignificanceEvaluator >::result_type,
                                           typename boost::unary_traits< PlayerIdSignificanceEvaluator >::result_type
                                         > formation_sample_significance_type;

    formation_sample_significance_type _new_formation_sample_significance;

    // Calculate ball significance
    const auto _ball_significance = _ball_significance_evaluator( get_ball( _formation_sample ) );
    set_ball_significance( _ball_significance, _new_formation_sample_significance );

    // Calculate ball significance
    typename soccer::player_iterator_type< FormationSample >::type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( _formation_sample );
    for ( auto itp = _player_begin; _player_begin != _player_end ; itp++ )
    {
        const auto _player_significance = _pid_significance_evaluator( *itp );
        set_player_significance( *itp, _player_significance, _new_formation_sample_significance );
    }

    return _new_formation_sample_significance;
}

//
// Formation sample evaluation
//

} // end namespace soccer


/*!
 * \brief Write a description of a formation sample to an output stream
 * \param os output stream
 * \param _formation_sample the formation sample
 * \return
 */
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Significance,
           typename Info >
std::ostream& operator<<( std::ostream& os,
                          soccer::formation_sample<PlayerId, Ball, Player, Significance, Info> const& _formation_sample )
{
    typedef soccer::formation_sample<PlayerId, Ball, Player, Significance, Info> formation_sample_type;
    os << std::setprecision(2);
    os << soccer::get_info( _formation_sample ) << std::endl;
    os << "ball(" << soccer::get_x( soccer::get_position( soccer::get_ball( _formation_sample ) ) )
       << "," << soccer::get_y( soccer::get_position( soccer::get_ball( _formation_sample ) ) )
       << ") ";
    typename soccer::player_iterator_type< formation_sample_type >::type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = soccer::get_players( _formation_sample );
    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        os << "P" << *itp;
        os << "(" << soccer::get_x( soccer::get_position( soccer::get_player( *itp, _formation_sample ) ) )
           << "," << soccer::get_y( soccer::get_position( soccer::get_player( *itp, _formation_sample ) ) )
           << ") ";
    }
    os << "]";


    return os;
}



namespace rcsc {


/*!
 * \brief The FormationSampleConfidenceBallSpeedMax struct
 */
template < typename FormationSample >
struct FormationSampleConfidenceBallSpeedMax
    : public std::unary_function< rcss::rcg::DispInfoT, double >
{
    const double M_ball_speed_max;

    FormationSampleConfidenceBallSpeedMax( const double ball_speed_max )
        : M_ball_speed_max( ball_speed_max )
    {

    }

    double operator() ( const FormationSample& sample ) const
    {
        // BOOST_CONCEPT_ASSERT(( FormationSampleConcept<FormationSample> ));
        /* BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename FormationSample::formation_sample_category_tag,
                                                     formation_sample_dynamic_tag ),
                                    "Invalid formation sample category );*/
        const double ball_speed = sample.ball_velocity.r();

        const double ball_confidence = ball_speed > M_ball_speed_max
                                        ? 0.0
                                        : 1.0 - (ball_speed / M_ball_speed_max);

        return ball_confidence;
    }
};

/*!
 * \brief The FormationSampleConfidenceAvgPlayerSpeedMax struct
 */
template < typename FormationSample >
struct FormationSampleConfidenceAvgPlayerSpeedMax
: public std::unary_function< rcss::rcg::DispInfoT, double > {
    const double M_player_speed_max;

    FormationSampleConfidenceAvgPlayerSpeedMax( const double player_speed_max )
        : M_player_speed_max( player_speed_max )
    {

    }

    double operator() ( const FormationSample& sample ) const
    {
        double total_confidence = 0;

        for ( auto itv = sample.player_velocities.begin();
              itv != sample.player_velocities.end(); itv++ )
        {
            const Vector2D& player_vel = *itv;
            const double player_speed = player_vel.r();
            double player_confidence = player_speed > M_player_speed_max
                                        ? 0.0
                                        : 1.0 - (player_speed / M_player_speed_max);
            total_confidence += player_confidence;
        }
        // average confidence
        total_confidence /= sample.player_velocities.size() * 1.0;

        return total_confidence;
    }
};

/*!
 * \brief The FormationSampleConfidenceMaxPlayerSpeedMax struct
 */
template < typename FormationSample >
struct FormationSampleConfidenceMaxPlayerSpeedMax
    : public std::unary_function< rcss::rcg::DispInfoT, double > {

    const double M_player_speed_max;

    FormationSampleConfidenceMaxPlayerSpeedMax( const double player_speed_max )
        : M_player_speed_max( player_speed_max )
    {

    }

    double operator() ( const FormationSample& sample ) const
    {
        // BOOST_CONCEPT_ASSERT(( FormationSampleConcept<FormationSample> ));
        /* BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename FormationSample::formation_sample_category_tag,
                                                     formation_sample_dynamic_tag ),
                                    "Invalid formation sample category );*/
        auto itv = sample.player_velocities.begin();
        double player_speed_max =  itv->r();
        itv++;
        for ( ; itv != sample.player_velocities.end(); itv++ )
        {
            const Vector2D& player_vel = *itv;
            const double player_speed = player_vel.r();
            if ( player_speed_max < player_speed )
            {
                player_speed_max = player_speed;
            }
        }

        double total_confidence = player_speed_max > M_player_speed_max
                                    ? 0.0
                                    : 1.0 - (player_speed_max / M_player_speed_max);

        return total_confidence;
    }
};

/*!
 * \brief The FormationSampleConfidenceMaxPlayerSpeedMax struct
 */
template < typename FormationSample >
struct FormationSampleDistanceMax
    : public std::unary_function< rcss::rcg::DispInfoT, double > {

    const double M_ball_distance_max;

    FormationSampleDistanceMax( const double ball_distance_max )
        : M_ball_distance_max( ball_distance_max )
    {

    }

    double operator() ( const FormationSample& sample ) const
    {
        // BOOST_CONCEPT_ASSERT(( FormationSampleConcept<FormationSample> ));

    }
};


} // end namespace rcsc


#include <soccer/core/access.hpp>

// GENERIC FORMATION SAMPLE MODEL REGISTRATION
SOCCER_TRAITS_NAMESPACE_BEGIN

// GENERIC FORMATION SAMPLE TYPE TRAITS REGISTRATION
template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct tag< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef formation_sample_tag type;
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct ball_type< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef Ball type;
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct player_type< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef Player type;
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct player_id_type< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef PlayerId type;
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct player_iterator_type< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef typename soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory >::player_iterator_t type;
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct info_type< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef Info type;
};

// GENERIC FORMATION SAMPLE ACCESS METHODS REGISTRATION
template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct ball_access< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef ::soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > formation_sample_t;

    static
    inline Ball get( formation_sample_t const& obj )
    {
        return obj.get_ball();
    }

    static
    inline void set( Ball const& val,
                     formation_sample_t& obj )
    {
        obj.set_ball( val );
    }
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory>
struct player_iterators_access< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef ::soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > formation_sample_t;

    static
    inline std::pair< typename formation_sample_t::player_iterator_t,
                      typename formation_sample_t::player_iterator_t >
    get( formation_sample_t const& obj )
    {
        return obj.get_players();
    }
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct player_access< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef ::soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > formation_sample_t;

    static
    inline Player get( PlayerId const& pid, formation_sample_t const& obj )
    {
        return obj.get_player( pid );
    }

    static
    inline void set( PlayerId const pid,
                     Player const& val,
                     ::soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory >& obj )
    {
        obj.set_player( pid, val );
    }
};

template <>
template < typename PlayerId,
           typename Ball,
           typename Player,
           typename Info,
           typename FormationSampleCategory >
struct info_access< soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > >
{
    typedef ::soccer::formation_sample< PlayerId, Ball, Player, Info, FormationSampleCategory > formation_sample_t;

    static
    inline Info get( formation_sample_t const& obj )
    {
        return obj.get_info();
    }

    static
    inline void set( Info const& val,
                     formation_sample_t& obj )
    {
        obj.set_info( val );
    }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_FORMATION_MODELING_SAMPLE_DATA_H
