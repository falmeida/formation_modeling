#ifndef SOCCER_STATE_PROCESSORS_HPP
#define SOCCER_STATE_PROCESSORS_HPP

#include <soccer/algorithms/pitch_algorithms.hpp>

#include <soccer/concepts/state_concepts.hpp>
#include <boost/tuple/tuple.hpp>

namespace soccer {

/// State Modifiers (transformations)
template <typename Pitch>
struct vertical_symmetry_state {
    // BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    Pitch const* M_pitch;

    vertical_symmetry_state( const Pitch& pitch )
        : M_pitch( &pitch )
    {

    }

    template < typename State >
    bool operator()( State& state )
    {
        // BOOST_CONCEPT_ASSERT(( StaticStateConcept< State > ));
        // HACK Non-generic access to position coordinate
        auto _new_ball = get_ball( state );
        auto _new_ball_pos = get_position( _new_ball );
        const auto _ball_y = get_y( _new_ball_pos );
        if ( is_within_upper_field( _new_ball_pos, *M_pitch ) )
        {
            const auto _ball_offset_y = std::fabs( _ball_y - get_pitch_top_left_y( *M_pitch ) );
            const auto _new_ball_y = get_pitch_top_left_y( *M_pitch ) +
                                     get_pitch_width( *M_pitch ) -
                                     _ball_offset_y;
            set_y( _new_ball_y, _new_ball_pos );
        }
        else
        {
            const auto _ball_offset_y = fabs( get_pitch_top_left_y( *M_pitch ) +
                                              get_pitch_width( *M_pitch) -
                                              _ball_y );
            const auto _new_ball_y = get_pitch_top_left_y( *M_pitch ) +
                                     _ball_offset_y;
            set_y( _new_ball_y, _new_ball_pos );
        }

        set_position( _new_ball_pos, _new_ball );
        // Invert vertical velocity
        auto _new_ball_vel = get_velocity( _new_ball );
        set_y( -get_y( _new_ball_vel ), _new_ball_vel );
        set_velocity( _new_ball_vel, _new_ball );

        set_ball( _new_ball, state );

        // Invert players y-coordinates
        typename player_iterator_type< State >::type _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( state );
        for ( auto it = _player_begin; it != _player_end ; it++ )
        {
            auto _player = get_player( *it, state );
            auto _player_pos = get_position( _player );
            if ( is_within_upper_field( _player_pos, *M_pitch ) )
            {
                const auto _player_y = get_y( _player_pos );
                const auto _player_offset_y = fabs( _player_y - get_pitch_top_left_y( *M_pitch ) );
                const auto _new_player_y = get_pitch_top_left_y( *M_pitch ) +
                                           get_pitch_width( *M_pitch ) -
                                           _player_offset_y;
                set_y( _new_player_y, _player_pos );
            }
            else
            {
                const auto _player_offset_y = fabs( get_pitch_top_left_y( *M_pitch ) +
                                                    get_pitch_width( *M_pitch) -
                                                    _player.y_ );
                const auto _new_player_y = get_pitch_top_left_y( *M_pitch ) +
                                           _player_offset_y;
                set_y( _new_player_y, _player_pos );
            }
            set_position( _player_pos, _player );
            auto _player_vel = get_velocity( _player );
            const auto _player_vy = get_y( _player_vel );
            // Invert vertical velocity
            set_y( -_player_vy, _player_vel );
            set_velocity( _player_vel, _player );

            set_player( *it, _player, state );
        }
    }
};


template < typename Pitch >
struct exchange_team_sides_state
{
    Pitch const* M_pitch;

    exchange_team_sides_state( const Pitch& pitch )
        : M_pitch( &pitch )
    {

    }

    bool operator()( rcss::rcg::DispInfoT& state )
    {
        // BOOST_CONCEPT_ASSERT(( StaticStateConcept< State > ));
        // HACK Non-generic access to position coordinate
        // HACK Non-generic (using robocup 2d soccer model)
        state.show_.ball_.x_ = -state.show_.ball_.x_;
        state.show_.ball_.vx_ = -state.show_.ball_.vx_;

        auto tmp_team = state.team_[0];
        state.team_[0] = state.team_[1];
        state.team_[1] = tmp_team;

        // Convert right team in left team
        for ( auto i = 0u; i < rcss::rcg::MAX_PLAYER; i++ )
        {
            const auto _left_pid = i;
            const auto _right_pid = i + rcss::rcg::MAX_PLAYER;

            const auto tmp = state.show_.player_[_left_pid];

            state.show_.player_[_left_pid] = state.show_.player_[_right_pid];
            state.show_.player_[_left_pid].x_ = -state.show_.player_[_left_pid].x_;
            state.show_.player_[_left_pid].vx_ = -state.show_.player_[_left_pid].vx_;
            state.show_.player_[_left_pid].side_ = 'l';

            state.show_.player_[_right_pid] = tmp;
            state.show_.player_[_right_pid].x_ = - state.show_.player_[_right_pid].x_;
            state.show_.player_[_right_pid].vx_ = - state.show_.player_[_right_pid].vx_;
            state.show_.player_[_right_pid].side_ = 'r';
        }
    }

    template < typename State >
    bool operator()( State& state )
    {
        auto _new_ball = pitchget_ball( state );
        auto _new_ball_pos = get_position( _new_ball );
        const auto _ball_x = get_x( _new_ball_pos );
        if ( in_left_midfield( _ball_x, *M_pitch ) )
        {
            const auto _ball_offset_x = fabs( _ball_x - pitch_top_left_x( *M_pitch ) );
            const auto _new_ball_x = pitch_top_left_x( *M_pitch ) +
                                     pitch_width( *M_pitch ) -
                                     _ball_offset_x;
            set_x( _new_ball_x, _new_ball_pos );
        }
        else
        {
            const auto _ball_offset_x = fabs( pitch_top_left_x( *M_pitch ) +
                                              pitch_length( *M_pitch) -
                                              _ball_x );
            const auto _new_ball_x = pitch_top_left_x( *M_pitch ) +
                          _ball_offset_x;
            set_x( _new_ball_x, _new_ball_pos );
        }
        set_position( _new_ball_pos, _new_ball );
        // Invert ball velocity
        const auto _new_ball_vel = get_velocity( _new_ball );
        set_x( get_x( _new_ball_vel ), _new_ball_vel );
        set_velocity( _new_ball_vel, _new_ball );
        set_ball( _new_ball, state );

        // Switch teams
        auto tmp_team = get_left_team( state );
        set_left_team( get_right_team( state ), state );
        set_right_team( tmp_team, state );

        // Convert right team in left team
        typename player_iterator_type< State >::type _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( state );
        for ( auto itp = _player_begin; itp != _player_end; _player_end++ )
        {
            // HACK non generic
            auto _new_pid = (*itp + 11) % (11 * 2);
            auto _new_player_side = *itp < 11  ? 'l' : 'r';
            auto _new_player = get_player( _new_pid, state );
            auto _new_player_pos = get_position( _new_player );
            auto _new_player_vel = get_velocity( _new_player );

            // TODO Non-generic => Should user field model info
            set_x( -get_x( _new_player_pos ), _new_player_pos );
            set_x( -get_x( _new_player_vel ), _new_player_vel );
            set_position( _new_player_pos, _new_player );
            set_velocity( _new_player_vel, _new_player );

            set_side( _new_player_side, _new_player );
            set_player( *itp, _new_player, state );
        }
    }
};


//
// StatePreprocessor
//
template < typename State,
           typename Pitch >
/*!
 * \brief The state_preprocessor struct
 */
struct state_preprocessor
    : public std::unary_function< State, void >
{
    BOOST_CONCEPT_ASSERT(( soccer::StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( soccer::PitchConcept< Pitch > ));

    typedef typename soccer::side_type< typename soccer::player_type< State >::type >::type side_t;

    Pitch* const M_pitch;
    side_t M_formation_side;
    side_t M_team_side;
    bool M_use_vertical_symmetry;

    state_preprocessor( )
        : M_pitch( NULL )
        , M_formation_side( soccer::get_left_side< side_t >() )
        , M_team_side( soccer::get_unknown_side< side_t >() )
        , M_use_vertical_symmetry( true )
    {

    }

    void set_soccer_model( Pitch* const soccer_model )
    {
        M_pitch = soccer_model;
    }

    void set_team_side( side_t const side )
    {
        this->M_team_side = side;
    }

    void set_use_vertical_symmetry( bool enable )
    {
        this->M_use_vertical_symmetry = enable;
    }

    void operator()( State& state )
    {
        assert(( this->M_team_side != soccer::get_unknown_side< side_t >() ));

        if ( M_team_side != M_formation_side )
        {
            std::clog << "Exchanging team sides" << std::endl;
            soccer::exchange_team_sides_state< Pitch > _exchange_team_sides( *M_pitch );
            _exchange_team_sides( state );
        }

        const auto _ball = soccer::get_ball( state );
        const auto _ball_position = soccer::get_position( _ball );
        if ( M_use_vertical_symmetry &&
             !soccer::is_within_upper_field( _ball_position, *M_pitch ) )
        {
            std::clog << "Applying vertical symmetry" << std::endl;
            soccer::vertical_symmetry_state< Pitch > _vertical_symmetry_state( *M_pitch );
            _vertical_symmetry_state( state );
        }
    }
};


} // end namespace soccer

#endif // SOCCER_STATE_PROCESSORS_HPP
