#ifndef FORMATION_SAMPLE_STORAGE_POLICY_HPP
#define FORMATION_SAMPLE_STORAGE_POLICY_HPP

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "formation_sample_concept.hpp"

#include "sample_matching.h"

#include "formation_sample_transform.hpp"

#include <cassert>

#include <iostream>
#include <functional>

namespace soccer {


/*!
 * \brief Always add formation storage policy for formation samples
 * \tparam FormationSample
 * \tparam Triangulation
 */
template < typename FormationSample,
           typename Triangulation = typename soccer::CGAL::triangulation_formation_sample_storage_generator< FormationSample >::type
         >
struct always_add_formation_sample_storage_policy
    : std::unary_function< typename soccer::formation_sample_type< Triangulation >::type, void >
{
    typedef Triangulation formation_sample_storage_type;

    typedef typename soccer::formation_sample_id_type< Triangulation > ::type formation_sample_id_type;
    typedef typename soccer::formation_sample_type< Triangulation > ::type formation_sample_type;
    typedef typename soccer::player_id_type< formation_sample_type >::type sample_player_id_descriptor;
    typedef std::map< sample_player_id_descriptor,
                      sample_player_id_descriptor > formation_sample_player_id_map_type;

    Triangulation* M_storage;

    always_add_formation_sample_storage_policy( )
        : M_storage( NULL )
    {

    }

    always_add_formation_sample_storage_policy( Triangulation& cgal_triangulation  )
        : M_storage( &cgal_triangulation )
    {

    }

    void set_storage( Triangulation* storage )
    {
        M_storage = storage;
        // Would it be relevant to trigger an event to notify eventual observers that the storage back-end has changed
    }

    virtual bool operator()( formation_sample_type _new_formation_sample )
    {
        assert( M_storage );

        auto _ball = get_ball( _new_formation_sample );
        auto _ball_position = get_position( _ball );
        auto _ball_x = get_x( _ball_position );
        auto _ball_y = get_y( _ball_position );
        typename Triangulation::Point _ball_pt( _ball_x, _ball_y );

        auto _new_vertex_handle = M_storage->insert( _ball_pt );

        assert( !M_storage->is_infinite( _new_vertex_handle ) );

        const auto _vertex_id = num_samples( *M_storage ) - 1;
        _new_vertex_handle->info() = _new_formation_sample;
        _new_vertex_handle->id() = _vertex_id;
    }
};

/*!
 * \brief Storage policy for formation samples
 * \tparam FormationSample
 */
template < typename FormationSample,
           typename FormationSampleSignificanceEvaluator = std::function< float(const FormationSample&) >,
           typename Triangulation = typename soccer::CGAL::triangulation_formation_sample_storage_generator< FormationSample >::type
         >
struct formation_sample_storage_policy
    : std::unary_function< typename soccer::formation_sample_type< Triangulation >::type, bool >
{
    typedef Triangulation formation_sample_storage_type;

    typedef typename formation_sample_id_type< Triangulation > ::type formation_sample_id_t;
    typedef typename formation_sample_type< Triangulation > ::type formation_sample_t;
    typedef typename player_id_type< formation_sample_t >::type formation_sample_player_id_descriptor;
    typedef std::map< formation_sample_player_id_descriptor,
                      formation_sample_player_id_descriptor
                    > formation_sample_player_id_map_type;

    Triangulation* M_cgal_triangulation;
    FormationSampleSignificanceEvaluator M_sample_scorer;
    float M_sample_dist2_thr;

    formation_sample_storage_policy( Triangulation& cgal_triangulation,
                                     FormationSampleSignificanceEvaluator sample_scorer,
                                     const float sample_dist2_thr = std::numeric_limits< float >::max() )
        : M_cgal_triangulation( &cgal_triangulation )
        , M_sample_scorer( sample_scorer )
        , M_sample_dist2_thr( sample_dist2_thr )
    {

    }

    template <typename FormationSampleSignificanceEvaluatorFn >
    void set_sample_scorer( FormationSampleSignificanceEvaluatorFn sample_scorer )
    {
        M_sample_scorer = sample_scorer;
    }

    bool operator()( formation_sample_t _new_formation_sample )
    {
        typename Triangulation::Locate_type lt;
        int li;
        auto _ball_position = soccer::get_position( soccer::get_ball( _new_formation_sample ) );
        typename Triangulation::Point ball_pt( soccer::get_x( _ball_position ),
                                               soccer::get_y( _ball_position ));

        auto _new_sample_score = M_sample_scorer( _new_formation_sample );
        auto _contained_face_handle = M_cgal_triangulation->locate( ball_pt, lt, li );
        auto nvertices = M_cgal_triangulation->number_of_vertices();
        auto _new_vertex_handle = M_cgal_triangulation->infinite_vertex();

        std::clog << " new_sample=" << _new_formation_sample << std::endl;

        switch ( lt )
        {
            case Triangulation::VERTEX:
            {
                // li is set to the index of the vertex index in the resulting face
                auto _vertex_handle = _contained_face_handle->vertex( li );
                auto _vertex_sample = _vertex_handle->info();
                auto _vertex_sample_score = M_sample_scorer( _vertex_sample );
                // TODO Look at individual players not just the overall picture
                if ( _vertex_sample_score < _new_sample_score )
                {
                    auto _sample_info = soccer::get_info( _vertex_sample );
                    /* std::cout << "Replaced VERTEX sample at time ="
                              << " and score=" << _vertex_sample_score
                              << " with sample at time=" << soccer::get_time( soccer::modeling::get_state( _new_formation_sample ) )
                              << " and score=" << _new_sample_score
                              << std::endl; */
                    _new_vertex_handle = _vertex_handle;

                    // Calculate matching between new_formation_sample and existing samples

                }

                // TODO Merge samples similarity for players => TODO Matching between player ids
                std::clog << "Merge samples based on differences " << std::endl;
            }
            break;
            case Triangulation::EDGE:
            {
                // li is set to the index of the vertex opposite to the edge
                // Analyze distance between points
                auto _edge_ccw_vertex_handle = _contained_face_handle->vertex( M_cgal_triangulation->ccw( li ) );
                auto _edge_cw_vertex_handle = _contained_face_handle->vertex( M_cgal_triangulation->cw( li ) );
                auto _edge_length = ::CGAL::sqrt( ::CGAL::squared_distance( _edge_ccw_vertex_handle->point(),
                                                                        _edge_cw_vertex_handle->point() ) );
                std::clog << "TODO Do something!!! Edge length=" << _edge_length << std::endl;
            }
            break;
            case Triangulation::FACE:
            {
                std::cout << " CONTAINED_FACE ";
                auto _v0_handle = _contained_face_handle->vertex( 0 );
                auto _v1_handle = _contained_face_handle->vertex( 1 );
                auto _v2_handle = _contained_face_handle->vertex( 2 );
                auto _face_area = M_cgal_triangulation->triangle( _contained_face_handle ).area();


                auto _v0_dist2 = ::CGAL::squared_distance( _v0_handle->point(), ball_pt );
                auto _v1_dist2 = ::CGAL::squared_distance( _v1_handle->point(), ball_pt );
                auto _v2_dist2 = ::CGAL::squared_distance( _v2_handle->point(), ball_pt );

                // rcsc::Formation HACK for near data which messes up player matchings indexes in BFS visit
                auto _nearest_vertex = _v0_dist2 < _v1_dist2
                                        ? _v0_dist2 < _v2_dist2
                                            ? _v0_handle
                                            : _v2_handle
                                        : _v1_dist2 < _v2_dist2
                                            ? _v1_handle
                                            : _v2_handle;

                if ( _v0_dist2 < M_sample_dist2_thr ||
                     _v1_dist2 < M_sample_dist2_thr ||
                     _v2_dist2 < M_sample_dist2_thr )
                {
                    // Disconsider the sample (for now, even it is better)
                    std::cout << " TOO_NEAR_DATA thr=" << M_sample_dist2_thr << " Disconsider the sample (for now, even it is better)" << std::endl;
                    // TODO Should we replace any of the previous samples, namingly the nearest???
                    break;
                }

                // TODO Store score in vertex property to prevent recalculation of score
                auto _nearest_vertex_sample = _nearest_vertex->info();
                auto _nearest_sample_score = M_sample_scorer( _nearest_vertex_sample );
                if ( _nearest_sample_score >= _new_sample_score )
                {
                    std::cout << "Nearest sample score (" << _nearest_sample_score
                              << ") >= new_sample score("
                              << _new_sample_score << ")" << std::endl;
                    break;
                }

                /* std::cout << "Replaced FACE sample (t=" // << soccer::get_time( soccer::modeling::get_state( soccer::get_info( _nearest_vertex_sample ) ) )
                          << ",score=)" << _nearest_sample_score
                          << " with sample(t=" << soccer::get_time( soccer::modeling::get_state( _new_formation_sample ) )
                          << ",score=" << _new_sample_score
                          << " ball_dist " << (_nearest_vertex == _v0_handle
                                                ? _v0_dist2
                                                : _nearest_vertex == _v1_handle
                                                  ? _v1_dist2
                                                  : _v2_dist2)
                          << " face_area=" << _face_area
                          << std::endl; */

                _new_vertex_handle = M_cgal_triangulation->insert( ball_pt, _contained_face_handle );


                // M_triangulation.remove(_nearest_vertex );

                // Decide if point should be inserted
                // 1) check area of face
                // 2) check distance to each vertex
                // 3) analyze similarity of neighbor samples
                // 4) analyze relevance of neighbor samples
            }
            break;
            case Triangulation::OUTSIDE_AFFINE_HULL:
            case Triangulation::OUTSIDE_CONVEX_HULL:
            {
                std::cout << ( lt == Triangulation::OUTSIDE_AFFINE_HULL
                                ? "OUTSIDE_AFFINE_HULL "
                                : "OUTSIDE_CONVEX_HULL" )
                          << std::endl;
                // ??
                // Analyze
                // Add new sample
                if ( nvertices == 0 )
                {
                    std::cout << " => Insert first vertex " << std::endl;
                    _new_vertex_handle = M_cgal_triangulation->insert( ball_pt );
                    break;
                }
                // Analyze
                auto _nearest_vertex = M_cgal_triangulation->nearest_vertex( ball_pt );
                auto _nearest_vertex_dist2 = ::CGAL::squared_distance( _nearest_vertex->point(),
                                                                     ball_pt );
                std::cout << " nearest_vertex_dist2=" << _nearest_vertex_dist2 << " < sample_dist2_thr=" << M_sample_dist2_thr << "?" << std::endl;
                if ( _nearest_vertex_dist2 < M_sample_dist2_thr )
                {
                    // Disconsider the sample (for now, even it is better)
                    std::cout << " TOO_NEAR_DATA thr=" << M_sample_dist2_thr
                              << " Disconsider the sample (for now, even it is better)"
                              << std::endl;
                    break;
                }

                _new_vertex_handle = M_cgal_triangulation->insert( ball_pt );
            }
            break;

        }

        if ( !M_cgal_triangulation->is_infinite( _new_vertex_handle ) )
        {

            if ( nvertices != 0 )
            {
                // BEGIN CALCULATE MATCHING
                auto _it_incident_vertices_circulator = M_cgal_triangulation->incident_vertices( _new_vertex_handle );
                std::vector< formation_sample_id_t > _incident_vertices;
                const auto _end_incident_vertices_circulator = _it_incident_vertices_circulator;
                do {
                    if ( !M_cgal_triangulation->is_infinite( _it_incident_vertices_circulator ) )
                    {
                        _incident_vertices.push_back( _it_incident_vertices_circulator );
                    }
                    _it_incident_vertices_circulator++;
                } while ( _it_incident_vertices_circulator != _end_incident_vertices_circulator );

                std::cout << std::endl << "Incident vertices[" << _incident_vertices.size() << "]=[";
                std::for_each( _incident_vertices.begin(),
                               _incident_vertices.end(),
                               [&] ( const typename Triangulation::Vertex_handle vh )
                                {
                                    std::cout << "(" << vh->id() << ",";
                                    std::cout << vh->info() << ")" << std::endl;
                                }
                             );
                std::cout << "]" << std::endl;

                formation_sample_player_id_map_type _player_matchings;
                boost::associative_property_map< formation_sample_player_id_map_type >
                        _player_matchings_prop_map( _player_matchings );

                auto _sample_player_iterators = soccer::get_players( _new_formation_sample );
                soccer::modeling::player_matching_cost< Triangulation >
                        _player_matching_cost_fn( _new_formation_sample,
                                                  *M_cgal_triangulation,
                                                  _incident_vertices.begin(),
                                                  _incident_vertices.end() );
                long cost = soccer::modeling::formation_sample_matching( _sample_player_iterators.first,
                                                                         _sample_player_iterators.second,
                                                                         _player_matching_cost_fn,
                                                                         _player_matchings_prop_map );

                std::cout << "Matchings[cost=" << cost << "]=";
                std::for_each( _player_matchings.begin(),
                               _player_matchings.end(),
                               [] ( const std::pair< formation_sample_player_id_descriptor,
                                                     formation_sample_player_id_descriptor >& kvp )
                                    { std::cout << "(" << kvp.first << "," << kvp.second << ")"; } );
                std::cout << std::endl;
                //!< apply matchings found to the new formation sample
                soccer::apply_matchings( _new_formation_sample, _player_matchings_prop_map );
                // END CALCULATE MATCHINGS
            }

            // HACK non-generic declaration of info type );
            _new_vertex_handle->info() = _new_formation_sample;
            _new_vertex_handle->id() = M_cgal_triangulation->number_of_vertices() - 1;

            std::clog << std::endl << " vertex_id=" << _new_vertex_handle->id() << std::endl;
            std::clog << " new_sample=" << _new_formation_sample << std::endl;
            std::clog << " score=" << _new_sample_score << std::endl;
            return true;
        }

        return false;
    }
};


} // end namespace soccer

#endif // FORMATION_SAMPLE_STORAGE_POLICY_HPP
