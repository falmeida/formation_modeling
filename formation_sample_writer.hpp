#ifndef SOCCER_FORMATION_SAMPLE_WRITER_HPP
#define SOCCER_FORMATION_SAMPLE_WRITER_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <rcsc/formation/formation_dt.h>

#include "formation_sample_storage.hpp"

#include <boost/tuple/tuple.hpp>

namespace soccer {

template < typename FormationSampleStorage,
           typename PlayerIdConverter >
void convert( const FormationSampleStorage& _storage,
              PlayerIdConverter _player_id_converter,
              ::rcsc::FormationDT& formation,
              bool symmetry = false )
{
    typedef typename soccer::formation_sample_iterator_type< FormationSampleStorage >::type formation_sample_iterator_type;
    typedef typename soccer::formation_sample_type< FormationSampleStorage >::type formation_sample_type;
    typedef typename soccer::player_iterator_type< formation_sample_type >::type sample_player_iterator_type;
    // typedef typename soccer::sample_id< FormationSampleStorage >::type formation_sample_id_type;

    formation_sample_iterator_type _sample_begin, _sample_end;
    boost::tie( _sample_begin, _sample_end ) = get_samples( _storage );
    for ( auto its = _sample_begin; its != _sample_end ; its++ )
    {
        const auto& _sample = get_sample( *its, _storage );
        // Print sample id
        std::cout << "VertexId=" << (*its)->id() << std::endl;

        ::rcsc::formation::SampleData _rcsc_formation_sample;

        // Save ball
        const auto _ball_position = soccer::get_position( soccer::get_ball( _sample ));
        _rcsc_formation_sample.ball_.x = get_x( _ball_position );
        _rcsc_formation_sample.ball_.y = get_y( _ball_position );

        // Save players
        _rcsc_formation_sample.players_.resize( num_players( _sample ) );
        sample_player_iterator_type _sample_player_begin, _sample_player_end;
        boost::tie( _sample_player_begin, _sample_player_end ) = get_players( _sample );
        for ( auto itp = _sample_player_begin; itp != _sample_player_end ; itp++ )
        {
            const auto _player = get_player( *itp, _sample );
            const auto _player_position = soccer::get_position( _player );

            auto _role_id = _player_id_converter( *itp );
            _rcsc_formation_sample.players_[ _role_id ].x = get_x( _player_position );
            _rcsc_formation_sample.players_[ _role_id ].y = get_y( _player_position );
        }

        const auto _samples_add_data_res = formation.samples()->addData( formation, _rcsc_formation_sample, symmetry);

        switch ( _samples_add_data_res )
        {
            case ::rcsc::formation::SampleDataSet::NO_FORMATION:
            case ::rcsc::formation::SampleDataSet::TOO_MANY_DATA:
            case ::rcsc::formation::SampleDataSet::TOO_NEAR_DATA:
            case ::rcsc::formation::SampleDataSet::ILLEGAL_SYMMETRY_DATA:
            case ::rcsc::formation::SampleDataSet::TOO_NEAR_SYMMETRY_DATA:
            case ::rcsc::formation::SampleDataSet::INSERT_RANGE_OVER:
            case ::rcsc::formation::SampleDataSet::INVALID_INDEX:
            case ::rcsc::formation::SampleDataSet::DUPLICATED_INDEX:
            case ::rcsc::formation::SampleDataSet::DUPLICATED_CONSTRAINT:
            case ::rcsc::formation::SampleDataSet::INTERSECTS_CONSTRAINT:
                std::cerr << "addSample error "
                          << _samples_add_data_res
                          // << " sample_id=" << *its
                          << " ball=" << _rcsc_formation_sample.ball_
                          << std::endl;
            break;
            case ::rcsc::formation::SampleDataSet::NO_ERROR:
            break;

        }
    }
}

}

#endif // SOCCER_FORMATION_SAMPLE_WRITER_HPP
