#ifndef SOCCER_FORMATION_SAMPLE_STORAGE_HPP
#define SOCCER_FORMATION_SAMPLE_STORAGE_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <boost/concept_check.hpp>
#include "Core/access.hpp"

#include <vector>
#include <map>

#include <algorithm>

#include <boost/tuple/tuple.hpp> //!< boost::tie

#include <boost/functional.hpp> //!< STL does not support unary traits inference
#include <functional>

#include <iterator>
#include <type_traits>

#include <iostream>

namespace soccer {

// POST-PROCESSING

/*!
 * \brief formation sample predicate
 */
template < typename FormationSamplePredicate,
           typename FormationSample >
struct formation_sample_unary_predicate
    : public std::unary_function< bool, FormationSample > {

    //!< concept assertions do not "work" with lambda functions (TODO try defining internal typedefs)
    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicate< FormationSamplePredicate, FormationSample > ));

    FormationSamplePredicate M_formation_sample_predicate;

    formation_sample_unary_predicate( FormationSamplePredicate formation_sample_predicate )
        : M_formation_sample_predicate( formation_sample_predicate )
    {

    }

    bool operator()( const FormationSample& sample )
    {
        return M_formation_sample_predicate( sample );
    }
};

/*!
 * \brief formation sample score unary predicate
 */
template < typename FormationSampleScoreEvaluator,
           typename ScorePredicate,
           typename FormationSample >
struct formation_sample_score_unary_predicate
    : public std::unary_function< bool, FormationSample > {

    typedef boost::unary_traits< FormationSampleScoreEvaluator > score_evaluator_traits;
    typedef typename score_evaluator_traits::result_type score_type;

    //!< concept assertions do not "work" with lambda functions (TODO try defining internal typedefs)
    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicate< ScorePredicate, score_type > ));

    // BOOST_CONCEPT_ASSERT(( boost::UnaryFunction<FormationSampleScoreEvaluator> ));
    FormationSampleScoreEvaluator  M_formation_sample_score_evaluator;
    ScorePredicate M_score_predicate;

    formation_sample_score_unary_predicate( FormationSampleScoreEvaluator formation_sample_score_evaluator,
                                      ScorePredicate score_predicate )
        : M_formation_sample_score_evaluator( formation_sample_score_evaluator )
        , M_score_predicate( score_predicate  )
    {

    }

    bool operator()( const FormationSample& sample )
    {
        const auto _score = M_formation_sample_score_evaluator( sample );
        return M_score_predicate( _score );
    }
};

template < typename FormationSampleIdUnaryPredicate,
           typename FormationSampleStorage >
/*!
 * \brief The formation_sample_storage_remover struct
 */
struct formation_sample_storage_remover
    : public std::unary_function< void, FormationSampleStorage >
{
    typedef typename soccer::formation_sample_iterator_type< FormationSampleStorage >::type formation_sample_iterator_type;
    typedef typename soccer::formation_sample_id_type< FormationSampleStorage >::type formation_sample_id_type;

    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicateConcept< FormationSampleIdUnaryPredicate, formation_sample_id_type > ));

    FormationSampleIdUnaryPredicate M_formation_sample_unary_predicate;

    formation_sample_storage_remover( FormationSampleIdUnaryPredicate formation_sample_id_unary_predicate )
        : M_formation_sample_unary_predicate( formation_sample_id_unary_predicate )
    {

    }

    void operator()( FormationSampleStorage& _sample_storage )
    {
        formation_sample_iterator_type _sample_begin, _sample_end;
        boost::tie( _sample_begin, _sample_end ) = get_samples( _sample_storage );
        std::vector< formation_sample_id_type > _sample_ids_to_remove;

        // 1) Collect all sample ids that should be removed
        std::clog << "Samples to remove=[";
        for ( auto its = _sample_begin; its != _sample_end ; its++ )
        {
            if ( M_formation_sample_unary_predicate( *its ) )
            {
                std::clog << *its << ", ";
                _sample_ids_to_remove.push_back( *its );
            }
        }
        std::clog << "]" << std::endl;

        // 2) Remove each collected sample id from storage

        for ( auto itrs = _sample_ids_to_remove.begin(); itrs != _sample_ids_to_remove.end(); itrs++ )
        {
            remove_sample( *itrs, _sample_storage );
        }
    }
};

/*!
 * \brief Keep only the best N formation samples in storage
 * \tparam FormationSampleStorage model of storage for samples
 */
template < typename FormationSampleStorage >
struct formation_sample_keep_best_N
    : public std::unary_function< bool,
                                  typename soccer::formation_sample_id_type< FormationSampleStorage >::type >
{
    typedef typename soccer::formation_sample_iterator_type< FormationSampleStorage >::type formation_sample_iterator_t;
    typedef typename soccer::formation_sample_id_type< FormationSampleStorage >::type formation_sample_id_t;
    std::vector< formation_sample_id_t > M_sample_ids;

    template < typename FormationSampleScoreEvaluator >
    formation_sample_keep_best_N( const FormationSampleStorage& storage,
                                  FormationSampleScoreEvaluator score_evaluator,
                                  const size_t max_samples )
    {
        typedef float score_type; // HACK formation_sample_id_type not being correctly determined!!!
        // typedef decltype( FormationSampleScoreEvaluator(formation_sample_id_type{}) ) score_type;

        // BOOST_CONCEPT_ASSERT(( boost::UnaryFunction< FormationSampleScoreEvaluator, score_type, formation_sample_id_type> ));

        formation_sample_iterator_t sf, sl;
        boost::tie( sf, sl ) = get_samples( storage );
        auto _num_samples = std::distance( sf, sl );
        if ( _num_samples > max_samples )
        {
            std::map< formation_sample_id_t, score_type > _sample_scores;
            // Load all samples ids and precalculate their scores
            std::for_each( sf,
                           sl,
                           [&] ( const formation_sample_id_t sid )
                            {
                                M_sample_ids.push_back( sid );
                                _sample_scores[ sid ] = score_evaluator( sid );
                            }
                         );

            // Sort samples in descending order
            std::sort( M_sample_ids.begin(),
                       M_sample_ids.end(),
                       [&] ( const formation_sample_id_t sid1, const formation_sample_id_t sid2 )
                            { return _sample_scores.at(sid1) < _sample_scores.at(sid2); }
                      );
            // Remove excess samples
            auto _num_samples_to_erase = M_sample_ids.size() - max_samples;
            M_sample_ids.erase( M_sample_ids.begin() + _num_samples_to_erase,
                                M_sample_ids.end() );
            // TODO IMPORTANT CHECK WHETHER THE REMOVAL OF SAMPLES AFFECTS THE CALCULATION OF TRIANGULATIONS (in particular player motions/swaps)!!!!
        }
    }

    bool operator()( const formation_sample_id_t sid )
    {
        return std::find( M_sample_ids.begin(),
                          M_sample_ids.end(),
                          sid ) != M_sample_ids.end();
    }

};


} // end namespace soccer


#endif // SOCCER_FORMATION_SAMPLE_STORAGE_HPP
