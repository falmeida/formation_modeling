#ifndef SOCCER_FORMATION_MODELING_FACTORY_HPP
#define SOCCER_FORMATION_MODELING_FACTORY_HPP

#include "modeling_strategies.h"

class formation_modeling_factory {

private:
    formation_modeling_factory() { }

public:
    formation_modeling_factory& instance()
    {
        static formation_modeling_factory _instance;
        return _instance;
    }

    rcsc::formation_modeler* create( )
    {
        assert( false );
        return NULL;
    }

};

#endif // SOCCER_FORMATION_MODELING_FACTORY_HPP
