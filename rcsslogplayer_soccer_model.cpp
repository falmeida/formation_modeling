#include "rcsslogplayer_soccer_model.h"

namespace soccer {
namespace model {

float pitch_top_left_y( const rcss::rcg::ServerParamT& )
{
    return -34.0f;
}

float pitch_top_left_x( const rcss::rcg::ServerParamT& )
{
    return -52.5f;
}

float pitch_width( const rcss::rcg::ServerParamT& )
{
    return 68.0f;
}

float pitch_length( const rcss::rcg::ServerParamT& model )
{
    return 105.0f;
}

float penalty_area_width( const rcss::rcg::ServerParamT& )
{
    return 40.32f;
}

float penalty_area_length( const rcss::rcg::ServerParamT& )
{
    return 16.5f;
}

float goal_area_width( const rcss::rcg::ServerParamT& )
{
    return 5.5f;
}

float goal_area_length( const rcss::rcg::ServerParamT& )
{
    return 18.32f;
}

float goal_width( const rcss::rcg::ServerParamT& model)
{
    return model.goal_width_;
}

float goal_depth( const rcss::rcg::ServerParamT& )
{
    return 2.44f;
}


float center_circle_radius( const rcss::rcg::ServerParamT& )
{
    return 9.15f;
}

float penalty_spot_dist( const rcss::rcg::ServerParamT& )
{
    return 11.0f;
}

float corner_arc_radius( const rcss::rcg::ServerParamT& )
{
    return 1.0f;
}

unsigned short num_players( const rcss::rcg::ServerParamT&  )
{
    return 11u;
}

}
}
