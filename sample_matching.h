#ifndef SOCCER_FORMATION_MODELING_SAMPLE_MATCHING_H
#define SOCCER_FORMATION_MODELING_SAMPLE_MATCHING_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "formation_sample_concept.hpp"

#include "Core/access.hpp"

#include <rcsslogplayer/types.h>

// For planarity testing of player matching in position space
#include <CGAL/intersections.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
// #include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <boost/graph/graphviz.hpp>

#include <boost/property_map/property_map.hpp>

#include <boost/tuple/tuple.hpp>

#include <boost/concept_check.hpp>
#include <boost/type_traits.hpp>

//!< Enable the use of named parameters in functions
#include <boost/parameter/name.hpp>
#include <boost/parameter/preprocessor.hpp>

#include <cstdlib>
#include <map>
#include <list>

#include <iomanip>
#include <functional>

namespace soccer {
namespace modeling {

BOOST_PARAMETER_NAME( sample1 )
BOOST_PARAMETER_NAME( sample2 )
BOOST_PARAMETER_NAME( sample_id1 )
BOOST_PARAMETER_NAME( sample_id2 )
BOOST_PARAMETER_NAME( player_id_cost_map )
BOOST_PARAMETER_NAME( player_id_map )


//
// Player matching cost function
//

/*!
 * \brief Calculate the squared euclidean distance between 2 players
 * \tparam Model of a player object
 */
template < typename PlayerDescriptor >
struct player_squared_distance
    : public std::binary_function< PlayerDescriptor, PlayerDescriptor, long > {

    virtual long operator()( const PlayerDescriptor& p1,
                             const PlayerDescriptor& p2 )
    {
        const auto _p1_pos = soccer::get_position( p1 );
        const auto _p2_pos = soccer::get_position( p2 );
        const auto _dist2 = std::pow( soccer::get_x( _p1_pos ) - soccer::get_x( _p2_pos ), 2 ) +
                            std::pow( soccer::get_y( _p1_pos ) - soccer::get_y( _p2_pos ), 2 );
        return _dist2;
    }
};

/*!
 * \brief Calculate the euclidean distance between 2 player objects
 * \tparam Model of a player object
 */
template < typename PlayerDescriptor >
struct player_euclidean_distance
    : public player_squared_distance< PlayerDescriptor > {

    long operator()( const PlayerDescriptor& p1,
                     const PlayerDescriptor& p2 )
    {
        const auto _dist2 = player_squared_distance< PlayerDescriptor >::operator ()( p1, p2 );
        const auto _dist = std::sqrt( _dist2 );
        return _dist * 100.0; // 2 decimals precision for comparison due to long conversion/truncation
    }
};

template < typename FormationSampleStorage,
           typename PlayerMatchingCostFunction = player_euclidean_distance< typename soccer::player_type< typename soccer::formation_sample_type< FormationSampleStorage >::type >::type >
         >
/*!
 * \brief Calculate the cost of matching players from neighboring samples
 */
struct player_matching_cost
    : public std::binary_function< typename soccer::player_id_type< typename soccer::formation_sample_type< FormationSampleStorage >::type >::type,
                                   typename soccer::player_id_type< typename soccer::formation_sample_type< FormationSampleStorage >::type >::type,
                                   long >
{
    typedef typename soccer::formation_sample_id_type< FormationSampleStorage >::type formation_sample_id_type;
    typedef typename soccer::formation_sample_type< FormationSampleStorage >::type formation_sample_type;
    typedef typename soccer::player_id_type< formation_sample_type >::type sample_player_id_type;

    // BOOST_CONCEPT_ASSERT(( soccer::FormationSampleStorageConcept< FormationSampleStorage > ));

    const formation_sample_type* M_new_sample;
    const FormationSampleStorage* M_storage;
    std::list< formation_sample_id_type > M_comparable_sample_ids;
    PlayerMatchingCostFunction M_cost_function;

    player_matching_cost( const formation_sample_type& _new_sample,
                          const FormationSampleStorage& _storage,
                          formation_sample_id_type _sample_id )
        : M_new_sample( &_new_sample )
        , M_storage( &_storage )
    {
        M_comparable_sample_ids.push_back( _sample_id );
    }

    player_matching_cost( const formation_sample_type& _new_sample,
                          const FormationSampleStorage& _storage,
                          formation_sample_id_type _sample_id,
                          PlayerMatchingCostFunction _cost_function )
        : M_new_sample( &_new_sample )
        , M_storage( &_storage )
        , M_cost_function( _cost_function )
    {
        M_comparable_sample_ids.push_back( _sample_id );
    }

    template < typename SampleIdInputIterator >
    player_matching_cost( const formation_sample_type& _new_sample,
                          const FormationSampleStorage& _storage,
                          SampleIdInputIterator _sample_begin,
                          const SampleIdInputIterator _sample_end )
        : M_new_sample( &_new_sample )
        , M_storage( &_storage )
        , M_comparable_sample_ids( _sample_begin, _sample_end )
    {
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< SampleIdInputIterator>::type,
                                                  formation_sample_id_type>::value ),
                                "Sample id descriptor types mismatch");

    }

    template < typename SampleIdInputIterator >
    player_matching_cost( const formation_sample_type& _new_sample,
                          const FormationSampleStorage& _storage,
                          SampleIdInputIterator _sample_begin,
                          const SampleIdInputIterator _sample_end,
                          PlayerMatchingCostFunction _cost_function )
        : M_new_sample( &_new_sample )
        , M_storage( &_storage )
        , M_comparable_sample_ids( _sample_begin, _sample_end )
        , M_cost_function( _cost_function )
    {
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< SampleIdInputIterator, formation_sample_id_type>::value ),
                                "Sample id descriptor types mismatch");

    }

    long operator() ( const sample_player_id_type pid1,
                      const sample_player_id_type pid2 )
    {
        const auto _p1s = soccer::get_player( pid1, *M_new_sample );

        auto total_cost = 0.0;
        for ( auto its = M_comparable_sample_ids.begin() ; its != M_comparable_sample_ids.end(); its++ )
        {
            const auto _existing_sample = soccer::get_sample( *its, *M_storage );
            const auto _p2s = soccer::get_player( pid2,  _existing_sample );
            const auto _cost = M_cost_function( _p1s, _p2s );
            total_cost += _cost;
        }

        return total_cost; // Cost function cannot use decimal values => using decimal numbers to compose an integer cost with sufficient precision
    }
};


/*!
 * \brief The player_inter_state_distance struct
 * \tparam State models the state of a soccer match
 */
template < typename State >
struct player_inter_state_distance
    : public std::binary_function< int, int, long >
{
    // HACK
    typedef int player_id_t;

    State const* M_s1;
    State const* M_s2;
    player_inter_state_distance( State const& s1,
                                 const State &s2 )
        : M_s1( &s1 )
        , M_s2( &s2 )
    {

    }

    long operator()( const player_id_t pid_s1, const player_id_t pid_s2 )
    {
        const auto& _ps1 = soccer::get_player( pid_s1, *M_s1 );
        const auto& _ps2 = soccer::get_player( pid_s2, *M_s2 );
        // TODO Non-generic dist call
        const auto dist = soccer::get_position( _ps1 ).dist( soccer::get_position( _ps2 ) );
        return dist * 100.0; // Cost function cannot use decimal values => using decimal numbers to compose an integer cost with sufficient precision
    }
};

/*!
 * \brief The player_inter_state_distance struct
 * \tparam State models the state of a soccer match
 */
template < typename State >
struct player_inter_state_teammate_aware_cost
    : public std::binary_function< int, int, long >
{
    // HACK
    typedef int player_id_t;

    State const* M_s1;
    State const* M_s2;
    rcss::rcg::Side M_side;
    player_inter_state_teammate_aware_cost( State const& s1,
                                            const State &s2 )
        : M_s1( &s1 )
        , M_s2( &s2 )
    {
        // TODO Pre-calculate distance from all players in P1 to all players in P2 (or use an external readable property map)
    }

    long operator()( const player_id_t pid_s1, const player_id_t pid_s2 )
    {
        const auto& _ps1 = soccer::get_player( pid_s1, *M_s1 );
        const auto& _ps2 = soccer::get_player( pid_s2, *M_s2 );

        // HACK non_generic
        if ( _ps1.isGoalie() )
        {
            if ( _ps2.isGoalie() )
            {
                return 0;
            }
            return 1000;// std::numeric_limits<long>::max(); // Force goalie not to swap with other players except the goali
        }

        // INNEFICIENT Calculate distances everytime or only called once? Store prev. calcs
        std::map< int, double > _player_dists;
        auto _team_players_iterators = soccer::get_players( soccer::get_side( _ps1 ), *M_s1 );
        for ( auto itp = _team_players_iterators.first; itp != _team_players_iterators.second; itp++ )
        {
            _player_dists[*itp] = soccer::get_position( _ps2 ).dist( soccer::get_position( soccer::get_player( *itp, *M_s1 ) ) );
        }

        auto _max_dist = std::max_element( _player_dists.begin(),
                                           _player_dists.end(),
                                           [&] (const std::pair<int, double>& kvp1, const std::pair<int, double>& kvp2 )
                                                { return kvp1.second < kvp2.second;  } );
        auto _min_dist = std::min_element( _player_dists.begin(),
                                           _player_dists.end(),
                                           [&] (const std::pair<int, double>& kvp1, const std::pair<int, double>& kvp2 )
                                                { return kvp1.second < kvp2.second;  } );
        auto _half_dist = ( _max_dist->second - _min_dist->second ) / 2.0;
        auto _player_cost = fabs(_player_dists[ pid_s1 ] - _min_dist->second - _half_dist);
        return _player_cost;
    }
};

template < typename CostFunctor,
           typename Side,
           typename PlayerIdMatchingMap >
long inter_state_matching_cost( CostFunctor cost_functor,
                                const Side side,
                                PlayerIdMatchingMap pmap )
{
    // TODO HACK
    typedef int player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::ReadablePropertyMapConcept<PlayerIdMatchingMap, player_id_t> ));

    auto total_cost = 0;
    const auto pid_offset = side == rcss::rcg::LEFT ? 0 : rcss::rcg::MAX_PLAYER;
    for ( auto i = 0u; i < rcss::rcg::MAX_PLAYER ; i++ )
    {
        total_cost += cost_functor( i + pid_offset, boost::get( pmap, i ) + pid_offset );
    }

    return total_cost;
}

/*!
 * \brief formation_sample_matching
 * \param begin_pid1
 * \param end_pid1
 * \param begin_pid2
 * \param end_pid2
 * \param cost_functor
 * \param pmap
 * \return
 */
template < typename PlayerId1InputIterator,
           typename PlayerId2InputIterator,
           typename CostFunctor,
           typename PlayerIdMatchingMap >
long formation_sample_matching( PlayerId1InputIterator begin_pid1, const PlayerId1InputIterator end_pid1,
                                PlayerId2InputIterator begin_pid2, const PlayerId2InputIterator end_pid2,
                                CostFunctor cost_functor,
                                PlayerIdMatchingMap pmap // from players in [begin_pid1,end_pid1[ to [begin_pid2,end_pid2[
                              )
{

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerId1InputIterator> ));
    typedef typename boost::iterator_value< PlayerId1InputIterator >::type player_id1_descriptor;

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerId2InputIterator> ));
    typedef typename boost::iterator_value< PlayerId2InputIterator >::type player_id2_descriptor;

    BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept<PlayerIdMatchingMap, player_id1_descriptor> ));

    BOOST_STATIC_ASSERT(( boost::is_same< typename boost::property_traits< PlayerIdMatchingMap >::value_type,
                                          player_id2_descriptor>::value,
                          "PlayerIdMatchingMap value types mismatch"));

    typedef boost::adjacency_list_traits < boost::vecS, boost::vecS, boost::directedS > adjacency_list_traits;

    typedef boost::no_property VertexProperty;

    typedef boost::property< boost::edge_capacity_t, long,
                boost::property< boost::edge_residual_capacity_t, long,
                    boost::property< boost::edge_reverse_t, typename adjacency_list_traits::edge_descriptor,
                        boost::property< boost::edge_weight_t, long
                        >
                    >
                >
            >
            EdgeProperty;

    typedef boost::adjacency_list< boost::vecS,
                                   boost::vecS,
                                   boost::directedS,
                                   VertexProperty,
                                   EdgeProperty > graph;

    typedef boost::property_map < graph, boost::edge_capacity_t >::type CapacityMap;
    typedef boost::property_map < graph, boost::edge_residual_capacity_t >::type ResidualCapacityMap;
    typedef boost::property_map < graph, boost::edge_weight_t >::type WeightMap;
    typedef boost::property_map < graph, boost::edge_reverse_t>::type ReverseEdgeMap;
    typedef boost::graph_traits<graph>::vertices_size_type vertices_size_type;
    typedef graph::vertex_descriptor vertex_descriptor;
    typedef graph::edge_descriptor edge_descriptor;
    typedef graph::edge_iterator edge_iterator;

    typedef std::map< player_id1_descriptor, vertex_descriptor > pid1_to_vertex_matching;
    typedef std::map< player_id2_descriptor, vertex_descriptor > pid2_to_vertex_matching;

    pid1_to_vertex_matching source_pid_to_vertex_map;
    pid2_to_vertex_matching target_pid_to_vertex_map;

    vertices_size_type _num_players1 = std::distance( begin_pid1, end_pid1 );
    vertices_size_type _num_players2 = std::distance( begin_pid2, end_pid2 );
    assert( _num_players1 == _num_players2 ); // HACK Should only work for equal size sets of players
    graph _player_graph( _num_players1 );

    CapacityMap  capacity_map = get( boost::edge_capacity, _player_graph );
    ReverseEdgeMap edge_reverse_map = get( boost::edge_reverse, _player_graph );
    ResidualCapacityMap residual_capacity_map = get( boost::edge_residual_capacity, _player_graph );
    WeightMap weight_map = get(boost::edge_weight, _player_graph );

    auto s_vd = boost::add_vertex( _player_graph );
    auto t_vd = boost::add_vertex( _player_graph );

    auto add_edge_fn = [&] ( const vertex_descriptor s,
                             const vertex_descriptor t,
                             const double _weight,
                             const long _capacity )
    {
            bool b = false;
            edge_descriptor ed;

            boost::tie(  ed, b ) = boost::add_edge( s, t, _player_graph );
            if ( !b )
            {
                std::cerr << "The edge (" << s << "," << t << ") with weight=" << _weight << std::endl;
                std::abort();
            }
            weight_map[ed] = _weight;
            capacity_map[ed] = _capacity;
            return ed;
    };

    auto add_edge_rev_fn = [&] ( const vertex_descriptor s,
                                 const vertex_descriptor t,
                                 const double _weight = 0 )
    {
            auto e1 = add_edge_fn( s, t, _weight, 1 );
            auto e2 = add_edge_fn( t, s, -_weight, 0 );
            edge_reverse_map[e1] = e2;
            edge_reverse_map[e2] = e1;
    };

    // Save mapping of player ids to vertex ids
    for ( auto itp1 = begin_pid1; itp1 != end_pid1 ; itp1++ )
    {
        //
        // Create source vertices
        //
        auto source_pid_vd = boost::add_vertex( _player_graph );
        source_pid_to_vertex_map[*itp1]  = source_pid_vd;
        // Add edges from source to the player vertex
        add_edge_rev_fn( s_vd, source_pid_vd );
    }

    // Save mapping of player ids to vertex ids
    for ( auto itp2 = begin_pid2; itp2 != end_pid2 ; itp2++ )
    {
        //
        // Create target vertices
        //
        auto target_pid_vd = boost::add_vertex( _player_graph );
        target_pid_to_vertex_map[*itp2]  = target_pid_vd;

        // Add edges from target to the player vertex
        add_edge_rev_fn( target_pid_vd, t_vd );
    }

    // Create edges between player ids
    for ( auto its = source_pid_to_vertex_map.begin() ; its != source_pid_to_vertex_map.end(); its++ )
    {
        for ( auto itt = target_pid_to_vertex_map.begin() ; itt != target_pid_to_vertex_map.end(); itt++ )
        {
            // CostFunction => abstract this
            auto weight = cost_functor( its->first, itt->first );

            add_edge_rev_fn( its->second, itt->second, weight );
        }
    }

    boost::successive_shortest_path_nonnegative_weights( _player_graph, s_vd, t_vd );

#if defined(DEBUG_PLAYER_MATCHING_RESULT_WEIGHT) || \
    defined(DEBUG_PLAYER_MATCHING_RESULT_CAPACITY) || \
      defined(DEBUG_PLAYER_MATCHING_RESULT_RESIDUAL_CAPACITY)

    std::cout << "All Edge Weights in Matrix based format => each cell is a tuple (weight, capacity, residual_capacity)" << std::endl;
    for ( auto it1 = source_pid_to_vertex_map.begin(); it1 != source_pid_to_vertex_map.end(); it1++ )
    {
        for ( auto it2 = target_pid_to_vertex_map.begin(); it2 != target_pid_to_vertex_map.end(); it2++ )
        {
            bool b = false;
            edge_descriptor ed;
            boost::tie( ed, b ) = boost::edge( it1->second, it2->second, _player_graph );
            assert( b );
            #if defined(DEBUG_PLAYER_MATCHING_RESULT_WEIGHT) && \
                !defined(DEBUG_PLAYER_MATCHING_RESULT_CAPACITY) && \
                !defined(DEBUG_PLAYER_MATCHING_RESULT_RESIDUAL_CAPACITY)
                std::cout << weight_map[ed] << " ";
            #else
                std::cout << "(" << weight_map[ed]
                          << "," << capacity_map[ed]
                          << "," << residual_capacity_map[ed]
                          << ") ";
            #endif
        }
        std::cout << std::endl;
    }
#endif

    std::cout << "Weights=";
    for ( auto its = source_pid_to_vertex_map.begin() ; its != source_pid_to_vertex_map.end(); its++ )
    {
        for ( auto itt = target_pid_to_vertex_map.begin() ; itt != target_pid_to_vertex_map.end() ; itt++ )
        {
            bool b = false;
            edge_descriptor ed;
            boost::tie( ed, b ) = boost::edge( its->second,
                                               itt->second,
                                               _player_graph );
            assert( b );
            if ( residual_capacity_map[ed] == 0 )
            {
                // Found matching for player => there can be only one
                // boost::put( pmap, it1, std::make_pair( it2, weight_map[ed] ) );
                boost::put( pmap, its->first, itt->first );
                std::cout << "[(" << its->first << "," << itt->first << ")=" << weight_map[ed] << "] ";
            }
        }
    }
    std::cout << std::endl;

    auto cost =  boost::find_flow_cost( _player_graph );

    return cost;
}

/*!
 * \brief calculate_sample_matching
 * \param begin_pid iterator to the first player id
 * \param end_pid iterator to the end player id
 * \param cost_functor cost functor
 * \param pmap writable property map of player ids within the range [begin_pid, end_pid]
 */
template < typename PlayerIdInputIterator,
           typename CostFunctor,
           typename PlayerIdMatchingMap >
long formation_sample_matching( PlayerIdInputIterator begin_pid,
                                const PlayerIdInputIterator end_pid,
                                  CostFunctor cost_functor,
                                  PlayerIdMatchingMap pmap
                                 )
{
    return formation_sample_matching( begin_pid, end_pid,
                                      begin_pid, end_pid,
                                      cost_functor,
                                      pmap );
}


/*!
 * \brief Create default_player_id_map
 */
template < typename Sample1,
           typename Sample2 >
std::map< typename std::iterator_traits< typename decltype(soccer::get_players(Sample1{}))::first_type >::value_type,
          typename std::iterator_traits< typename decltype(soccer::get_players(Sample2{}))::first_type >::value_type >
default_player_id_map()
{
    typedef typename std::iterator_traits< typename decltype(soccer::get_players(Sample1{}))::first_type >::value_type player_id1_type;
    typedef typename std::iterator_traits< typename decltype(soccer::get_players(Sample2{}))::first_type >::value_type player_id2_type;
    return std::map< player_id1_type, player_id2_type >();
}

/*!
 * \brief Create default_player_id_cost_map
 */
template < typename Sample1,
           typename Sample2>
std::map< std::pair<
                        typename std::iterator_traits< typename decltype(soccer::get_players(Sample1{}))::first_type >::value_type,
                        typename std::iterator_traits< typename decltype(soccer::get_players(Sample2{}))::first_type >::value_type
                    >,
          long
        >
default_player_id_cost_map()
{
    typedef typename std::iterator_traits< typename decltype(soccer::get_players(Sample1{}))::first_type >::value_type player_id1_type;
    typedef typename std::iterator_traits< typename decltype(soccer::get_players(Sample2{}))::first_type >::value_type player_id2_type;
    return std::map< std::pair< player_id1_type, player_id2_type>, long >();
}

BOOST_PARAMETER_FUNCTION(
      (long),                // 1. parenthesized return type
      test_formation_sample_matching,    // 2. name of the function template

      tag,                   // 3. namespace of tag types

       // 4. Function signature (2 required parameter, and
      (required
        (sample1, *)
        (sample2, *)
      )

      (optional                         //    2 optional parameters, with defaults
        (out(player_id_map),      *, default_player_id_map( sample1, sample2 ))
        (player_id_cost_map, *, default_player_id_cost_map( sample1, sample2 ))
      )
  )
{
    /* auto _its1 = soccer::get_players( sample1 );
    auto _its2 = soccer::get_players( sample1 );
    return formation_sample_matching( _its1.first, _its1.second,
                                      _its2.first, _its2.second,
                                      player_id_map,
                                      player_id_cost_map ); */
}


/*!
 * \brief remove_player_crossings_in_matching
 * \tparam model of CGAL Kernel
 * \param player_matchings mapping of player matchins
 * \param s1 source state
 * \param s2 target state
 */
template <
            typename FormationSample,
            typename Kernel = ::CGAL::Simple_cartesian< double >
         >
void remove_player_crossings_in_matching( std::map< typename soccer::player_id_type< FormationSample >::type,
                                                    typename soccer::player_id_type< FormationSample >::type >& player_matchings,
                                          const FormationSample& source,
                                          const FormationSample& target )
{
    typedef typename Kernel::Segment_2 Segment;
    typedef typename Kernel::Point_2 Point;

    typedef typename soccer::player_id_type< FormationSample >::type player_id_t;

    typedef boost::adjacency_list_traits < boost::vecS, boost::vecS, boost::directedS > adjacency_list_traits;

    typedef boost::no_property VertexProperty;

    typedef boost::property< boost::edge_weight_t, long > EdgeProperty;

    typedef boost::adjacency_list< boost::vecS,
                                   boost::vecS,
                                   boost::directedS,
                                   VertexProperty,
                                   EdgeProperty > graph;

    typedef boost::property_map < graph, boost::edge_weight_t >::type WeightMap;
    typedef boost::graph_traits<graph>::vertices_size_type size_type;
    typedef graph::vertex_descriptor vertex_descriptor;
    typedef graph::edge_descriptor edge_descriptor;
    typedef graph::edge_iterator edge_iterator;

    std::map< player_id_t, vertex_descriptor > _pid_to_vertex_map;

    graph _player_crossings_graph;
    WeightMap weight_map = get(boost::edge_weight, _player_crossings_graph );
    // initialize graph with vertices corresponding to player ids
    for ( auto it = player_matchings.begin(); it != player_matchings.end(); it++ )
    {
        _pid_to_vertex_map.insert( std::make_pair( it->first , boost::add_vertex( _player_crossings_graph ) ) );
    }

    std::map< std::pair< player_id_t, player_id_t >, Segment > _player_match_segments;
    // Create segments for each matching
    for ( auto itkvp = player_matchings.begin(); itkvp != player_matchings.end() ; itkvp++ )
    {
        const auto _p1_pos = get_position( get_player(itkvp->first, source ));
        const auto _p2_pos = get_position( get_player(itkvp->second, source ));
        Point p1( get_x( _p1_pos), get_y( _p1_pos) );
        Point p2( get_x( _p2_pos), get_y( _p2_pos) );
        Segment _player_match_segment( p1, p2 );
        _player_match_segments.insert( std::make_pair( *itkvp, _player_match_segment ) );
    }

    // Determine nearest intersecting segment if any

    edge_descriptor ed;
    bool _edge_added = false;
    size_t _point_crossings_found = 0u;
    size_t _segment_crossings_found = 0u;
    for ( auto itkvp1 = player_matchings.begin(); itkvp1 != player_matchings.end() ; itkvp1++ )
    {
        auto itkvp2 = itkvp1;
        itkvp2++;
        std::vector< std::pair<int, int > > _intersections;
        for ( ; itkvp2 != player_matchings.end() ; itkvp2++ )
        {
            // Check for intersecting line segments
            auto _intersection_res = ::CGAL::intersection( _player_match_segments[*itkvp1],
                                                           _player_match_segments[*itkvp2] );
            Point point;
            Segment segment;
            if ( ::CGAL::assign(point, _intersection_res) )
            {
                _point_crossings_found++;
                // Register change in the player crossings graph
                boost::tie( ed, _edge_added ) = boost::add_edge( _pid_to_vertex_map[itkvp1->first],
                                                                 _pid_to_vertex_map[itkvp2->first],
                                                                 _player_crossings_graph );
                weight_map[ed] = 1; // Register a cost with the edge (TODO Should have saved the previous costs)
                assert( _edge_added );

                //do something with point (1,2) (2,1) => (1,1) (2,2)
                _intersections.push_back( *itkvp2 );
                std::cout << "Point intersection between ";
                // std::cout << "(" << itkvp1->first << "," << itkvp1->second << ")";
                std::cout << "(" << soccer::get_number( soccer::get_player( itkvp1->first , source ) ) << "," << soccer::get_number( soccer::get_player( itkvp1->second, target ) ) << ")";
                std::cout << "[(" << _player_match_segments[*itkvp1].source().x() << "," << _player_match_segments[*itkvp1].source().y() << ")";
                std::cout << "(" << _player_match_segments[*itkvp1].target().x() << "," << _player_match_segments[*itkvp1].target().y() << ")]";
                std::cout << " and ";
                // std::cout << "(" << itkvp2->first << "," << itkvp2->second << ")";
                std::cout << "(" << soccer::get_number( soccer::get_player( itkvp2->first , source ) ) << "," << soccer::get_number( soccer::get_player( itkvp2->second, target ) ) << ")";
                std::cout << "[(" << _player_match_segments[*itkvp2].source().x() << "," << _player_match_segments[*itkvp2].source().y() << ")";
                std::cout << "(" << _player_match_segments[*itkvp2].target().x() << "," << _player_match_segments[*itkvp2].target().y() << ")]";
                std::cout << " at ";
                std::cout << "(" << point.x() << "," << point.y() << ")";
                std::cout << std::endl;
                const auto dist_kvp1 = ::CGAL::sqrt( ::CGAL::squared_distance( _player_match_segments[*itkvp1].source(),
                                                                           _player_match_segments[*itkvp1].target()));
                const auto dist_kvp2 = ::CGAL::sqrt( ::CGAL::squared_distance( _player_match_segments[*itkvp2].source(),
                                                                           _player_match_segments[*itkvp2].target()));
                const auto dist_swap1 = ::CGAL::sqrt( ::CGAL::squared_distance( _player_match_segments[*itkvp1].source(),
                                                                            _player_match_segments[*itkvp2].target()));
                const auto dist_swap2 = ::CGAL::sqrt( ::CGAL::squared_distance( _player_match_segments[*itkvp2].source(),
                                                                            _player_match_segments[*itkvp1].target()));

                std::cout << "Distances ";
                std::cout << "(" << itkvp1->first << "," << itkvp1->second << ")=" << dist_kvp1;
                std::cout << " (" << itkvp2->first << "," << itkvp2->second << ")=" << dist_kvp2;
                std::cout << " total=" << dist_kvp1 + dist_kvp2;
                std::cout << " new ";
                std::cout << "(" << itkvp1->first << "," << itkvp2->second << ")=" << dist_swap1;
                std::cout << " (" << itkvp2->first << "," << itkvp1->second << ")=" << dist_swap2;
                std::cout << " total=" << dist_swap1 + dist_swap2;
                std::cout << " loss=" << (dist_swap1 + dist_swap2) - ( dist_kvp1 + dist_kvp2 );
                std::cout << std::endl;
                // std::swap( itkvp1->second, itkvp2->second );
            }
            else if (assign(segment, _intersection_res))
            {
                _segment_crossings_found++;
                // Register change in the player crossings graph
                boost::tie( ed, _edge_added ) = boost::add_edge( _pid_to_vertex_map[itkvp1->first],
                                                                 _pid_to_vertex_map[itkvp2->first],
                                                                 _player_crossings_graph );
                weight_map[ed] = 1; // TODO Register a cost with the edge (Should have saved the previous costs)
                assert( _edge_added );

                // do something with segment
                _intersections.push_back( *itkvp2 );
                std::cout << "Segment intersection between ";
                std::cout << "(" << itkvp1->first << "," << itkvp1->second << ")";
                std::cout << "[(" << _player_match_segments[*itkvp1].source().x() << "," << _player_match_segments[*itkvp1].source().y() << ")";
                std::cout << "(" << _player_match_segments[*itkvp1].target().x() << "," << _player_match_segments[*itkvp1].target().y() << ")]";
                std::cout << " and ";
                std::cout << "(" << itkvp2->first << "," << itkvp2->second << ")";
                std::cout << "[(" << _player_match_segments[*itkvp2].source().x() << "," << _player_match_segments[*itkvp2].source().y() << ")";
                std::cout << "(" << _player_match_segments[*itkvp2].target().x() << "," << _player_match_segments[*itkvp2].target().y() << ")]";
                std::cout << " at ";
                std::cout << "[(" << segment.source().x() << "," << segment.source().y() << ")";
                std::cout << "(" << segment.target().x() << "," << segment.target().y() << ")]";
                std::cout << std::endl;
                // std::swap( itkvp1->second, itkvp2->second );
            }
            else
            {
                // no intersection
            }

        }
    }

    const auto _total_crossings_found = _point_crossings_found + _segment_crossings_found;
    if ( _total_crossings_found > 0u )
    {
        // Remove all vertices with no out edges

        std::cout << "Time(" << soccer::get_time( source ) << "," << soccer::get_time( target ) << ") PLAYER_CROSSINGS[" << _total_crossings_found << "]=";
        edge_iterator ef, el;
        boost::tie( ef, el ) = boost::edges( _player_crossings_graph );
        std::for_each( ef, el,
                       [&] ( edge_descriptor ed )
                        {
                       std::cout << "("
                                 << std::find_if( _pid_to_vertex_map.begin(),
                                                  _pid_to_vertex_map.end(),
                                                  [&] ( const std::pair< player_id_t, vertex_descriptor>& kvp )
                                                    { return kvp.second == boost::source( ed, _player_crossings_graph ); } )->first + 1
                                 << ","
                                 << std::find_if( _pid_to_vertex_map.begin(),
                                                  _pid_to_vertex_map.end(),
                                                  [&] ( const std::pair< player_id_t, vertex_descriptor>& kvp )
                                                       { return kvp.second == boost::target( ed, _player_crossings_graph ); } )->first + 1
                                 << ")";
                        }
                     );
        std::cout << std::endl;
        auto _vertex_property_writer
                =  [&] ( std::ostream& out, vertex_descriptor vd )
                    {
                        auto itf = std::find_if( _pid_to_vertex_map.begin(),
                                                 _pid_to_vertex_map.end(),
                                                 [&] ( const std::pair< player_id_t, vertex_descriptor >& kvp ) { return kvp.second == vd; } );
                        assert( itf != _pid_to_vertex_map.end() );
                        out << "[label=\"" << itf->first << "\"]";
                    };
        auto _edge_property_writer
                = [&] ( std::ostream& out, edge_descriptor ed )
                 {
                     out << "[label=" << weight_map[ed] << "]";
                     // out << "[weight=" << weight_map[ed] << "]"; // TODO use distances
                 };
        auto _graph_property_writer
                = [&] ( std::ostream& out )
                {
                    out << "label=\"Time=(" << soccer::get_time( source ) << ","
                        << soccer::get_time( target ) << ")\"" << std::endl;
                    out << "graph [bgcolor=white]" << std::endl;
                    out << "node [shape=circle color=black]" << std::endl;
                    out << "edge [style=solid]" << std::endl;
                };
        std::stringstream ss_graph_viz_filename;
        ss_graph_viz_filename << "graphs/source_" << soccer::get_time( source )
                              << "_target_" << soccer::get_time( target )
                              << ".dot";
        std::ofstream ofs( ss_graph_viz_filename.str() );
        boost::write_graphviz( ofs,
                               _player_crossings_graph,
                               _vertex_property_writer,
                               _edge_property_writer,
                               _graph_property_writer);
    }
    else
    {
        std::cout << "Time(" << soccer::get_time( source ) << "," << soccer::get_time( target ) << ") NO PLAYER CROSSINGS" << std::endl;
    }

}


} // end namespace modeling
} // end namespace soccer


#endif // SOCCER_FORMATION_MODELING_SAMPLE_MATCHING_H
