#ifndef SOCCER_FORMATION_SAMPLE_PROXY_HPP
#define SOCCER_FORMATION_SAMPLE_PROXY_HPP

#include "formation_sample_concept.hpp"

#include <soccer/core/access.hpp>

#include <cassert>

namespace soccer {

//
// FormationSampleProxy
//
template < typename FormationSample >
struct formation_sample_proxy {
    BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));

    FormationSample* M_sample;

    formation_sample_proxy( )
        : M_sample( NULL )
    {

    }

    formation_sample_proxy( FormationSample& sample )
        : M_sample( &sample )
    {

    }

    std::pair< typename player_iterator_type< FormationSample >::type,
               typename player_iterator_type< FormationSample >::type >
    get_players( ) const
    {
        assert( M_sample != NULL );

        return ::soccer::get_players( *M_sample );
    }

    typename ball_type< FormationSample >::type
    get_ball( ) const
    {
        assert( M_sample != NULL );

        return ::soccer::get_ball( *M_sample );
    }

    typename player_type< FormationSample >::type const&
    get_player( const typename player_id_type< FormationSample >::type pid ) const
    {
        assert( M_sample != NULL );

        return ::soccer::get_player( pid, *M_sample );
    }

    typename info_type< FormationSample>::type
    get_info( ) const
    {
        assert( M_sample != NULL );

        return ::soccer::get_info( *M_sample );
    }

    //
    // Mutable Concept
    //
    void set_ball( const typename ball_type< FormationSample >::type& _ball )
    {
        assert( M_sample != NULL );

        ::soccer::set_ball( _ball, *M_sample );
    }

    void set_player( const typename player_id_type< FormationSample >::type _pid,
                     const typename player_type< FormationSample >::type& _player )
    {
        assert( M_sample != NULL );

        ::soccer::set_player( _pid, _player, *M_sample );
    }

    void set_info( const typename player_id_type< FormationSample >::type& _info )
    {
        assert( M_sample != NULL );

        ::soccer::set_info( _info, *M_sample );
    }

};

/*!
 * \brief make_formation_sample_proxy
 */
template < typename FormationSample >
formation_sample_proxy< FormationSample >
make_formation_sample_proxy( FormationSample& sample )
{
    return formation_sample_proxy< FormationSample >( sample );
}


} // end namespace soccer


#include "register/formation_sample.hpp"

// formation_sample_proxy REGISTRATION
SOCCER_TRAITS_NAMESPACE_BEGIN

// FORMATION SAMPLE PROXY TYPE TRAITS REGISTRATION
template <>
template < typename FormationSample >
struct tag< soccer::formation_sample_proxy< FormationSample > >
{
    typedef formation_sample_tag type;
};

template <>
template < typename FormationSample >
struct ball_type< soccer::formation_sample_proxy< FormationSample > >
{
    typedef typename ball_type< FormationSample >::type type;
};

template <>
template < typename FormationSample >
struct player_type< soccer::formation_sample_proxy< FormationSample > >
{
    typedef typename player_type< FormationSample >::type type;
};

template <>
template < typename FormationSample >
struct player_id_type< soccer::formation_sample_proxy< FormationSample > >
{
    typedef typename player_id_type< FormationSample >::type type;
};

template <>
template < typename FormationSample >
struct player_iterator_type< soccer::formation_sample_proxy< FormationSample > >
{
    typedef typename player_iterator_type< FormationSample >::type type;
};

template <>
template < typename FormationSample >
struct info_type< soccer::formation_sample_proxy< FormationSample > >
{
    typedef typename info_type< FormationSample >::type type;
};

// GENERIC FORMATION SAMPLE ACCESS METHODS REGISTRATION
template <>
template < typename FormationSample >
struct ball_access< soccer::formation_sample_proxy< FormationSample > >
{

    static
    inline typename ball_type< FormationSample >::type
    get( soccer::formation_sample_proxy< FormationSample > const& obj )
    {
        return obj.get_ball();
    }

    static
    inline void set( typename ball_type< FormationSample >::type const& val,
                     soccer::formation_sample_proxy< FormationSample >& obj )
    {
        obj.set_ball( val );
    }
};

template <>
template < typename FormationSample >
struct player_iterators_access< soccer::formation_sample_proxy< FormationSample > >
{

    static
    inline std::pair< typename player_iterator_type< FormationSample >::type,
                      typename player_iterator_type< FormationSample >::type >
    get( soccer::formation_sample_proxy< FormationSample > const& obj )
    {
        return obj.get_players();
    }
};

template <>
template < typename FormationSample >
struct player_access< soccer::formation_sample_proxy< FormationSample > >
{
    static
    inline typename soccer::player_type< FormationSample >::type
    get( typename soccer::player_id_type< FormationSample >::type const& pid,
         soccer::formation_sample_proxy< FormationSample > const& obj )
    {
        return obj.get_player( pid );
    }

    static
    inline void set( typename soccer::player_id_type< FormationSample >::type const pid,
                     typename soccer::player_type< FormationSample >::type const& val,
                     soccer::formation_sample_proxy< FormationSample >& obj )
    {
        obj.set_player( pid, val );
    }
};

template <>
template < typename FormationSample >
struct info_access< soccer::formation_sample_proxy< FormationSample > >
{

    static
    inline typename info_type< FormationSample >::type
    get( soccer::formation_sample_proxy< FormationSample > const& obj )
    {
        return obj.get_info();
    }

    static
    inline void set( typename info_type< FormationSample >::type const& val,
                     soccer::formation_sample_proxy< FormationSample >& obj )
    {
        obj.set_info( val );
    }
};

SOCCER_TRAITS_NAMESPACE_END


#endif // SOCCER_FORMATION_SAMPLE_PROXY_HPP
