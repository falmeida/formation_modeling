#ifndef SOCCER_BOOST_GEOMETRY_GEOMETRIES_ADAPTED_RCSC_VECTOR2D_AS_POINT_HPP
#define SOCCER_BOOST_GEOMETRY_GEOMETRIES_ADAPTED_RCSC_VECTOR2D_AS_POINT_HPP

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>

#include <rcsc/geom/vector_2d.h>

BOOST_GEOMETRY_REGISTER_POINT_2D(rcsc::Vector2D, double, boost::geometry::cs::cartesian, x, y)

#endif // SOCCER_BOOST_GEOMETRY_GEOMETRIES_ADAPTED_RCSC_VECTOR2D_AS_POINT_HPP
