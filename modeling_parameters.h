#ifndef SOCCER_MODELING_PARAMETERS_H
#define SOCCER_MODELING_PARAMETERS_H

#include <boost/program_options.hpp>

#include <string>

#include <vector>

#include <iostream>

namespace soccer {
namespace modeling {

struct modeling_parameters
{
    typedef enum {
        Defensive,
        Offensive
    } ModelingStrategy;

    modeling_parameters();

    bool parse( const std::string &config_file_path );
    bool parse( int argc, char** argv );

    bool check_parameters( const boost::program_options::variables_map& vm );

    const std::string& teamNameRegex() const;
    const std::string& outputFormationFile() const;
    const std::string& logfileNameRegex() const;
    const std::vector< std::string>& logFileSearchPaths() const;
    const std::vector< std::string>& logFilesPaths() const;
    bool recursiveLookup() const;
    const std::vector< std::string>& inputFormationPaths() const;
    ModelingStrategy modelingStrategy() const;

    boost::program_options::options_description M_options;

    //!< name of file to save the inferred formation configuration
    std::string M_output_formation_file;
    //!< regular expression to match against teamnames (from left to right)
    std::string M_teamname_regex;
    //!< regular expression to match against logfile names
    std::string M_logfile_name_regex;
    //!< set of directories to search for logfiles
    std::vector< std::string > M_logfile_search_paths;
    //!< set of logfile paths to use
    std::vector< std::string > M_logfiles_paths;
    //!< flag to decide if directory should be looked up recursively
    bool M_recursive_lookup;

    //!< set of formation paths to formation models to test the similarity against
    std::vector< std::string > M_input_formation_paths;

    //!< modeling strategy
    std::string M_modeling_strategy;
    // TODO
    // Modeling strategy
    // Matching cost function
};

}
}

std::ostream& operator<<( std::ostream& os,
                          const soccer::modeling::modeling_parameters& _modeling_params );

#endif // SOCCER_MODELING_PARAMETERS_H
