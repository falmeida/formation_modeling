#ifndef RCSC_FORMATION_MODELING_STRATEGIES_H
#define RCSC_FORMATION_MODELING_STRATEGIES_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "rcsslogplayer/rcg_position_modeler.h"
#include <soccer/rcsslogplayer/rcsslogplayer_disp_info.h>
#include <soccer/rcsslogplayer/rcsslogplayer_disp_info_predicates.h>

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "formation_sample_data.h"

#include "state_evaluation.h"

#include "formation_sample_evaluation.hpp"

#include <CGAL/squared_distance_2.h>

#include <robocup/2d/game_log.h>

#include <boost/iterator/filter_iterator.hpp>

#include <vector>
#include <set>

#include <functional>
#include <type_traits>

namespace soccer {

template < typename Id >
/*!
 * \brief Generic value generator
 * \tparam Id model suitable for generating identifier values (distinct) with sufficient length
 */
struct formation_sample_player_id_generator
    : public std::unary_function< Id, void  >{

    formation_sample_player_id_generator()
    {

    }

    formation_sample_player_id_generator( const Id start_value )
        : M_start_value( start_value )
        , M_current_value( start_value )
    {

    }

    Id operator()( )
    {
        auto _value = M_current_value;
        M_current_value++;
        return _value;
    }

    Id M_start_value;
    Id M_current_value;
};


template < >
/*!
 * \brief Type generator for a formation sample to use with rcss::rcg::DispInfoT
 */
struct formation_sample_generator< rcss::rcg::DispInfoT, std::pair< robocup::soccer::GameLog, rcss::rcg::DispInfoT > > {
    typedef soccer::formation_sample< unsigned int,
                                      rcss::rcg::BallT,
                                      rcss::rcg::PlayerT,
                                      std::pair< robocup::soccer::GameLog, rcss::rcg::DispInfoT >,
                                      soccer::formation_sample_tag
                                    > type;
};

template <>
struct formation_sample_generator< rcss::rcg::DispInfoT >
{
    typedef formation_sample< unsigned int,
                              rcss::rcg::BallT,
                              rcss::rcg::PlayerT,
                              std::pair< robocup::soccer::GameLog, rcss::rcg::DispInfoT >,
                              soccer::formation_sample_tag
                            > type;
};

}

namespace soccer {

/*!
 * \brief Generic formation modeler
 * \tparam StatePreprocessor
 * \tparam StateSelectorPredicate
 * \tparam FormationSampleCreator
 * \tparam FormationSampleIsSignificantPredicate
 * \tparam FormationSampleStorage
 */
template < typename StatePreprocessor,
           typename StateSelectorPredicate,
           typename FormationSampleCreator,
           typename FormationSampleIsSignificantPredicate,
           typename FormationSampleStorage
         >
struct formation_modeler
{
    typedef StatePreprocessor state_preprocessor_type;
    typedef StateSelectorPredicate state_selector_predicate_type;
    typedef FormationSampleCreator formation_sample_creator_type;
    typedef FormationSampleIsSignificantPredicate formation_sample_is_significant_predicate_type;
    typedef FormationSampleStorage formation_sample_storage_type;

    StatePreprocessor M_state_preprocessor;
    StateSelectorPredicate M_state_selector_predicate;
    FormationSampleCreator M_formation_sample_creator;
    FormationSampleIsSignificantPredicate M_formation_sample_is_significant;
    FormationSampleStorage M_formation_sample_storage_policy;

    formation_modeler( )
    {

    }

    template < typename StatePreprocessorFn >
    void set_state_preprocessor( StatePreprocessorFn _state_preprocessor )
        { this->M_state_preprocessor = _state_preprocessor; }

    template < typename StateSelectorPredicateFn >
    void set_state_selector_predicate( StateSelectorPredicateFn _state_selector_predicate )
        { this->M_state_selector_predicate = _state_selector_predicate ; }

    template < typename FormationSampleCreatorFn >
    void set_formation_sample_creator( FormationSampleCreatorFn _formation_sample_creator )
        { this->M_formation_sample_creator = _formation_sample_creator; }

    template < typename FormationSampleIsSignificantPredicateFn >
    void set_formation_sample_significant_predicate( FormationSampleIsSignificantPredicateFn _formation_sample_is_significant_pred )
        { this->M_formation_sample_is_significant = _formation_sample_is_significant_pred; }

    template < typename FormationSampleStorageFn >
    void set_formation_sample_storage( FormationSampleStorageFn _formation_sample_storage )
        { this->M_formation_sample_storage_policy = _formation_sample_storage; }


    template < typename State >
    void operator()( State state )
    {
        // BOOST_CONCEPT_ASSERT(( soccer::StaticStateConcept< State > ));

        // Make sure that the state type passed matches the types expect by functors
        assert( M_state_preprocessor );
        assert( M_state_selector_predicate );
        assert( M_formation_sample_creator );
        assert( M_formation_sample_is_significant );
        assert( M_formation_sample_storage_policy );

        // analyze( state ) => Check that state is relevant for data collection
        if ( !M_state_selector_predicate( state ) )
        {
            return;
        }
        std::clog << "StateSelectorPredicate Selected state at time=" << soccer::get_time( state ) << std::endl;

        // preprocess( state ) => applies necessary adaptations to state (E.g. change team sides, vertical simmetry, etc. )
        std::clog << "BEGIN STATE PREPROCESSING" << std::endl;
        M_state_preprocessor( state );
        std::clog << "END STATE PREPROCESSING" << std::endl;

        // create formation sample from state
        std::clog << "BEGIN FORMATION SAMPLE CREATION" << std::endl;
        auto _new_formation_sample = M_formation_sample_creator( state );
        std::clog << "END FORMATION SAMPLE CREATION" << std::endl;

        /*soccer::formation_sample_total_players_significance_evaluator< formation_sample_type > _sample_scorer;

         static const auto _sample_score_thr = 10.85;
        auto _sample_score = _sample_scorer( _new_formation_sample );

        if ( _sample_score < _sample_score_thr )
        {
            std::clog << "State time=" << soccer::get_time( _state )
                      << " score " << _state_score
                      << " below thr=" << _state_score_thr
                      << std::endl;
            return;
        } */

        // recheck the significance of the sample
        if ( !M_formation_sample_is_significant( _new_formation_sample ) )
        {
            std::clog << "Insignificant formation sample" << std::endl;
            std::clog << _new_formation_sample << std::endl;
            return;
        }

        // Store the new formation sample
        std::clog << "BEGIN APPLY FORMATION SAMPLE STORAGE POLICY" << std::endl;
        M_formation_sample_storage_policy( _new_formation_sample );
        std::clog << "END APPLY FORMATION SAMPLE STORAGE POLICY" << std::endl;
    }
};


template < typename State,
           typename FormationSampleStorage >
struct formation_modeler_generator {
    // BOOST_CONCEPT_ASSERT(( soccer::MutableStateConcept< State > ));
    // BOOST_CONCEPT_ASSERT(( soccer::MutableFormationSampleStorageConcept<FormationSampleStorage> ));
    typedef typename soccer::formation_sample_type< FormationSampleStorage >::type formation_sample_type;

    typedef std::function< void(State&) > state_preprocessor_type;
    typedef std::function< bool(State const&) > state_selector_type;
    typedef std::function< formation_sample_type(State const&) > sample_creator_type;
    typedef std::function< bool(const formation_sample_type&) > sample_evaluator_type;
    typedef std::function< bool(const formation_sample_type&) > sample_storage_policy_type;

    typedef formation_modeler< state_preprocessor_type,
                               state_selector_type,
                               sample_creator_type,
                               sample_evaluator_type,
                               sample_storage_policy_type > type;
};

} // end namespace soccer



namespace soccer {
namespace modeling {

// Implementation of SampleData for formation modeling
template < typename GameLog,
           typename State >
inline
const GameLog& get_game_log( const std::pair< GameLog, State >& sample )
{
    return sample.first;
}

template < typename GameLog,
           typename State >
inline
State const& get_state( const std::pair< GameLog, State >& sample )
{
    return sample.second;
}

// Implementation of mutable SampleData for formation modeling
template < typename GameLog,
           typename State >
inline
void set_game_log( const GameLog& game_log,
                   std::pair< GameLog, State >& sample )
{
    sample.first = game_log;
}

template < typename GameLog,
           typename State >
inline
void set_state( State const& state,
                std::pair< GameLog, State >& sample )
{
    sample.second = state;
}

} // end namespace modeling
} // end namespace soccer

#endif // RCSC_FORMATION_MODELING_STRATEGIES_H
