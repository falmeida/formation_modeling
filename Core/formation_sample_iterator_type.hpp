#ifndef SOCCER_FORMATION_SAMPLE_ITERATOR_TYPE_HPP
#define SOCCER_FORMATION_SAMPLE_ITERATOR_TYPE_HPP

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class which indicates the formation sample iterator type of a storage
\ingroup traits
*/
template <typename FormationSampleStorage, typename Enable = void>
struct formation_sample_iterator_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_FORMATION_SAMPLE_STORAGE_TYPE, (types<FormationSampleStorage>)
        );
};

} // end namespace traits

/*!
\brief \brief_meta{type, formation_sample_storage_type, \meta_formation_sample_storage_type}
\tparam FormationSampleStorage \tparam_formation_sample_storage
\ingroup core

\qbk{[include reference/core/formation_sample_iterator_type.qbk]}
*/
template <typename FormationSampleStorage>
struct formation_sample_iterator_type
{
    typedef typename traits::formation_sample_iterator_type
                <
                    typename soccer::util::bare_type<FormationSampleStorage>::type
                >::type type;
};

} // end namespace soccer


#endif // SOCCER_FORMATION_SAMPLE_ITERATOR_TYPE_HPP
