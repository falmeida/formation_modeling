#ifndef SOCCER_CORE_FORMATION_ACCESS_HPP
#define SOCCER_CORE_FORMATION_ACCESS_HPP

#include "info_type.hpp"
#include "formation_sample_type.hpp"
#include "formation_sample_id_type.hpp"
#include "formation_sample_iterator_type.hpp"

#include <soccer/core/tag.hpp>

#include <soccer/util/bare_type.hpp>

namespace soccer { namespace traits {

template < typename FormationSampleStorage >
struct formation_sample_access
{
   BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_FORMATION_SAMPLE_STORAGE_TYPE, (types<FormationSampleStorage>) );
};

template < typename FormationSampleStorage >
struct formation_sample_iterators_access
{
   BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_FORMATION_SAMPLE_STORAGE_TYPE, (types<FormationSampleStorage>) );
};

template < typename Object >
struct info_access
{
   BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_OBJECT_TYPE, (types<Object>) );
};

}


//
// Get samples in a formation sample storage
//
/*!
\brief get sample iterators pair from storage object
\details \details_get
\tparam FormationSampleStorageObject \tparam_formation_sample_storage
\param storage \param_storage
\return pair of sample id iterators object
\ingroup get
*/
template < typename FormationSampleStorage >
std::pair< typename formation_sample_iterator_type< FormationSampleStorage >::type,
           typename formation_sample_iterator_type< FormationSampleStorage >::type >
get_samples( const FormationSampleStorage& storage )
{
    return traits::formation_sample_iterators_access< typename soccer::util::bare_type< FormationSampleStorage >::type >::get( storage );
}

//
// Get sample with a given id in a formation sample storage
//

/*!
\brief get sample from storage object
\details \details_get_set
\tparam FormationSampleStorage \tparam_storage
\param storage \param_storage
\return sample object
\ingroup get
*/
template < typename FormationSampleStorage >
inline typename formation_sample_type< typename soccer::util::bare_type< FormationSampleStorage >::type >::type
get_sample( typename formation_sample_id_type< FormationSampleStorage >::type sid,
            FormationSampleStorage const& storage )
{
    return traits::formation_sample_access< typename soccer::util::bare_type< FormationSampleStorage >::type >::get( sid, storage );
}

/*!
\brief get sample from storage object
\details \details_get_set
\tparam FormationSampleStorage \tparam_storage
\param storage \param_storage
\return sample object
\ingroup get
*/
template < typename FormationSampleStorage >
inline typename formation_sample_type< typename soccer::util::bare_type< FormationSampleStorage >::type >::type&
get_sample( typename formation_sample_id_type< FormationSampleStorage >::type sid,
            FormationSampleStorage& storage )
{
    return traits::formation_sample_access< typename soccer::util::bare_type< FormationSampleStorage >::type >::get( sid, storage );
}


//
// Get/Set formation sample info
//
template < typename Object >
inline typename info_type< Object >::type
get_info( Object const& obj )
{
    return traits::info_access< typename soccer::util::bare_type< Object >::type >::get( obj );
}

template < typename Object >
inline void
set_info( typename info_type< Object >::type const& info,
          Object& obj )
{
    return traits::info_access< typename soccer::util::bare_type< Object >::type >::set( info, obj );
}

} // end namespace soccer

#endif // SOCCER_CORE_FORMATION_ACCESS_HPP
