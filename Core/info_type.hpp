#ifndef SOCCER_INFO_TYPE_HPP
#define SOCCER_INFO_TYPE_HPP

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class which indicates the type of additional info stored in an object
\ingroup traits
*/
template <typename Object, typename Enable = void>
struct info_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_OBJECT_TYPE, (types<Object>)
        );
};

} // end namespace traits

/*!
\brief \brief_meta{type, object, \meta_object}
\tparam Object \tparam_object
\ingroup core

\qbk{[include reference/core/info_type.qbk]}
*/
template <typename Object>
struct info_type
{
    typedef typename traits::info_type
                <
                    typename soccer::util::bare_type<Object>::type
                >::type type;
};

} // end namespace soccer

#endif // SOCCER_INFO_TYPE_HPP
