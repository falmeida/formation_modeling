#ifndef SOCCER_FORMATION_CLASSIFIER_HPP
#define SOCCER_FORMATION_CLASSIFIER_HPP

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "formation_sample_algorithms.hpp"

#include <soccer/algorithms/state_algorithms.hpp>

#include <boost/graph/breadth_first_search.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/tuple/tuple.hpp>

#include <tuple>
#include <vector>
#include <map>
#include <functional>

namespace soccer {


template < typename State,
           typename PlayerIdInputIterator,
           typename VertexToPlayerIdPropertyMap,
           typename PlayerIdToVertexPropertyMap,
           typename Triangulation = ::CGAL::Delaunay_triangulation_2< ::CGAL::Simple_cartesian< float > > >
/*!
 * \brief create_player_triangulation_from_state
 * \param state
 * \param player_begin
 * \param player_end
 * \param vhtopid_pmap
 * \param pidtovh_pmap
 * \param triangulation
 */
void create_player_triangulation_from_state( State const& state,
                                             PlayerIdInputIterator player_begin,
                                             const PlayerIdInputIterator player_end,
                                             VertexToPlayerIdPropertyMap vhtopid_pmap,
                                             PlayerIdToVertexPropertyMap pidtovh_pmap,
                                             Triangulation& triangulation )
{
    // BOOST_CONCEPT_ASSERT(( StaticStateConcept< State > ));
    typedef typename player_id_type< State >::type state_player_id_type;
    typedef typename Triangulation::Vertex_handle dt_vertex_handle_type;
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< PlayerIdInputIterator > ));
    BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< VertexToPlayerIdPropertyMap, dt_vertex_handle_type  > ));
    BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< PlayerIdToVertexPropertyMap, state_player_id_type > ));

    auto id = 0u;
    for ( auto itp = player_begin; itp != player_end ; itp++ )
    {
        const auto _player = get_player( *itp, state );
        const auto _ppos = get_position( _player );

        typename Triangulation::Point pt( get_x( _ppos ), get_y( _ppos ));
        auto vh = triangulation.insert( pt );
        vh->id() = id++;

        boost::put( vhtopid_pmap, vh, *itp );
        boost::put( pidtovh_pmap, *itp, vh );
    }
}

template < typename State,
           typename VertexToPlayerIdPropertyMap,
           typename PlayerIdToVertexPropertyMap,
           typename Triangulation = ::CGAL::Delaunay_triangulation_2< ::CGAL::Simple_cartesian< float > > >
void create_player_triangulation_from_state( State const& state,
                                             VertexToPlayerIdPropertyMap vhtopid_pmap,
                                             PlayerIdToVertexPropertyMap pidtovh_pmap,
                                             Triangulation& _triangulation)
{
    auto _player_iterators = get_players( state );
    create_player_triangulation_from_state( state,
                                            _player_iterators.first,
                                            _player_iterators.second,
                                            vhtopid_pmap,
                                            pidtovh_pmap,
                                            _triangulation );
}

/*!
 * \brief simple formation description model (<Defenders>-<Midfielders>-<Attackers> )
 */
struct formation_description {
    static constexpr unsigned short NUM_ZONES = 3u;

    formation_description( unsigned short num_defenders = 0u,
                           unsigned short num_midfielders = 0u,
                           unsigned short num_attackers = 0u )
        : M_num_defenders( num_defenders )
        , M_num_midfielders( num_midfielders )
        , M_num_attackers( num_attackers )
    {

    }

    unsigned short M_num_defenders;
    unsigned short M_num_midfielders;
    unsigned short M_num_attackers;
};

/*!
 * \brief The advanced_formation_description struct
 */
struct advanced_formation_description
    : public formation_description {

    advanced_formation_description( unsigned short num_back_defenders = 0u,
                                    unsigned short num_defenders = 0u,
                                    unsigned short num_back_midfielders = 0u,
                                    unsigned short num_midfielders = 0u,
                                    unsigned short num_back_attackers = 0u,
                                    unsigned short num_attackers = 0u )
        : formation_description( num_defenders, num_midfielders, num_attackers )
        , M_num_back_defenders( num_back_defenders )
        , M_num_back_midfielders( num_back_midfielders )
        , M_num_back_attackers( num_back_attackers )
    {

    }

    unsigned short M_num_back_defenders;
    unsigned short M_num_back_midfielders;
    unsigned short M_num_back_attackers;
};

/*!
 * \brief The formation_description1 struct
 */
template < typename PlayerId >
struct formation_description1 {

    std::vector< PlayerId > M_defenders;
    std::vector< PlayerId > M_midfielders;
    std::vector< PlayerId > M_attackers;
};


// TODO Classify formation as wide or narrow in the x and y axis
// TODO Classify formation as defensive of offensive
template < typename State >
bool is_formation_wide( const formation_description& /*_formation_desc*/, State const& /*state*/ )
{
    assert( false );
    return false;
}

bool is_formation_offensive( const formation_description& _formation_desc )
{
    return _formation_desc.M_num_attackers >= _formation_desc.M_num_defenders;
}

bool is_formation_defensive( const formation_description& _formation_desc )
{
    return _formation_desc.M_num_defenders >= _formation_desc.M_num_attackers - 1;
}


/*!
 * \brief The formation_triangulation_classifier struct
 * \tparam State
 * \tparam Side
 */
template < typename State >
struct formation_triangulation_classifier
    : public std::unary_function< State, formation_description >
{
    //BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename soccer::player_id_type< State >::type player_id_t;
    typedef typename soccer::side_type< typename soccer::player_type< State  >::type >::type side_t;
    typedef typename soccer::player_iterator_type< State >::type player_iterator_t;
    typedef typename player_filter_iterator_generator< State >::type player_filter_iterator_t;

    typedef soccer::CGAL::triangulation_formation_sample_storage_generator< >::type delaunay_triangulation_2_type;
    typedef std::map< typename delaunay_triangulation_2_type::Vertex_handle, player_id_t > dt_vertex_to_pid_map_type;
    typedef std::map< player_id_t, typename delaunay_triangulation_2_type::Vertex_handle > pid_to_dt_vertex_map_type;

    typedef enum class { Defender, Midfielder, Attacker } AnalysisScope;
    typedef std::map< typename delaunay_triangulation_2_type::Vertex_handle,
                      std::tuple< AnalysisScope, player_id_t, double > > vertex_scope_map_type;

    struct bfs_visitor
        : public boost::default_bfs_visitor
    {


        State const* M_state;
        vertex_scope_map_type& M_vertex_scope;



        bfs_visitor( State const& state,
                     vertex_scope_map_type& _vertex_scope )
            : M_state( &state )
            , M_vertex_scope( _vertex_scope )
        {

        }

        template < typename Vertex, typename Graph >
        void discover_vertex(Vertex u, const Graph & g) const
        {

        }

        template < typename Vertex, typename Graph >
        void examine_vertex(Vertex u, const Graph &) const
        {
            /* const auto itf = M_vertex_scope.find( u );

            if ( itf == M_vertex_scope.end() ) { return; }

            switch ( itf->second.first )
            {
                case AnalysisScope::Defender:
                    M_formation_description.M_defenders.push_back( itf->first->id() );
                break;
                case AnalysisScope::Midfielder:
                    M_formation_description.M_midfielders.push_back( itf->first->id() );
                break;
                case AnalysisScope::Attacker:
                    M_formation_description.M_attackers.push_back( itf->first->id() );
                break;
            } */
        }


        template < typename Edge, typename Graph >
        void tree_edge(Edge e, const Graph &g)
        {
            auto _source_vd = boost::source( e, g );
            auto _target_vd = boost::target( e, g );
            auto _source_pos = soccer::get_position( soccer::get_player( _source_vd->id() , *M_state ) );
            auto _target_pos = soccer::get_position( soccer::get_player( _target_vd->id() , *M_state ) );
            std::clog << "TreeEdge [" << _source_vd->id()
                      << "(" << soccer::get_x( _source_pos ) << "," << soccer::get_y( _source_pos )  << ")"
                      << "," << _target_vd->id()
                      << "(" << soccer::get_x( _target_pos ) << "," << soccer::get_y( _target_pos )  << ")"
                      << "]";
            auto itfs = M_vertex_scope.find( _source_vd );
            const auto _dist = soccer::distance( soccer::get_position( soccer::get_player( _source_vd->id(), *M_state ) ),
                                                 soccer::get_position( soccer::get_player( _target_vd->id(), *M_state ) ));
            if ( itfs == M_vertex_scope.end() )
            {
                M_vertex_scope[ _target_vd ] = std::make_tuple( AnalysisScope::Defender, _source_vd->id(), _dist );
                std::clog << "=" << " PID= " << _target_vd->id() << " is defender src=" << _source_vd->id() << " dist " << _dist << std::endl;
                return;
            }

            auto itft = M_vertex_scope.find( _target_vd );

            if ( itft == M_vertex_scope.end() ||
                 ( std::get< 1 >( itfs->second ) == std::get< 1 >( itft->second ) && // // check if nodes have the same parent
                   _dist < std::get< 2 >( itft->second )
                  )
                 )
            {
                switch ( std::get<0>( itft->second ) )
                {
                    case AnalysisScope::Defender:
                    {
                        M_vertex_scope[ _target_vd ] = std::make_tuple( AnalysisScope::Midfielder, _source_vd->id(), _dist );
                        std::clog << "=" << " PID= " << _target_vd->id() << " is Midfielder src=" << _source_vd->id() << " dist " << _dist << std::endl;
                    }
                    break;
                    case AnalysisScope::Midfielder:
                        M_vertex_scope[ _target_vd ] = std::make_tuple( AnalysisScope::Attacker, _source_vd->id(), _dist );
                        std::clog << "=" << " PID= " << _target_vd->id() << " is Attacker src=" << _source_vd->id() << " dist " << _dist << std::endl;
                    break;
                    case AnalysisScope::Attacker:

                    break;
                }
            }

        }

        template < typename Edge, typename Graph >
        void non_tree_edge(Edge e, const Graph &g)
        {
             auto _source_vd = boost::source( e, g );
            auto _target_vd = boost::target( e, g );
            auto _source_pos = soccer::get_position( soccer::get_player( _source_vd->id() , *M_state ) );
            auto _target_pos = soccer::get_position( soccer::get_player( _target_vd->id() , *M_state ) );
            std::clog << "NonTreeEdge [" << _source_vd->id()
                      << "(" << soccer::get_x( _source_pos ) << "," << soccer::get_y( _source_pos )  << ")"
                      << "," << _target_vd->id()
                      << "(" << soccer::get_x( _target_pos ) << "," << soccer::get_y( _target_pos )  << ")"
                      << "]";

            /*auto itfs = M_vertex_scope.find( _source_vd );
            auto itft = M_vertex_scope.find( _target_vd );

            if ( std::get< 1 >( itfs->second ) != std::get< 1 >( itft->second ) )
            {
                std::clog << "Different parent " << std::endl;
                return;
            }

            if ( std::get< 2 >( itfs->second ) != std::get< 2 >( itft->second ) )
            {
                std::clog << "Source further from parent" << std::endl;
                return;
            } */

        }

        template < typename Vertex, typename Graph >
        void finish_vertex(Vertex u, const Graph & g)
        {
            std::cout << "Finished vertex " << u->id() << std::endl;
            // M_vertex_scope.erase(u);
        }

    };

    side_t M_side;

    formation_triangulation_classifier( )
    {

    }

    void setSide( const side_t side )
    {
        M_side = side;
    }

    formation_description1< player_id_t >
    operator()( State const& state )
    {
        // 1) Create teammates triangulation from state
        typename player_filter_iterator_generator< State >::type _filtered_player_begin, _filtered_player_end;
        boost::tie( _filtered_player_begin, _filtered_player_end ) = soccer::make_teammate_iterator( M_side, state );
        dt_vertex_to_pid_map_type _dt_vertex_to_pid_map;
        pid_to_dt_vertex_map_type _pid_to_dt_vertex_map;

        delaunay_triangulation_2_type _dt;

        create_player_triangulation_from_state( state,
                                                _filtered_player_begin,
                                                _filtered_player_end,
                                                boost::associative_property_map< dt_vertex_to_pid_map_type >( _dt_vertex_to_pid_map ),
                                                boost::associative_property_map< pid_to_dt_vertex_map_type >( _pid_to_dt_vertex_map ),
                                                _dt );

        // 2) Find goalie player (TODO!! Decide where goalie info is best places [player vs state info])
        const auto _goalie_id = *_filtered_player_begin;
        auto _goalie_vh = _pid_to_dt_vertex_map.at( _goalie_id );

        // 3) Run a breadth first visit to nodes and update defenders, midfielders and attacker
        // auto _dt_graph = soccer::CGAL::make_finite_delaunay_triangulation_graph( _dt );
        std::function< bool( delaunay_triangulation_2_type::Edge ) > _edge_pred_fn = [&_dt] ( delaunay_triangulation_2_type::Edge ed )
        {
                if ( _dt.is_infinite( ed ) ) { return false; }
               auto _face_handle = ed.first;
               if ( _dt.is_infinite( _face_handle ) ) { return false; }

                auto _face_geom = _dt.triangle( _face_handle );
                auto _face_circum = _dt.circumcenter( _face_handle );
                auto _intersection_res = ::CGAL::intersection( _face_circum, _face_geom );
                return _intersection_res ? true : false;
        };

        /* boost::filtered_graph< delaunay_triangulation_2_type,
                               std::function< bool( delaunay_triangulation_2_type::Edge ) >,
                               boost::keep_all
                            > _dt_graph( _dt, _edge_pred_fn );*/
        auto _dt_graph = boost::make_filtered_graph( _dt, _edge_pred_fn );

        formation_description1< player_id_t > _formation_description;
        vertex_scope_map_type vertex_scope;
        boost::breadth_first_search( _dt_graph,
                                     _goalie_vh,
                                     boost::visitor( bfs_visitor( state, vertex_scope ) ) );

        std::for_each( vertex_scope.begin(),
                       vertex_scope.end(),
                       [&_formation_description]( typename vertex_scope_map_type::value_type val )
                        {
                            switch( std::get< 0 >( val.second ))
                            {
                               case AnalysisScope::Defender:
                                    _formation_description.M_defenders.push_back( val.first->id() );
                               break;
                               case AnalysisScope::Midfielder:
                                   _formation_description.M_midfielders.push_back( val.first->id() );
                               break;
                               case AnalysisScope::Attacker:
                                    _formation_description.M_attackers.push_back( val.first->id() );
                               break;
                            }
                        }
                       );

        return _formation_description;
    }

};


/*!
 * \brief Classify formation based on a region analysis
 * \tparam State
 * \tparam Side
 */
template < typename State,
           typename Side >
struct formation_region_based_classifier
    : public std::unary_function< State, formation_description >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_type;
    typedef typename player_filter_iterator_generator< State >::type player_filter_iterator_type;

    Side M_side;

    formation_region_based_classifier( Side side )
        : M_side( side )
    {

    }

    void setSide( const Side side)
    {
        M_side = side;
    }

    formation_description
    operator()( State const& state )
    {
        const auto _defense_line = std::fabs( defense_line( M_side, state ) );
        const auto _offense_line = std::fabs( offense_line( M_side, state ) );

        // Calculate distance between closest teammate to left goal and closest teammate to right goal from each team
        const auto _formation_length = std::fabs( _offense_line - _defense_line );

        // TODO
        // const auto _formation_width = std::fabs( _defense_line - _offense_line );

        const  auto length_formation_zones_with_equal_distribution = _formation_length / formation_description::NUM_ZONES * 1.0;

        // Calculate mid-point
        //float leftTeamMiddleX = (leftTeamClosestToLeftGoal.pos.x-leftTeamClosestToRightGoal.pos.x);
        //float rightTeamMiddleX = (rightTeamClosestToLeftGoal.pos.x-rightTeamClosestToRightGoal.pos.x)/2;

        assert( false );
        // Calculate 1/3 point
        double _defense_midfield_line; /* = std::fabs( M_side == soccer::get_left_side< side_t >() ?
                                                   _defense_line + length_formation_zones_with_equal_distribution :
                                                   _defense_line - length_formation_zones_with_equal_distribution );*/

        // Calculate 2/3 point
        double _midfield_offense_line; /*= std::fabs( M_side == soccer::get_left_side< side_t >() ?
                                                      _offense_line - length_formation_zones_with_equal_distribution :
                                                      _offense_line + length_formation_zones_with_equal_distribution ); */

        // Build formation zones based on the previous calculation
        // Iniliaze team formation zones
        player_filter_iterator_type _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_filtered_players( [&] ( const player_id_type pid ) { return get_side( get_player( pid, state ) ) != M_side; },
                                                                         state );


        formation_description _form_desc;
        // Check players in each zone
        for ( auto itp = _player_begin; itp != _player_end; itp++ )
        {
            const auto _player = get_player( *itp, state );
            if ( is_goalie( _player ) ) { continue; }

            const auto _ppos = get_position( _player );
            const auto _ppos_x_abs = std::fabs( get_x( _ppos ) );
            if ( _defense_line <= _ppos_x_abs  && _defense_midfield_line > _ppos_x_abs  )
            {
                _form_desc.M_num_defenders++;
            }
            else if ( _defense_midfield_line <= _ppos_x_abs && _midfield_offense_line > _ppos_x_abs  )
            {
                _form_desc.M_num_midfielders++;
            }
            else
            {
                _form_desc.M_num_attackers++;
            }
        }

        return _form_desc;
    }

};

} // end namespace soccer

/*!
 * \brief operator <<
 * \param os
 * \param _formation_description
 * \return
 */
std::ostream& operator<<( std::ostream& os, const soccer::formation_description& _formation_description )
{
    os << "("
       << _formation_description.M_num_defenders << "-"
       << _formation_description.M_num_midfielders << "-"
       << _formation_description.M_num_attackers
       << ")";
    return os;
}

/*!
 * \brief operator <<
 * \param os
 * \param _adv_formation_description
 * \return
 */
std::ostream& operator<<( std::ostream& os, const soccer::advanced_formation_description& _adv_formation_description )
{
    os << "("
       << _adv_formation_description.M_num_back_defenders << "-"
       << _adv_formation_description.M_num_defenders << "-"
       << _adv_formation_description.M_num_back_midfielders << "-"
       << _adv_formation_description.M_num_defenders << "-"
       << _adv_formation_description.M_num_back_attackers << "-"
       << _adv_formation_description.M_num_attackers
       << ")";
    return os;
}


/*!
 * \brief operator <<
 * \param os
 * \param _formation_description1
 * \return
 */
template < typename PlayerId >
std::ostream& operator<<( std::ostream& os, const soccer::formation_description1< PlayerId >& _formation_description )
{
    os << "(";
    std::for_each( _formation_description.M_defenders.begin(),
                   _formation_description.M_defenders.end(),
                   [&os] ( PlayerId const pid ) { os << " " << pid; } );
    os << ")-(";
    std::for_each( _formation_description.M_midfielders.begin(),
                   _formation_description.M_midfielders.end(),
                 [&os] ( PlayerId const pid ) { os << " " << pid; } );
    os << ")-(";
    std::for_each( _formation_description.M_attackers.begin(),
                   _formation_description.M_attackers.end(),
                   [&os] ( PlayerId const pid ) { os << " " << pid; } );
    os << ")";
    return os;
}

#endif // SOCCER_FORMATION_CLASSIFIER_HPP
