#ifndef SOCCER_FORMATION_STATE_SELECTOR_HPP
#define SOCCER_FORMATION_STATE_SELECTOR_HPP

#include <soccer/rcsslogplayer/rcsslogplayer_disp_info_predicates.h>

#include <soccer/algorithms/state_algorithms.hpp>

namespace rcss {
namespace rcg {

template < typename PlayerIdCanKickPredicate >
struct offense_state_selector
    : public std::unary_function< DispInfoT, bool > {

    PlayerIdCanKickPredicate M_pid_can_kick;
    Side M_offense_side;
    // bool offense_side_in_possession

    offense_state_selector( PlayerIdCanKickPredicate _pid_can_kick_predicate,
                            Side offense_side = rcss::rcg::NEUTRAL )
        : M_pid_can_kick( _pid_can_kick_predicate )
        , M_offense_side( offense_side )
    {

    }

    void set_offense_side( const Side offense_side )
    {
        assert( offense_side != rcss::rcg::NEUTRAL );
        M_offense_side = offense_side;
    }

    bool operator()( const DispInfoT& state )
    {
        assert( M_offense_side != rcss::rcg::NEUTRAL );

        auto _players = soccer::get_players( state );
        // Ball is kickable by offense side
        for ( auto itp = _players.first; itp != _players.second; itp++ )
        {
            if ( M_pid_can_kick( state ) )
            {
                return true;
            }
        }

        return false;
    }
};


template < typename PlayerIdCanKickPredicate >
struct defense_state_selector
    : public std::unary_function< DispInfoT, bool > {

    PlayerIdCanKickPredicate M_pid_can_kick;
    Side M_defense_side;
    // bool offense_side_in_possession

    defense_state_selector( PlayerIdCanKickPredicate _pid_can_kick_predicate,
                            Side defense_side = rcss::rcg::NEUTRAL )
        : M_pid_can_kick( _pid_can_kick_predicate )
        , M_defense_side( defense_side )
    {

    }

    void set_defense_side( const Side defense_side )
    {
        assert( defense_side != rcss::rcg::NEUTRAL );
        M_defense_side = defense_side;
    }

    bool operator()( const DispInfoT& state )
    {
        assert( M_defense_side != rcss::rcg::NEUTRAL );

        auto _players = soccer::get_players( state );
        // Ball is kickable by offense side
        for ( auto itp = _players.first; itp != _players.second; itp++ )
        {
            if ( M_pid_can_kick( *itp, state ) )
            {
                return false;
            }
        }

        return true;
    }
};


}
}


namespace soccer {

struct formation_state_selector_factory {


    /* std::function< bool(const rcss::rcg::DispInfoT&) > create( const std::string&  )
    {

    }*/

    static
    std::function< bool(const rcss::rcg::DispInfoT&) >
    createOffenseSampleSelector( rcss::rcg::Side attacking_team_side,
                                 const rcss::rcg::ServerParamT& server_param ,
                                 const std::map< int, rcss::rcg::PlayerTypeT>& player_types )
    {
        return [attacking_team_side,&server_param,&player_types] ( rcss::rcg::DispInfoT const& state )
                {
                    const double ball_speed_thr = server_param.ball_speed_max_ * 0.25;
                    const double player_speed_thr = server_param.player_speed_max_ * 0.25;

                    rcss::rcg::ball_speed_max ball_speed_max_pred( ball_speed_thr );
                    rcss::rcg::player_speed_max player_speed_max_pred( player_speed_thr, attacking_team_side );
                    rcss::rcg::state_in_playmode _state_in_pmode( rcss::rcg::PM_PlayOn );
                    rcss::rcg::player_can_kick_binary_predicate _player_can_kick( server_param, player_types );

                    if ( !ball_speed_max_pred( state ) ) { return false; }

                    if ( !player_speed_max_pred( state ) ) { return false; }

                    if ( !_state_in_pmode( state ) ) { return false; }

                    typename soccer::player_iterator_type< rcss::rcg::DispInfoT >::type _player_begin, _player_end;
                    boost::tie( _player_begin, _player_end ) = soccer::get_players( state );

                    bool teammate_can_kick = false;
                    bool opponent_can_kick = false;
                    for ( auto itp = _player_begin; itp != _player_end; itp++ )
                    {
                        auto _player = soccer::get_player( *itp, state );

                        if ( _player_can_kick( *itp, state ) )
                        {
                            if ( soccer::get_side( _player ) == attacking_team_side )
                            {
                                teammate_can_kick = true;
                            }
                            else
                            {
                                opponent_can_kick = true;
                            }
                        }
                    }

                    return teammate_can_kick && !opponent_can_kick;
                };
    }

    static
    std::function< bool(const rcss::rcg::DispInfoT&) >
    createDefenseSampleSelector( rcss::rcg::Side defending_team_side,
                                 rcss::rcg::ServerParamT const& server_param ,
                                 std::map< int, rcss::rcg::PlayerTypeT> const& player_types )
    {
        return [defending_team_side,&server_param,&player_types] ( rcss::rcg::DispInfoT const& state )
                {
                    const double ball_speed_thr = server_param.ball_speed_max_ * 0.25;
                    const double player_speed_thr = server_param.player_speed_max_ * 0.25;

                    rcss::rcg::ball_speed_max ball_speed_max_pred( ball_speed_thr );
                    rcss::rcg::player_speed_max player_speed_max_pred( player_speed_thr, defending_team_side );
                    rcss::rcg::state_in_playmode _state_in_pmode( rcss::rcg::PM_PlayOn );
                    rcss::rcg::player_can_kick_binary_predicate _player_can_kick( server_param, player_types );

                    if ( !ball_speed_max_pred( state ) ) { return false; }

                    if ( !player_speed_max_pred( state ) ) { return false; }

                    if ( !_state_in_pmode( state ) ) { return false; }

                    typename soccer::player_iterator_type< rcss::rcg::DispInfoT >::type _player_begin, _player_end;
                    boost::tie( _player_begin, _player_end ) = soccer::get_players( state );
                    bool teammate_can_kick = false;
                    bool opponent_can_kick = false;
                    for ( auto itp = _player_begin; itp != _player_end; itp++ )
                    {
                        if ( _player_can_kick( *itp, state ) )
                        {
                            auto _player = soccer::get_player( *itp, state );

                            if ( soccer::get_side( _player ) == defending_team_side )
                            {
                                teammate_can_kick = true;
                            }
                            else
                            {
                                opponent_can_kick = true;
                            }
                        }
                    }

                    if ( teammate_can_kick )
                    {
                        std::clog << "Teammate can kick";
                    }

                    if ( opponent_can_kick )
                    {
                        std::clog << " Opponent can kick";
                    }
                    std::clog <<  std::endl;
                    return opponent_can_kick && !teammate_can_kick;
                };
    }

};

}

#endif // SOCCER_FORMATION_STATE_SELECTOR_HPP
