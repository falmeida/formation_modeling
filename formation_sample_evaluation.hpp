#ifndef SOCCER_FORMATION_SAMPLE_EVALUATION_HPP
#define SOCCER_FORMATION_SAMPLE_EVALUATION_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "formation_sample_algorithms.hpp"

#include "formation_sample_concept.hpp"

#include <boost/tuple/tuple.hpp>
#include <boost/concept_check.hpp>

#include <functional>

namespace soccer {


/*!
 * \brief Mobile object speed max significance evaluator
 */
template < typename MobileObject >
struct mobile_object_speed_max_significance_evaluator
    : public std::unary_function< MobileObject, float > {
    // BOOST_CONCEPT_ASSERT(( soccer::MobileObjectConcept<Ball> ));
    float M_speed_max;

    mobile_object_speed_max_significance_evaluator( float _speed_max )
        : M_speed_max( _speed_max )
    {

    }

    float operator()( const MobileObject& _mobile_object ) const
    {
        const auto _speed = get_speed( _mobile_object );

        const auto _significance = _speed > M_speed_max
                                        ? 0.0
                                        : 1.0 - ( _speed / M_speed_max);

        return _significance;
    }

};

/*!
 * \brief Functor that calculates the significance of a formation sample based on objects speed
 */
template < typename FormationSample >
struct formation_sample_max_speed_evaluator
    : public std::unary_function< FormationSample, float > {

    float M_ball_speed_max;
    float M_player_speed_max;
    float M_ball_speed_weight;
    float M_player_speed_weight;

    static constexpr float DEFAULT_BALL_SPEED_WEIGHT = 0.2;
    static constexpr float DEFAULT_PLAYER_SPEED_WEIGHT = 0.8;

    formation_sample_max_speed_evaluator( const float ball_speed_max,
                                          const float player_speed_max )
        : M_ball_speed_max( ball_speed_max )
        , M_player_speed_max( player_speed_max )
        , M_ball_speed_weight( DEFAULT_BALL_SPEED_WEIGHT )
        , M_player_speed_weight( DEFAULT_PLAYER_SPEED_WEIGHT )
    {

    }

    float operator()( const FormationSample& _formation_sample )
    {
        typedef typename soccer::tag< FormationSample >::type sample_category_tag;
        typedef typename soccer::player_type< FormationSample >::type sample_player_t;
        typedef typename soccer::ball_type< FormationSample >::type sample_ball_t;
        // TODO Assert that player and ball have velocity

        typedef typename soccer::player_iterator_type< FormationSample >::type sample_player_iterator;

        const auto _ball = get_ball( _formation_sample );
        const auto _ball_vel  = get_velocity( _ball );
        const auto _ball_speed = get_speed(  _ball_vel );

        if ( _ball_speed > M_ball_speed_max ) { return 0.0; }

        auto _players_speed_score_total = 0.0;
        size_t _num_players = 0u;
        sample_player_iterator _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( _formation_sample );
        for ( auto itp = _player_begin; itp != _player_end; itp++ )
        {
            const auto _player = get_player( *itp, _formation_sample );
            const auto _player_vel = get_velocity( _player );
            const auto _player_speed = get_speed( _player_vel );
            if ( _player_speed > M_player_speed_max ) { return 0.0; }

            _players_speed_score_total += 1.0 - _player_speed / M_player_speed_max;
            _num_players++;
        }

        const auto _ball_speed_score = 1.0 - _ball_speed / M_ball_speed_max;
        const auto _players_speed_score = _players_speed_score_total / _num_players;
        const auto _final_score = _ball_speed_score * M_ball_speed_weight +
                                  _players_speed_score * M_player_speed_weight;

        return _final_score;
    }


};

/*!
 * \brief Functor that calculates the total significance of a formation sample
 */
template < typename FormationSample >
struct formation_sample_adjacent_player_ball_dist_significance_evaluator 
    : public std::unary_function< FormationSample, float > {
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));

    float M_max_ball_dist; 
    
    formation_sample_adjacent_player_ball_dist_significance_evaluator( const float max_ball_dist )
        : M_max_ball_dist( max_ball_dist )
    {

    }

    float operator()( const FormationSample& _sample )
    {
        typedef ::CGAL::Simple_cartesian< float > kernel_type;
        typedef ::CGAL::Delaunay_triangulation_2< kernel_type > triangulation_2_type;
        typedef typename triangulation_2_type::Point triangulation_point_type;

        typedef typename soccer::player_iterator_type< FormationSample >::type sample_player_iterator;
        typedef typename soccer::player_id_type< FormationSample >::type sample_player_id_descriptor;

        // 1) Calculate triangulation of formation sample
        typename triangulation_2_type::Vertex_handle _ball_vh;
        triangulation_2_type _triangulation;
        typedef std::map< typename triangulation_2_type::Vertex_handle, sample_player_id_descriptor > vertex_to_pid_map;
        vertex_to_pid_map _vertex_to_pid_map;
        boost::associative_property_map< vertex_to_pid_map > _vertex_to_pid_pmap( _vertex_to_pid_map );
        create_formation_sample_triangulation( _sample, &_ball_vh, _vertex_to_pid_pmap, _triangulation );

        // 2) Determine distance from adjacent players to ball
        auto _ball_edges_circulator = _triangulation.incident_edges( _ball_vh );
        float _total_player_dist = 0.0;
        unsigned int _num_players = 0u;
        auto _ball_edges_circulator_end = _ball_edges_circulator;
        do {
            if ( _triangulation.is_infinite( _ball_edges_circulator ) )
            {
                _ball_edges_circulator++;
                continue;
            }
            auto _edge_segment = _triangulation.segment( *_ball_edges_circulator );
            const auto _player_to_ball_dist = std::sqrt( _edge_segment.squared_length() );

            if ( _player_to_ball_dist > M_max_ball_dist )
            {
                return 0.0;
            }

            _total_player_dist += _player_to_ball_dist;
            _num_players++;
            _ball_edges_circulator++;
        } while ( _ball_edges_circulator != _ball_edges_circulator_end );

        assert( _num_players != 0 );

        // 3) Calculate average distance of players neighbor to the ball if below M_max_ball_dist
        auto _score = _total_player_dist / _num_players * 1.0;
        return _score;
    }

};


template < typename FormationSample >
struct formation_sample_inter_player_ball_dist_significance_evaluator 
   : public std::unary_function< FormationSample, float > {
   // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));
   float M_adjacent_player_dist_max;
   float M_ball_adjacent_player_dist_max;

   formation_sample_inter_player_ball_dist_significance_evaluator( const float adjacent_player_dist_max  )
        : M_adjacent_player_dist_max( adjacent_player_dist_max )
        , M_ball_adjacent_player_dist_max( adjacent_player_dist_max )
   {

   }

   formation_sample_inter_player_ball_dist_significance_evaluator( const float adjacent_player_dist_max,
                                                                   const float ball_adjacent_player_dist_max)
        : M_adjacent_player_dist_max( adjacent_player_dist_max )
        , M_ball_adjacent_player_dist_max( ball_adjacent_player_dist_max )
   {

   }

   float operator()( const FormationSample& _sample )
   {
       typedef ::CGAL::Simple_cartesian< float > kernel_type;
       typedef ::CGAL::Triangulation_data_structure_2<> triangulation_data_structure_type;
       typedef ::CGAL::Delaunay_triangulation_2< kernel_type,
                                               triangulation_data_structure_type > triangulation_2_type;
       typedef typename triangulation_2_type::Point triangulation_point_type;


       typedef typename soccer::player_iterator_type< FormationSample >::type sample_player_iterator;
       typedef typename soccer::player_id_type< FormationSample >::type sample_player_id_descriptor;

        // 1) Calculate triangulation of formation sample (REDUNDANT CODE)
       triangulation_2_type _triangulation;
       triangulation_point_type _ball_pt( get_x( get_ball( _sample ) ),
                                          get_y( get_ball( _sample ) ) );
       auto _ball_vh = _triangulation.insert( _ball_pt );

       sample_player_iterator _player_begin,_player_end;
       boost::tie( _player_begin, _player_end ) = get_players( _sample );
       std::map< sample_player_id_descriptor, typename triangulation_2_type::Vertex_handle > _pid_to_vertex_map;
       std::map< typename triangulation_2_type::Vertex_handle, sample_player_id_descriptor> _vertex_to_pid_map;
       for ( auto itp = _player_begin; itp != _player_end; itp++ )
       {
           triangulation_point_type _player_pt( get_x( get_player( *itp, _sample )),
                                                get_y( get_player( *itp, _sample )) );
           auto _player_vh = _triangulation.insert( _player_pt );
           _pid_to_vertex_map[*itp] = _player_vh;
           _vertex_to_pid_map[_player_vh] = *itp;
       }

        // 2) For each edge determine distance
       auto _edge_total_dist2 = 0.0f;
       size_t _num_edges = 0u;
       for ( auto ite = _triangulation.finite_edges_begin();
             ite != _triangulation.finite_edges_end(); ite++, _num_edges++ )
       {
           auto _ccw_vh = ite->first->vertex( _triangulation.ccw( ite->second ) );
           auto _cw_vh = ite->first->vertex( _triangulation.cw( ite->second ) );
           const auto _edge_segment = _triangulation.segment( *ite );
           const auto _edge_segment_length2 = _edge_segment.squared_length();

           if ( ( _ccw_vh == _ball_vh || _cw_vh == _ball_vh ) )
           {
               if ( _edge_segment_length2 > M_ball_adjacent_player_dist_max )
               {
                    return 0.0;
               }
           }

           if ( _edge_segment_length2 > M_adjacent_player_dist_max )
           {
               return 0.0;
           }

           _edge_total_dist2 += _edge_segment_length2;

            // 2.1) If distance( edge ) > max_distance then value = 0 (stop)
            // 3) For each vertex
            // 3.1 ) Calculate average distance for each outgoing edge
            // 3.2 ) Calculate weight of each vertex (or use static constant)
            // 4) Return average distance of all vertices calculated in 3.1 using weights in 3.2
       }

       assert( _num_edges != 0 );

       auto _score = _edge_total_dist2 / _num_edges * 1.0f;
       return _score;
   }
 
};

/*!
 * \brief Functor that calculates the total significance of a formation sample
 */
template < typename FormationSample >
struct formation_sample_total_significance_evaluator
    : public std::unary_function< FormationSample, float > {
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));

    virtual float operator()( const FormationSample& _sample )
    {
        // TODO FormationSample must have significance tag
        auto _total_significance = get_ball_significance( _sample );

        typename FormationSample::player_iterator_t pid_begin, pid_end;
        boost::tie( pid_begin, pid_end ) = get_players( _sample );
        for ( ; pid_begin != pid_end; pid_begin++ )
        {
            _total_significance += get_player_significance( *pid_begin, _sample );
        }

        return _total_significance;
    }
};

/*!
 * \brief Functor that calculates the total significance of players in a formation sample
 */
template < typename FormationSample >
struct formation_sample_total_players_significance_evaluator
    : public std::unary_function< float, FormationSample > {
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));

    virtual float operator()( const FormationSample& _sample )
    {
        // FormationSample must have significance tag
        float _total_significance = 0.0;

        typename FormationSample::player_iterator_t pid_begin, pid_end;
        boost::tie( pid_begin, pid_end ) = get_players( _sample );
        for ( ; pid_begin != pid_end; pid_begin++ )
        {
            _total_significance += get_player_significance( *pid_begin, _sample );
        }

        return _total_significance;
    }
};

/*!
 * \brief Functor that calculates the average significance of a formation sample
 */
template < typename FormationSample >
struct formation_sample_average_significance_evaluator
    : public formation_sample_total_significance_evaluator< FormationSample > {
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));

    virtual float operator()( const FormationSample& _sample )
    {
        auto _total_significance = formation_sample_total_significance_evaluator< FormationSample >::operator ()( _sample );
        return _total_significance / ( ( num_players( _sample ) + 1) * 1.0f );
    }
};

/*!
 * \brief Functor that calculates the average significance of players in a formation sample
 */
template < typename FormationSample >
struct formation_sample_average_players_significance_evaluator
    : public formation_sample_total_players_significance_evaluator< FormationSample > {
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));

    virtual float operator()( const FormationSample& _sample )
    {
        auto _total_significance = formation_sample_total_players_significance_evaluator< FormationSample >::operator ()( _sample );
        return _total_significance / ( num_players( _sample ) * 1.0f );
    }
};

} // end namespace soccer

#endif // SOCCER_FORMATION_SAMPLE_EVALUATION_HPP
