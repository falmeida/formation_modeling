#ifndef STATE_EVALUATION_H
#define STATE_EVALUATION_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "rcsslogplayer/rcg_position_modeler.h"
#include <soccer/rcsslogplayer/rcsslogplayer_disp_info.h>


#include <rcsc/common/server_param.h>

#include <boost/tuple/tuple.hpp>
#include <map>
#include <functional>

namespace rcss {
namespace rcg {

class state_object_speed_score
    : public std::unary_function< rcss::rcg::DispInfoT, double >

{
public:
    state_object_speed_score( const rcss::rcg::RcgHandler& modeler,
                              const rcss::rcg::Side side )
        : M_modeler( &modeler )
        , M_side( side )
    {

    }

    double operator()( const rcss::rcg::DispInfoT& state )
    {
        static const auto ball_speed_weight = 0.25;
        static const auto players_speed_weight = 0.75;

        auto score = 0.0;

        // TODO Consider ball velocity with regard to its maximum speed
        const auto _ball = soccer::get_ball( state );
        const auto _ball_vel = soccer::get_velocity( _ball );
        const auto ball_speed = soccer::get_speed( _ball_vel );
        auto ball_speed_score = 1.0 - ball_speed / M_modeler->serverParams().ball_speed_max_;


        auto players_speed_score = 0.0;
        size_t num_players_considered = 0;

        typename soccer::player_iterator_type< rcss::rcg::DispInfoT >::type _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = soccer::get_players( state );
        for ( auto itp = _player_begin; itp != _player_end; itp++ )
        {
            const auto _player = soccer::get_player( *itp, state );
            const auto _player_vel = soccer::get_velocity( _player );
            const auto _player_speed = soccer::get_speed( _player_vel );

            const double _player_speed_max = _player.type_ == -1
                                              ? M_modeler->serverParams().player_speed_max_
                                              : M_modeler->playerTypes().at(_player.type_).player_speed_max_;

            auto _player_speed_score = 1.0 - _player_speed / _player_speed_max;
            players_speed_score += _player_speed_score;
            num_players_considered++;
        }
        players_speed_score /= num_players_considered * 1.0;

        score = ball_speed_score * ball_speed_weight +
                players_speed_score * players_speed_weight;
    #ifdef DEBUG_STATE_EVALUATION
        std::clog << "(state_evaluation) time=" << soccer::get_time( state )
                  << " score=" << score << std::endl;
    #endif
        return score;
    }

private:
    rcss::rcg::RcgHandler const* M_modeler;
    rcss::rcg::Side M_side;
};

}
}

#endif // STATE_EVALUATION_H
