#ifndef SOCCER_FORMATION_MODELING_SAMPLE_MERGE_H
#define SOCCER_FORMATION_MODELING_SAMPLE_MERGE_H

#include <boost/property_map/property_map.hpp>
#include <boost/concept_check.hpp>

#include <functional>

namespace soccer {
namespace modeling {

template < typename MobileObject,
           typename ScoreEvaluator >
struct simple_mobile_object_merger
    : public std::binary_function< MobileObject, MobileObject, MobileObject> {

    /*BOOST_CONCEPT_ASSERT(( soccer::MutableMobileObjectConcept<MobileObject> ));
    typedef double score_descriptor;
    BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< ScoreEvaluator,
                                                        score_descriptor,
                                                        MobileObject> )); */

    ScoreEvaluator M_score_evaluator;

    simple_mobile_object_merger( ScoreEvaluator _score_evaluator )
        : M_score_evaluator( _score_evaluator )
    {

    }

    MobileObject operator()( const MobileObject& obj1, const MobileObject& obj2 )
    {
        MobileObject _new_obj;
        // Assuming score is normalized to [0..1]
        const auto _obj1_score = M_score_evaluator( obj1 );
        const auto _obj2_score = M_score_evaluator( obj2 );

        // Calculate new mobile object position using a linear interpolation
        auto _new_obj_x = interpolate( soccer::get_x( obj1 ), soccer::get_x( obj2 ),
                                       _obj1_score, _obj2_score );
        auto _new_obj_y = interpolate( soccer::get_y( obj1 ), soccer::get_y( obj2 ),
                                       _obj1_score, _obj2_score );
        soccer::set_x( _new_obj_x, _new_obj );
        soccer::set_y( _new_obj_y, _new_obj );

        // Calculate new mobile object position using a linear interpolation
        auto _new_obj_vx = interpolate( soccer::get_vx( obj1 ), soccer::get_vx( obj2 ),
                                       _obj1_score, _obj2_score );
        auto _new_obj_vy = interpolate( soccer::get_vy( obj1 ), soccer::get_vy( obj2 ),
                                       _obj1_score, _obj2_score );
        soccer::set_vx( _new_obj_vx, _new_obj );
        soccer::set_vy( _new_obj_vy, _new_obj );

        return _new_obj;
    }

private:
    template <typename Coordinate,
              typename Weight >
    double interpolate( const Coordinate a, const Coordinate b,
                        const Weight score_a,
                        const Weight score_b)
    {
        const auto b_to_a_offset = b - a;
        return ( a + b_to_a_offset * score_a ) +
               ( b - b_to_a_offset * score_b ) / 2.0;
    }

};

template < typename Ball,
           typename ScoreEvaluator >
struct simple_ball_object_merger
    : public std::binary_function< Ball, Ball, Ball> {

    /*BOOST_CONCEPT_ASSERT(( soccer::MutableBallConcept<Ball> ));
    typedef double score_descriptor;
    BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< ScoreEvaluator,
                                                        score_descriptor,
                                                        Ball> )); */
    ScoreEvaluator M_score_evaluator;

    simple_ball_object_merger( ScoreEvaluator _score_evaluator )
        : M_score_evaluator( _score_evaluator )
    {

    }

    Ball operator()( const Ball& b1, const Ball& b2 )
    {

    }

};

template < typename Player,
           typename ScoreEvaluator >
struct simple_player_merger
    : public std::binary_function< Player, Player, Player> {
    /*BOOST_CONCEPT_ASSERT(( soccer::MutablePlayerConcept<Ball> ));
    typedef double score_descriptor;
    BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< ScoreEvaluator,
                                                        score_descriptor,
                                                        Player> )); */
    ScoreEvaluator M_score_evaluator;

    simple_player_merger( ScoreEvaluator _score_evaluator )
        : M_score_evaluator( _score_evaluator )
    {

    }

    Player operator()( const Player& p1, const Player& p2 )
    {

    }

};



template < typename State,
           typename PlayerIdMapS1toS2,
           typename BallMerger,
           typename PlayerMerger = BallMerger >
/*!
 * \brief Create a new state samples from two existing samples
 * \param s1 first state
 * \param s1 first state
 * \param s2 second state
 * \return returns a new sample from the merge of s1 and s2
 */
State merge_samples( State const& s1,
                     State const& s2,
                     PlayerIdMapS1toS2 s1_to_s2_pid_map,
                     BallMerger ball_merger,
                     PlayerMerger player_merger )
{
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< PlayerIdInputIteratorS1 > ));
    /*
    // For future integration with libsoccer
    BOOST_CONCEPT_ASSERT(( soccer::StaticStateConcept< State > ));
    typedef soccer::state_traits< State > state_traits_t;
    typedef typename state_traits_t::ball_t ball_t;
    typedef typename state_traits_t::player_t player_t;
    typedef typename state_traits_t::player_id_t player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::ReadablePropertyMapConcept< PlayerIdMapS1toS2, player_id_t > ));

    typedef float score_descriptor;

    BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< BallEvaluator,
                                                        core_descriptor,
                                                        ball_t> ));
    BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< PlayerEvaluator,
                                                        score_descriptor,
                                                        player_t> ));*/


    State new_state;

    auto _new_ball = ball_merger( soccer::get_ball( s1 ), soccer::get_ball( s2 ) );
    soccer::set_ball( _new_ball, _new_state );

    auto _player_iterators = soccer::get_players( s1 );
    for ( auto itp = _player_iterators.first;
          itp != _player_iterators.second ; itp++ )
    {
        auto _new_player = player_merger( soccer::get_player( *itp, s1 ),
                                          soccer::get_player( boost::get( s1_to_s2_pid_map, *itp), s2 ));
        soccer::set_player( *itp, _new_player, new_state );
    }

    return new_state;
}

} // end namespace modeling
} // end namespace soccer

#endif // SOCCER_FORMATION_MODELING_SAMPLE_MERGE_H
