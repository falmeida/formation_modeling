#ifndef SOCCER_FORMATION_SAMPLE_TRANSFORM_HPP
#define SOCCER_FORMATION_SAMPLE_TRANSFORM_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "sample_matching.h"

#include "formation_sample_algorithms.hpp"

#include <boost/property_map/property_map.hpp>

#include <limits>

namespace soccer {

//
// Apply player matchings
//
template < typename FormationSample,
           typename PlayerIdMatchingMap >
/*!
 * \brief Apply pre-calculated matchings to a given formation sample
 * \param _formation_sample the formation sample to update
 * \param _pid_map the player matching to be applied
 */
void apply_matchings( FormationSample& _formation_sample,
                      PlayerIdMatchingMap _pid_map )
{
    BOOST_CONCEPT_ASSERT(( soccer::FormationSampleConcept<FormationSample> ));
    BOOST_CONCEPT_ASSERT(( boost::ReadablePropertyMapConcept<PlayerIdMatchingMap,
                           typename soccer::player_id_type< FormationSample >::type > ));
    typename soccer::player_iterator_type< FormationSample >::type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = soccer::get_players( _formation_sample );
    std::map< typename soccer::player_id_type< FormationSample >::type,
              typename soccer::player_type< FormationSample >::type > old_players;
    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        old_players.insert( std::make_pair( *itp, get_player( *itp, _formation_sample ) ));
    }
    // Cannot use the form below for sample proxies
    // const auto _formation_sample_copy = _formation_sample;
    for ( auto itp = _player_begin ; itp != _player_end; itp++ )
    {
        auto _new_pid = boost::get( _pid_map, *itp );
        soccer::set_player( _new_pid,
                            old_players.at( *itp ),
                            _formation_sample );
    }

}


/*!
 * \brief Enforce player spacings within the formation
 * \todo NOT DONE
 */
template < typename FormationSample >
struct enforce_spacings_formation_sample {
    BOOST_CONCEPT_ASSERT(( soccer::FormationSampleConcept<FormationSample> ));

    typedef ::CGAL::Simple_cartesian< float > kernel_type;
    typedef ::CGAL::Delaunay_triangulation_2< kernel_type > triangulation_2_type;
    typedef typename triangulation_2_type::Point triangulation_point_type;
    typedef typename triangulation_2_type::Vertex_handle triangulation_vertex_handle_type;

    typedef typename soccer::player_iterator_type< FormationSample >::type sample_player_iterator;
    typedef typename soccer::player_id_type< FormationSample >::type sample_player_id_descriptor;
    typedef std::map< typename triangulation_2_type::Vertex_handle, sample_player_id_descriptor > vertex_to_pid_map;

    // Internal Triangulation Graph Visitor
    class bfs_visitor
            : public boost::default_bfs_visitor
    {
        private:
            const FormationSample& M_spacings_enforcer;
            const vertex_to_pid_map M_vertex_to_pid_map;
            const triangulation_vertex_handle_type M_ball_vh;

        public:
            bfs_visitor( const enforce_spacings_formation_sample< FormationSample >& _spacings_enforcer,
                         const vertex_to_pid_map& _vertex_to_pid_map,
                         const triangulation_vertex_handle_type _ball_vh)
                : M_spacings_enforcer( _spacings_enforcer  )
                , M_vertex_to_pid_map( _vertex_to_pid_map )
                , M_ball_vh( _ball_vh )
            { }

              template < typename Vertex, typename Graph >
              void discover_vertex(Vertex u, const Graph & g) const
              {

              }

              template < typename Vertex, typename Graph >
              void examine_vertex(Vertex u, const Graph & g) const
              {
                  if ( u == M_ball_vh ) { return; }

                  // Get adjacent vertices

                  // Adjust position based on already visited vertices
              }

              template < typename Edge, typename Graph >
              void examine_edge(Edge e, const Graph & g) const
              {

              }

              template < typename Vertex, typename Graph >
              void finish_vertex(Vertex v, const Graph & g) const
              {

              }

    };

    float M_ball_to_player_dist_min;
    float M_ball_to_player_dist_max;

    float M_player_to_player_dist_min;
    float M_player_to_player_dist_max;

    enforce_spacings_formation_sample( float ball_to_player_dist_min,
                                       float player_to_player_dist_min,
                                       float ball_to_player_dist_max = std::numeric_limits<float>::max(),
                                       float player_to_player_dist_max = std::numeric_limits<float>::max() )
        : M_ball_to_player_dist_min( ball_to_player_dist_min )
        , M_ball_to_player_dist_max( ball_to_player_dist_max )
        , M_ball_to_player_dist_min( player_to_player_dist_min )
        , M_player_to_player_dist_max( player_to_player_dist_max )
    {

    }

    void operator()( FormationSample& _sample )
    {
        // 1) Calculate triangulation of formation sample
        typename triangulation_2_type::Vertex_handle _ball_vh;
        triangulation_2_type _triangulation;

        vertex_to_pid_map _vertex_to_pid_map;
        boost::associative_property_map< vertex_to_pid_map > _vertex_to_pid_pmap( _vertex_to_pid_map );
        create_formation_sample_triangulation( _sample, &_ball_vh, _vertex_to_pid_pmap, _triangulation );

        // 2) Check that player-ball spacing is kept and if not adjust!!
        auto _ball_edges_circulator = _triangulation.incident_edges( _ball_vh );
        float _total_player_dist = 0.0;
        unsigned int _num_players = 0u;
        auto _ball_edges_circulator_end = _ball_edges_circulator;
        do {

            if ( _triangulation.is_infinite( _ball_edges_circulator ) )
            {
                _ball_edges_circulator++;
                continue;
            }
            auto _edge_segment = _triangulation.segment( *_ball_edges_circulator );
            const auto _player_to_ball_dist = std::sqrt( _edge_segment.squared_length() );

            if ( _player_to_ball_dist >= M_ball_to_player_dist_min &&
                _player_to_ball_dist <= M_ball_to_player_dist_max )
            {
                _ball_edges_circulator++;
                continue;
            }

            // Adjust player position => preserve direction but adapt distance
            const auto _vh0 = _ball_edges_circulator->first->vertex( _triangulation.cw( _ball_edges_circulator->second ) );
            const auto _vh1 = _ball_edges_circulator->first->vertex( _triangulation.ccw( _ball_edges_circulator->second ) );
            const auto _player_vh = _vh0 == _ball_vh
                                    ? _vh1
                                    : _vh0;

            const auto _radius = _player_to_ball_dist < M_ball_to_player_dist_min
                                    ? M_ball_to_player_dist_min
                                    : M_ball_to_player_dist_max;
            const auto _dir_vector = _edge_segment.direction();

            const auto _player_id = _vertex_to_pid_map[_player_vh];
            const auto _player = soccer::get_player( _player_id, _sample );
            auto _new_player_pos = _ball_vh->point() + _dir_vector.vector() * _radius;
            soccer::set_x( _new_player_pos.x(), _player );
            soccer::set_y( _new_player_pos.y(), _player );
            soccer::set_player( _player_id, _player, _sample );

            _ball_edges_circulator++;
        } while ( _ball_edges_circulator != _ball_edges_circulator_end );

        assert( _num_players != 0 );


        // 3) TODO Check that player-player spacing is kept

    }

};


} // end namespace soccer

#endif // FORMATION_SAMPLE_TRANSFORM_HPP
