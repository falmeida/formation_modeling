#ifndef SOCCER_FORMATION_SAMPLE_ALGORITHMS_HPP
#define SOCCER_FORMATION_SAMPLE_ALGORITHMS_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "formation_sample_concept.hpp"

#include <soccer/geometry.hpp>
#include <soccer/algorithms/distance.hpp>

#include <boost/property_map/property_map.hpp>
#include <boost/tuple/tuple.hpp>

#include <algorithm>
#include <iterator>

namespace soccer {


/*!
 * \brief Default implementation for determining the number of samples in storage
 * \param storage formation sample storage
 * \return the number of samples stored
 */
template < typename FormationSampleStorage >
typename std::iterator_traits< typename formation_sample_iterator_type< FormationSampleStorage >::type >::difference_type
num_samples( const FormationSampleStorage& storage )
{
    typename formation_sample_iterator_type< FormationSampleStorage >::type _sample_begin, _sample_end;
    boost::tie( _sample_begin, _sample_end ) = get_samples( storage );
    return std::distance( _sample_begin, _sample_end );
}


/*!
 * \brief Default implementation for determining the number of players in a sample
 * \param sample formation sample storage
 * \return the number of players in the sample
 */
template < typename FormationSample >
typename std::iterator_traits< typename player_iterator_type< FormationSample >::type >::difference_type
num_players( const FormationSample& sample )
{
    typename player_iterator_type< FormationSample >::type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( sample );
    return std::distance( _player_begin, _player_end );
}


/*!
 * \brief Create a triangulation from a formation sample
 * \param _sample the formation sample
 * \param _ball_vh the ball vertex
 * \param _vertex_to_pid_pmap Triangulation vertex to player id property map
 * \param _triangulation Triangulation
 */
template < typename FormationSample,
           typename VertexToPlayerIdPropertyMap,
           typename Triangulation = ::CGAL::Delaunay_triangulation_2< ::CGAL::Simple_cartesian< float > >
         >
void
create_formation_sample_triangulation( const FormationSample& _sample,
                                       typename Triangulation::Vertex_handle* _ball_vh,
                                       VertexToPlayerIdPropertyMap _vertex_to_pid_pmap,
                                       Triangulation& _triangulation
                                     )
{
    BOOST_CONCEPT_ASSERT(( soccer::FormationSampleConcept<FormationSample> ));

    typedef typename soccer::player_id_type< FormationSample >::type sample_player_id_type;
    typedef typename soccer::player_iterator_type< FormationSample >::type sample_player_iterator_type;

    BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< VertexToPlayerIdPropertyMap,
                                                              typename Triangulation::Vertex_handle > ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< VertexToPlayerIdPropertyMap >::value_type,
                                              sample_player_id_type >::value ),
                            "VertexToPlayerIdPropertyMap value type mismatch");

    typedef typename Triangulation::Point triangulation_point_type;

    const auto _ball_position = get_position( get_ball( _sample ));
    triangulation_point_type _ball_pt( get_x( _ball_position ), get_y( _ball_position ));
    *_ball_vh = _triangulation.insert( _ball_pt );

    sample_player_iterator_type _player_begin,_player_end;
    boost::tie( _player_begin, _player_end ) = get_players( _sample );
    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        const auto _player = get_player( *itp, _sample );
        const auto _player_pos = get_position( _player );
        triangulation_point_type _player_pt( get_x( _player_pos ), get_y( _player_pos ));
        auto _player_vh = _triangulation.insert( _player_pt );
        boost::put( _vertex_to_pid_pmap, _player_vh, *itp );
    }
}


/*!
 * \brief Create a triangulation from a formation sample storage
 * \param _storage the formation sample storage
 * \param _vertex_to_sid_pmap Triangulation vertex to sample id property map
 * \param _triangulation Triangulation
 */
template < typename FormationSampleStorage,
           typename VertexToSampleIdPropertyMap,
           typename Triangulation = ::CGAL::Delaunay_triangulation_2< ::CGAL::Simple_cartesian< float > >
         >
void
create_formation_sample_storage_triangulation( const FormationSampleStorage& _storage,
                                               typename Triangulation::Vertex_handle* _ball_vh,
                                               VertexToSampleIdPropertyMap _vertex_to_sid_pmap,
                                               Triangulation& _triangulation
                                             )
{
    // BOOST_CONCEPT_ASSERT(( soccer::FormationSampleStorageConcept<FormationSampleStorage> ));

    typedef typename soccer::formation_sample_iterator_type< FormationSampleStorage >::type formation_sample_iterator_type;
    typedef typename soccer::formation_sample_id_type< FormationSampleStorage >::type formation_sample_id_type;

    BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< VertexToSampleIdPropertyMap,
                                                              typename Triangulation::Vertex_handle > ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< VertexToSampleIdPropertyMap >::value_type,
                                              formation_sample_id_type >::value ),
                            "VertexToSampleIdPropertyMap value type mismatch");

    typedef typename Triangulation::Point triangulation_point_type;

    formation_sample_iterator_type _sample_begin, _sample_end;
    boost::tie( _sample_begin, _sample_end ) = get_samples( _storage );
    for ( auto its = _sample_begin; its != _sample_end ; its++ )
    {
        const auto _sample = get_sample( *its, _storage );
        const auto _ball_position = get_position( get_ball( _sample ));
        triangulation_point_type _ball_pt( get_x( _ball_position ), get_y( _ball_position ));
        *_ball_vh = _triangulation.insert( _ball_pt );

        boost::put( _vertex_to_sid_pmap, _ball_vh, *its );
    }
}


/*!
 * \brief Find the player id nearest to a position in a sample
 * \param position The position to consider
 * \param sample The formation sample to consider
 */
template < typename Position,
           typename FormationSample >
typename soccer::player_id_type< FormationSample >::type
nearest_player_id( Position const& position,
                   const FormationSample& sample )
{
    typedef typename soccer::player_iterator_type< FormationSample >::type player_iterator_type;
    typedef typename soccer::player_id_type< FormationSample >::type player_id_descriptor_type;
    player_iterator_type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = soccer::get_players( sample );
    assert( _player_begin != _player_end );

    const auto _ppos = get_position( soccer::get_player( *_player_begin, sample)  );
    auto _nearest_dist = distance( position, _ppos );

    auto _nearest_player_it = std::max_element( _player_begin,
                                                _player_end,
                                                [&sample, &position, &_nearest_dist] ( const player_id_descriptor_type other_pid,
                                                                                       const player_id_descriptor_type /* nearest_pid */ )
                                                {
                                                    const auto _ppos = get_position( get_player( other_pid, sample ) );
                                                    const auto _dist = distance( position, _ppos );
                                                    if ( _dist < _nearest_dist )
                                                    {
                                                        _nearest_dist = _dist;
                                                        return true;
                                                    }
                                                    return false;
                                                }
                                              );
    return *_nearest_player_it;
}



/*!
 * \brief Find the player id furthest to a position in a sample
 * \param position The position to consider
 * \param sample The formation sample to consider
 */
template < typename Position,
           typename FormationSample >
typename soccer::player_id_type< FormationSample >::type
furthest_player_id( Position const& position,
                   const FormationSample& sample )
{
    typedef typename soccer::player_iterator_type< FormationSample >::type player_iterator_type;
    typedef typename soccer::player_id_type< FormationSample >::type player_id_descriptor_type;
    player_iterator_type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = soccer::get_players( sample );
    assert( _player_begin != _player_end );

    const auto _ppos = get_position( soccer::get_player( *_player_begin, sample)  );
    auto _furthest_dist = distance( position, _ppos );

    auto _furthest_player_it = std::max_element( _player_begin,
                                                _player_end,
                                                [&sample, &position, &_furthest_dist] ( const player_id_descriptor_type other_pid,
                                                                                       const player_id_descriptor_type /* nearest_pid */ )
                                                {
                                                    const auto _ppos = get_position( get_player( other_pid, sample ) );
                                                    const auto _dist = distance( position, _ppos );
                                                    if ( _dist > _furthest_dist )
                                                    {
                                                        _furthest_dist = _dist;
                                                        return true;
                                                    }
                                                    return false;
                                                }
                                              );
    return *_furthest_player_it;
}


/*!
 * \brief Find the player id furthest to a position in a sample
 * \param position The position to consider
 * \param sample The formation sample to consider
 */
template < typename FormationSample >
void
swap_players( const typename soccer::player_id_type< FormationSample >::type pid1,
              const typename soccer::player_id_type< FormationSample >::type pid2,
              FormationSample& sample )
{
    const auto _player_tmp = get_player( pid1, sample );
    set_player( pid1, get_player( pid2, sample ), sample );
    set_player( pid2, _player_tmp, sample );
}


}

#endif // SOCCER_FORMATION_SAMPLE_ALGORITHMS_HPP
