#ifndef RCSSLOGPLAYER_SOCCER_MODEL_H
#define RCSSLOGPLAYER_SOCCER_MODEL_H

#include <rcsslogplayer/types.h>

namespace soccer {
namespace model {

float pitch_top_left_y( const rcss::rcg::ServerParamT& model );
float pitch_top_left_x( const rcss::rcg::ServerParamT& model );

float pitch_width( const rcss::rcg::ServerParamT& model );
float pitch_length( const rcss::rcg::ServerParamT& model );

float penalty_area_width( const rcss::rcg::ServerParamT& model );
float penalty_area_length( const rcss::rcg::ServerParamT& model );

float goal_area_width( const rcss::rcg::ServerParamT& model );
float goal_area_length( const rcss::rcg::ServerParamT& model );

float goal_width( const rcss::rcg::ServerParamT& model );
float goal_depth( const rcss::rcg::ServerParamT& model );

float center_circle_radius( const rcss::rcg::ServerParamT& model );
float corner_arc_radius( const rcss::rcg::ServerParamT& model );

float penalty_spot_dist( const rcss::rcg::ServerParamT& model );

unsigned short num_players( const rcss::rcg::ServerParamT& model );


// Make this function generic
inline
bool in_upper_field( const float y,
                     const rcss::rcg::ServerParamT& model )
{
        return y > pitch_top_left_y( model ) &&
               y <= pitch_top_left_y( model ) + pitch_width( model ) / 2.0f;
}

inline
bool in_left_midfield( const float x,
                       const rcss::rcg::ServerParamT& model )
{
        return x > pitch_top_left_y( model ) &&
               x <= pitch_top_left_x( model ) + pitch_length( model ) / 2.0f;
}

} // end namespace model
} // end namespace soccer

#endif // RCSSLOGPLAYER_SOCCER_MODEL_H
