#ifndef RCSC_RCG_POSITION_MODELER_H
#define RCSC_RCG_POSITION_MODELER_H

#include <rcsslogplayer/handler.h>

#include <iostream>

#include <list>
#include <map>
#include <functional>

namespace rcss {
namespace rcg {

/*!
 * \brief The RcgHandler class
 */
class RcgHandler
    : public Handler {
private:    
    // Store server params
    ServerParamT M_server_params;
    PlayerParamT M_player_params;
    std::map<int, PlayerTypeT> M_player_types;

    //!< log file data
    std::string M_log_filename;
    std::istream* M_log_file_is;

    //! keep track of current playmode
    int M_playmode_time;
    PlayMode M_playmode;
    //! keep track of game teams
    TeamT M_left_team;
    TeamT M_right_team;

    //! team in possession
    Side M_team_in_possession;

    //! keep track of current log version
    int M_version;
    int M_time;

    std::list< std::function< void (DispInfoT) > > M_observers;
public:
    typedef rcss::rcg::DispInfoT state_type;
    typedef rcss::rcg::ServerParamT soccer_model_type;

    RcgHandler( );
    virtual ~RcgHandler();

    bool openLogFile( const std::string& log_file_name );
    const std::string& getLogFileName( );

    std::istream* getLogFileStream();

    virtual
    void doHandleLogVersion( int ver );

    virtual
    int doGetLogVersion() const;

    virtual
    void doHandleShowInfo( const ShowInfoT& show_info );

    virtual
    void doHandleMsgInfo( const int,
                          const int,
                          const std::string & );

    virtual
    void doHandlePlayMode( const int time ,
                           const PlayMode playmode );

    virtual
    void doHandleTeamInfo( const int,
                           const TeamT& left_team,
                           const TeamT& right_team);

    virtual
    void doHandleDrawClear( const int );

    virtual
    void doHandleDrawPointInfo( const int,
                                const PointInfoT & );

    virtual
    void doHandleDrawCircleInfo( const int,
                                 const CircleInfoT & );

    virtual
    void doHandleDrawLineInfo( const int,
                               const LineInfoT & );

    virtual
    void doHandlePlayerType( const PlayerTypeT& player_type );

    virtual
    void doHandlePlayerParam( const PlayerParamT& player_params );

    virtual
    void doHandleServerParam( const ServerParamT& server_params );

    virtual
    void doHandleEOF();

    const ServerParamT& serverParams() const;
    const PlayerParamT& playerParams() const;
    const std::map< int, PlayerTypeT>& playerTypes() const;

    void registerObserver( std::function< void (DispInfoT) > observer );
    // void unregisterObserver( IObserver* observer );
private:
    void close();
};

} // end namespace rcg
} // end namespace rcsc

#endif // RCSC_RCG_POSITION_MODELER_H
