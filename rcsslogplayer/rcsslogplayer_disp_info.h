#ifndef RCSSLOGPLAYER_DISP_INFO_H
#define RCSSLOGPLAYER_DISP_INFO_H

#include "rcsslogplayer_soccer_model.h"

#include <rcsslogplayer/types.h>

#include <soccer/state_traits.hpp>

//! Soccer types registration
#include <soccer/register/state.hpp>
#include <soccer/register/ball.hpp>
#include <soccer/register/player.hpp>

#include <soccer/core/access.hpp>


#include <boost/iterator/counting_iterator.hpp>

#include <map>
#include <algorithm>
#include <functional>
#include <iterator>
#include <cassert>

namespace rcss {
namespace rcg {    

    typedef std::pair< float, float > position_type;
    typedef std::pair< float, float > velocity_type;
    typedef boost::counting_iterator< unsigned short > player_iterator_type;

    //
    // BallT Free Functions
    //
    std::pair< float, float >
    get_position ( const rcss::rcg::BallT& ball ) { return std::make_pair( ball.x_, ball.y_ ); }

    std::pair< float, float >
    get_velocity( const rcss::rcg::BallT& ball ) { return std::make_pair( ball.vx_, ball.vy_ ); }

    void
    set_position ( const std::pair< float, float >& pos, rcss::rcg::BallT& ball ) { ball.x_ = pos.first; ball.y_ = pos.second; }

    void
    set_velocity( const std::pair< float, float >& vel, rcss::rcg::BallT& ball ) { ball.vx_ = vel.first; ball.vy_ = vel.second; }

    //
    // PlayerT Free Functions
    //
    std::pair< float, float >
    get_position ( const rcss::rcg::PlayerT& player ) { return std::make_pair( player.x_, player.y_ ); }

    std::pair< float, float >
    get_velocity( const rcss::rcg::PlayerT& player ) { return std::make_pair( player.vx_, player.vy_ ); }

    rcss::rcg::Side
    get_side( const rcss::rcg::PlayerT& player ) { return player.side(); }

    Int16
    get_number( const rcss::rcg::PlayerT& player ) { return player.unum_; }

    void
    set_position ( const std::pair< float, float >& pos, rcss::rcg::PlayerT& player ) { player.x_ = pos.first; player.y_ = pos.second; }

    void
    set_velocity( const std::pair< float, float >& vel, rcss::rcg::PlayerT& player ) { player.vx_ = vel.first; player.vy_ = vel.second; }

    void
    set_side( const rcss::rcg::Side& side, rcss::rcg::PlayerT& player ) { assert( side != NEUTRAL ); player.side_ = side == LEFT ? 'l' : 'r'; }

    void
    set_number( const Int16& number, rcss::rcg::PlayerT& player ) { player.unum_ = number; }

    //
    // DispInfoT
    //
    BallT get_ball( const rcss::rcg::DispInfoT& state ) { return state.show_.ball_; }
    void set_ball( const BallT& ball, rcss::rcg::DispInfoT& state ) { state.show_.ball_ = ball; }

    std::pair< player_iterator_type, player_iterator_type >
    get_players( const rcss::rcg::DispInfoT&  )
    {
        return std::make_pair( player_iterator_type(0), player_iterator_type( 22 ) );
    }

    PlayerT get_player( const int pid, const rcss::rcg::DispInfoT& state )
    {
        assert( pid >= 0 && pid < 11 * 2 );

        return state.show_.player_[pid];
    }
    void set_player( const int pid,
                     const PlayerT& player,
                     rcss::rcg::DispInfoT& state )
    {
        assert( pid >= 0 && pid < 11 * 2 );

        state.show_.player_[pid] = player;
    }

    Int32 get_time( const DispInfoT& state ) { return state.show_.time_; }
    void set_time( const Int32 time, DispInfoT& state ) { state.show_.time_ = time; }

    PlayMode get_playmode( const DispInfoT& state ) { return state.pmode_; }
    void set_playmode( const PlayMode pmode, DispInfoT& state ) { state.pmode_ = pmode; }
}
}

// Register state traits of rcss::rcg::DispInfoT
/* namespace soccer { namespace traits {
SOCCER_DETAIL_SPECIALIZE_STATE_TRAITS( rcss::rcg::DispInfoT, \
                                       rcss::rcg::BallT, \
                                       std::size_t, \
                                       rcss::rcg::PlayerT, \
                                       rcss::rcg::ShowInfoTPlayerIterator )
}}*/
SOCCER_REGISTER_DYNAMIC_BALL_GET_SET_FREE(rcss::rcg::BallT, \
                                          rcss::rcg::position_type, rcss::rcg::get_position, rcss::rcg::set_position, \
                                          rcss::rcg::velocity_type, rcss::rcg::get_velocity, rcss::rcg::set_velocity)

SOCCER_REGISTER_DYNAMIC_PLAYER_GET_SET_FREE(rcss::rcg::PlayerT, \
                                            rcss::rcg::position_type, rcss::rcg::get_position, rcss::rcg::set_position, \
                                            rcss::rcg::velocity_type, rcss::rcg::get_velocity, rcss::rcg::set_velocity, \
                                            rcss::rcg::Side, rcss::rcg::get_side, rcss::rcg::set_side, \
                                            rcss::rcg::Int16, rcss::rcg::get_number, rcss::rcg::set_number )

SOCCER_REGISTER_STATE_GET_SET_FREE(rcss::rcg::DispInfoT, \
                                   rcss::rcg::BallT, rcss::rcg::get_ball, rcss::rcg::set_ball, \
                                   int, rcss::rcg::player_iterator_type, rcss::rcg::get_players, \
                                   rcss::rcg::PlayerT, rcss::rcg::get_player, rcss::rcg::set_player, \
                                   rcss::rcg::UInt32, rcss::rcg::get_time, rcss::rcg::set_time, \
                                   rcss::rcg::PlayMode, rcss::rcg::get_playmode, rcss::rcg::set_playmode )

namespace soccer {

bool is_game_stopped( const rcss::rcg::DispInfoT& state )
{
    return state.pmode_ != rcss::rcg::PM_PlayOn;
}

bool is_setplay_mode( const rcss::rcg::DispInfoT& state )
{
    return state.pmode_ == rcss::rcg::PM_GoalKick_Left ||
           state.pmode_ == rcss::rcg::PM_GoalKick_Right ||
           state.pmode_ == rcss::rcg::PM_FreeKick_Left ||
           state.pmode_ == rcss::rcg::PM_FreeKick_Right||
           state.pmode_ == rcss::rcg::PM_IndFreeKick_Left ||
           state.pmode_ == rcss::rcg::PM_IndFreeKick_Right ||
           state.pmode_ == rcss::rcg::PM_CornerKick_Left ||
           state.pmode_ == rcss::rcg::PM_CornerKick_Right ||
           state.pmode_ == rcss::rcg::PM_KickOff_Left ||
           state.pmode_ == rcss::rcg::PM_KickOff_Right ||
           state.pmode_ == rcss::rcg::PM_KickIn_Left ||
           state.pmode_ == rcss::rcg::PM_KickIn_Right;
}


const rcss::rcg::TeamT& get_left_team( const rcss::rcg::DispInfoT& state );
const rcss::rcg::TeamT& get_right_team( const rcss::rcg::DispInfoT& state );

void set_left_team( const rcss::rcg::TeamT& team, rcss::rcg::DispInfoT& state );
void set_right_team( const rcss::rcg::TeamT& team, rcss::rcg::DispInfoT& state );

inline rcss::rcg::Side
opposite_side( const rcss::rcg::Side side )
{
    assert( side != rcss::rcg::NEUTRAL );

    return side == rcss::rcg::LEFT
            ? rcss::rcg::RIGHT
            : rcss::rcg::LEFT;
}

} // end namespace soccer


#endif // RCSSLOGPLAYER_DISP_INFO_H
