#ifndef SOCCER_FORMATION_SAMPLE_GENERATOR_HPP
#define SOCCER_FORMATION_SAMPLE_GENERATOR_HPP

#include <soccer/rcsslogplayer/rcsslogplayer_disp_info.h>

#include <robocup/2d/game_log.h>

#include "formation_sample_concept.hpp"

#include <soccer/core/access.hpp>
#include <soccer/algorithms/state_algorithms.hpp>
#include <soccer/algorithms/pitch_algorithms.hpp>

namespace soccer {

//
// Formation Sample Creator
//

template < typename FormationSample,
           typename FormationSamplePlayerIdConverter,
           typename PlayerIdInputIterator,
           typename State >
/*!
 * \brief Create a formation sample from a state representation
 * \param _pid_generator Unique player identifier generator
 * \param _ball_significance_evaluator Ball data quality evaluator
 * \param _player_significance_evaluator Player data quality evaluator
 * \param _pid_begin iterator to the first player identifier to consider in state
 * \param _pid_end iterator to the end player identifier to consider in state
 * \param _state state representation
 * \return Formation sample with pre-filled data based on state representation
 */
FormationSample
create_formation_sample( FormationSamplePlayerIdConverter _pid_converter,
                         // BallSignificanceEvaluator _ball_significance_evaluator,
                         // PlayerSignificanceEvaluator _player_significance_evaluator,
                         PlayerIdInputIterator _pid_begin,
                         const PlayerIdInputIterator _pid_end,
                         State const& _state )
{
    FormationSample _new_formation_sample;

    soccer::set_ball( soccer::get_ball( _state ), _new_formation_sample );

    for ( ; _pid_begin != _pid_end ; _pid_begin++ )
    {
        const auto _player_id = _pid_converter( *_pid_begin );
        soccer::set_player( _player_id,
                            soccer::get_player( *_pid_begin, _state ),
                            _new_formation_sample );
    }

    return _new_formation_sample;
}

/*!
 * \brief Create formation sample with info from rcss::rcg::DispInfoT
 */
/*template < typename FormationSample,
           typename FormationSamplePlayerIdGenerator >
struct formation_sample_from_dispinfo
    : public std::unary_function< rcss::rcg::DispInfoT, FormationSample >
{
    rcss::rcg::Side M_team_side;
    robocup::soccer::GameLog* const M_game_log;
    FormationSamplePlayerIdGenerator* M_formation_sample_pid_generator;


    formation_sample_from_dispinfo( FormationSamplePlayerIdGenerator& _formation_sample_pid_generator,
                                    rcss::rcg::Side team_side = rcss::rcg::NEUTRAL,
                                    robocup::soccer::GameLog* _game_log = NULL )
        : M_team_side( team_side )
        , M_game_log( _game_log )
        , M_formation_sample_pid_generator( &_formation_sample_pid_generator )
    {

    }

    formation_sample_from_dispinfo( const formation_sample_from_dispinfo& other )
        : M_team_side( other.M_team_side )
        , M_game_log( other.M_game_log )
    {

    }

    void set_team_side( const rcss::rcg::Side team_side )
    {
        this->M_team_side = team_side;
    }

    void set_game_log( robocup::soccer::GameLog* const game_log )
    {
        this->M_game_log = game_log;
    }

    void set_player_id_generator( FormationSamplePlayerIdGenerator* _formation_sample_pid_generator )
    {
        assert( _formation_sample_pid_generator = NULL );
        this->M_game_log = _formation_sample_pid_generator;
    }

    FormationSample
    operator()( const rcss::rcg::DispInfoT& state )
    {
        assert( this->M_team_side != rcss::rcg::NEUTRAL &&
                this->M_game_log != NULL &&
                this->M_formation_sample_pid_generator != NULL );

        auto _pid_skip_fn = [&] ( const player_id_type< State >::type pid ) { return get_side( get_player( pid, state ) ) != M_team_side; };
        typename player_filter_iterator_generator< State >::type _filtered_player_begin, _filtered_player_end;
        boost::tie( _filtered_player_begin, _filtered_player_end ) = make_player_filter_iterator( _pid_skip_fn, state);

        auto _new_sample = create_formation_sample<FormationSample>( *M_formation_sample_pid_generator,
                                                                     _filtered_player_begin,
                                                                     _filtered_player_end,
                                                                     state );
        set_info( std::make_pair( *M_game_log, state ), _new_sample );
        return _new_sample;
    }
}; */


template < typename Position,
           typename Pitch,
           typename FormationSample >
/*!
 * \brief Generate a sample from a position and a reference sample
 * \param position the ball position
 * \param sample the reference sample
 * \param out_sample the new sample
 * \return true if successfully generated or false otherwise
 */
bool generate_sample_from_sample( Position position,
                                  Pitch const& pitch,
                                  FormationSample const& _reference_sample,
                                  FormationSample& out_sample )
{
    typedef typename soccer::player_iterator_type< FormationSample >::type sample_player_iterator;

    // Determine ball displacement vector
    soccer::ConstrainPositionToPitch< Pitch > _constrain_position_to_pitch( pitch );
    _constrain_position_to_pitch( position );
    auto _ball = get_ball( _reference_sample );
    auto _ball_position = get_position( _ball );
    auto dx = get_x( position ) - get_x( _ball_position );
    auto dy = get_y( position ) - get_y( _ball_position );


    sample_player_iterator _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = soccer::get_players( _reference_sample );

    // Generate ball position
    set_x( get_x( position ), _ball_position );
    set_y( get_y( position ), _ball_position );
    set_position( _ball_position, _ball );
    set_ball( _ball, out_sample );

    for ( auto itp = _player_begin; itp != _player_end ; itp++ )
    {
        auto _player = get_player( *itp, _reference_sample );
        auto _player_pos = get_position( _player );

        auto px = get_x( _player_pos ) + dx;
        auto py = get_y( _player_pos ) + dy;

        // TODO Truncate player position within field boundaries
        set_x( px, _player_pos );
        set_y( py, _player_pos );
        _constrain_position_to_pitch( _player_pos );
        set_position( _player_pos, _player );


        // Generate player position
        set_player( *itp, _player, out_sample );
    }

    // TODO Copy info???
    set_info( soccer::get_info( _reference_sample ), out_sample );

    return true;
}

template < typename Position,
           typename FormationSampleStorage >
/*!
 * \brief Generate a sample from a position based on a formation sample storage
 * \param position the ball position
 * \param storage the formation sample storage
 * \param out_sample the new sample
 * \return true if successfully generated or false otherwise
 */
bool generate_position_sample_from_storage( Position const& position,
                                            const FormationSampleStorage& storage,
                                            typename soccer::formation_sample_type< FormationSampleStorage>::type& out_sample )
{
    typedef typename soccer::formation_sample_type< FormationSampleStorage >::type formation_sample_type;
    typedef typename soccer::player_iterator_type< formation_sample_type >::type sample_player_iterator;

    // Find nearest sample
    const auto _nearest_sample_id = soccer::nearest_sample_id( position, storage );
    // assert( _nearest_sample_id );
    const auto _nearest_sample = soccer::get_sample( _nearest_sample_id, storage );

    return generate_sample_from_sample( position, _nearest_sample, out_sample );
}


} // end namespace soccer

#endif // SOCCER_FORMATION_SAMPLE_GENERATOR_HPP
