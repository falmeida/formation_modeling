#ifndef SOCCER_FORMATION_SAMPLE_STORAGE_ALGORITHMS_HPP
#define SOCCER_FORMATION_SAMPLE_STORAGE_ALGORITHMS_HPP

#include "formation_sample_concept.hpp"

#include <soccer/algorithms/distance.hpp>

#include <boost/tuple/tuple.hpp>

namespace soccer {

template < typename Position,
           typename FormationSampleStorage >
typename formation_sample_id_type< FormationSampleStorage >::type
nearest_sample_id( Position const& position,
                   const FormationSampleStorage& storage )
{
    typedef typename formation_sample_iterator_type< FormationSampleStorage >::type formation_formation_sample_iterator_t;
    typedef typename formation_sample_id_type< FormationSampleStorage >::type formation_formation_sample_id_t;

    formation_formation_sample_iterator_t _sample_begin, _sample_end;
    boost::tie( _sample_begin, _sample_end ) = get_samples( storage );

    assert( _sample_begin != _sample_end );

    const auto _sample = get_sample( *_sample_begin, storage );
    const auto _ball = get_ball( _sample );
    const auto _ball_position = get_position( _ball );
    auto _nearest_sample_dist = distance( position, _ball_position );

    auto _nearest_sample_it = std::min_element( _sample_begin,
                                                _sample_end,
                                              [ &position, &storage, &_nearest_sample_dist ] ( const formation_formation_sample_id_t sid,
                                                                                               const formation_formation_sample_id_t /* nearest_sid */ )
                                                {
                                                    const auto _sample = get_sample( sid, storage );
                                                    const auto _ball = get_ball( _sample );
                                                    const auto _ball_position = get_position( _ball );
                                                    const auto _dist = distance( position, _ball_position );
                                                    if ( _dist < _nearest_sample_dist )
                                                    {
                                                        _nearest_sample_dist = _dist;
                                                        return true;
                                                    }
                                                    return false;
                                                }
                                             );
    assert( _nearest_sample_it != _sample_end );

    return *_nearest_sample_it;
}

}

#endif // SOCCER_FORMATION_SAMPLE_STORAGE_ALGORITHMS_HPP
