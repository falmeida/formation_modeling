#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "rcg_position_modeler.h"

#ifdef HAVE_LIBZ
#include <rcsslogplayer/gzfstream.h>
#endif


#include <fstream>
#include <algorithm>

#include <cassert>

namespace rcss {
namespace rcg {

/*-------------------------------------------------------------------*/
/*!

*/
RcgHandler::RcgHandler( )
    : M_version( 0 )
    , M_time( -1 )
    , M_playmode( PM_Null )
    , M_log_filename()
    , M_log_file_is( static_cast< std::istream * >( 0 ) )
{

}

RcgHandler::~RcgHandler()
{
    this->close();
}

bool RcgHandler::openLogFile( const std::string& log_filename )
{
    std::cout << log_filename << std::endl;
    // open the input stream
    if ( log_filename.length() > 3
         && log_filename.compare( log_filename.length() - 3, 3, ".gz" ) == 0 )
    {
#ifndef HAVE_LIBZ
        std::cerr << "No zlib support!" << std::endl;
        return false;
#else
        if ( M_log_file_is != NULL ) { delete M_log_file_is; }
        M_log_file_is = new rcss::gzifstream( log_filename.c_str() );
#endif
    }
    else
    {
        if ( M_log_file_is != NULL ) { delete M_log_file_is; }
        M_log_file_is = new std::ifstream( log_filename.c_str() );
    }

    if ( ! *M_log_file_is )
    {
        std::cerr << "could not open the input file ." << log_filename
                  << std::endl;
        return false;
    }

    std::cerr << ": input file = [" << log_filename << ']' << std::endl;
    M_log_filename = log_filename;
    return true;

}

const std::string& RcgHandler::getLogFileName( )
{
    return M_log_filename;
}

std::istream* RcgHandler::getLogFileStream( )
{
    assert( M_log_file_is != NULL );

    return M_log_file_is;
}

void RcgHandler::doHandleLogVersion( int ver )
{
    M_version = ver;
}

int RcgHandler::doGetLogVersion() const
{
    return M_version;
}

void RcgHandler::doHandleShowInfo( const ShowInfoT& show_info )
{
    M_time = show_info.time_;
    DispInfoT disp_info;
    disp_info.show_ = show_info;
    disp_info.team_[0] = M_left_team;
    disp_info.team_[1] = M_right_team;
    disp_info.pmode_ = M_playmode;

    // Should collect sample data?
    for ( auto it = M_observers.begin(); it != M_observers.end(); it++ )
    {
        (*it)( disp_info );
    }
}

void RcgHandler::doHandleMsgInfo( const int,
                                          const int,
                                          const std::string & )
{ }

void RcgHandler::doHandlePlayMode( const int time,
                                           const PlayMode playmode )
{
    M_playmode_time = time;
    M_playmode = playmode;
}

void RcgHandler::doHandleTeamInfo( const int,
                                           const TeamT& left_team,
                                           const TeamT& right_team )
{
    M_left_team = left_team;
    M_right_team = right_team;
}

void RcgHandler::doHandleDrawClear( const int )
{

}

void RcgHandler::doHandleDrawPointInfo( const int,
                                                const PointInfoT & )
{

}

void RcgHandler::doHandleDrawCircleInfo( const int,
                             const CircleInfoT & )
{

}

void RcgHandler::doHandleDrawLineInfo( const int,
                           const LineInfoT & )
{

}

void RcgHandler::doHandlePlayerType( const PlayerTypeT& player_type )
{
    M_player_types[player_type.id_] = player_type;
}

void RcgHandler::doHandlePlayerParam( const PlayerParamT& player_params )
{
    M_player_params = player_params;
}

void RcgHandler::doHandleServerParam( const ServerParamT& server_params )
{
    M_server_params = server_params;
}

void RcgHandler::doHandleEOF()
{
    std::clog << "Finished parsing logfile" << std::endl;
}

const ServerParamT& RcgHandler::serverParams() const
{
    return M_server_params;
}

const PlayerParamT& RcgHandler::playerParams() const
{
    return M_player_params;
}

const std::map< int, PlayerTypeT>& RcgHandler::playerTypes() const
{
    return M_player_types;
}

void RcgHandler::registerObserver( std::function< void (DispInfoT) > observer )
{
    M_observers.push_back( observer );
}

/* void RcgHandler::unregisterObserver( IObserver* observer )
{
    M_observers.erase( std::find( M_observers.begin(),
                                  M_observers.end(),
                                  observer ) );
}*/

void RcgHandler::close()
{
    if ( M_log_file_is &&
         M_log_file_is != &std::cin )
    {
        delete M_log_file_is;
        M_log_file_is = static_cast< std::istream * >( 0 );
    }
}


}
}
