#ifndef SOCCER_FORMATION_SAMPLE_STORAGE_TRANSFORM_HPP
#define SOCCER_FORMATION_SAMPLE_STORAGE_TRANSFORM_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

#include "formation_sample_proxy.hpp"

#include "formation_sample_algorithms.hpp"
#include "formation_sample_storage_policy.hpp"

#include "sample_matching.h"

#include <boost/property_map/property_map.hpp>

#include <boost/graph/breadth_first_search.hpp>

#include <boost/function_output_iterator.hpp>

#include <boost/mpl/has_xxx.hpp>
#include <boost/mpl/if.hpp>

#include <functional>
#include <limits>

#include <cassert>

// namespace soccer {

template < typename FormationSample >
/*!
 * \brief Furthest player id from ball in sample functor
 */
struct furthest_player_id_from_ball
    : std::unary_function< FormationSample, typename soccer::player_id_type< FormationSample >::type >
{

    furthest_player_id_from_ball( )
    {

    }

    typename soccer::player_id_type< FormationSample >::type
    operator()( const FormationSample& sample )
    {
        const auto _ball_position = get_position( get_ball( sample ));
        const auto _furthest_player_id = furthest_player_id( _ball_position, sample );
        return _furthest_player_id;
    }
};

template < typename FormationSampleStorage,
           typename PlayerIdDiscarder = furthest_player_id_from_ball< typename soccer::formation_sample_type< FormationSampleStorage >::type >
         >
struct offensive_sample_generator
    : public boost::default_bfs_visitor
{
    // BOOST_CONCEPT_ASSERT(( FormationSampleStorageConcept< FormationSampleStorage > ));
    typedef typename soccer::formation_sample_id_type< FormationSampleStorage >::type formation_formation_sample_id_type;
    typedef typename soccer::formation_sample_type< FormationSampleStorage >::type formation_sample_type;
    typedef typename soccer::player_id_type< formation_sample_type >::type player_id_descriptor_type;
    typedef typename soccer::player_iterator_type< formation_sample_type >::type player_iterator_type;

    typedef std::map< player_id_descriptor_type, player_id_descriptor_type > player_id_map_type;

    FormationSampleStorage* M_storage;
    PlayerIdDiscarder M_player_id_discarder;
    player_id_descriptor_type M_bowner_id;


    std::map< formation_formation_sample_id_type, std::set< formation_formation_sample_id_type > > M_predecessor_map;



    offensive_sample_generator( FormationSampleStorage& storage,
                                const player_id_descriptor_type bowner_id )
        : M_storage( &storage )
        , M_bowner_id( bowner_id )
    {

    }

    template < typename Vertex, typename Graph >
    void discover_vertex(Vertex , const Graph & ) const
    {

    }

    template < typename Edge, typename Graph >
    void tree_edge(Edge e, const Graph & g)
    {
        std::clog << "Tree edge=(" << boost::source( e, g ) << "," <<
                     boost::target( e, g ) << ") ";
        // save predecessor
        std::set< formation_formation_sample_id_type > _neighbor_samples;
        _neighbor_samples.insert( boost::source( e, g ) );
        M_predecessor_map[ boost::target( e, g ) ] = _neighbor_samples;
    }

    template < typename Vertex, typename Graph >
    void examine_vertex(Vertex u, const Graph & g)
    {
        const auto _sample_id = u;
        std::clog << std::endl << "Examining sample " << _sample_id << std::endl;

        auto& _sample = get_sample( _sample_id, *M_storage );
        auto _ball_position = soccer::get_position( soccer::get_ball( _sample ) );
        auto _nearest_pid = nearest_player_id( _ball_position, _sample );
        auto _nearest_player = get_player( _nearest_pid, _sample );

        // TODO Decide which player id position should be discarded
        // auto _pid_to_discard = M_player_id_discarder( _sample );

        // set nearest player id position to the ball position
        soccer::set_position( _ball_position, _nearest_player );

        // auto _tmp_player = soccer::get_player( M_bowner_id, _sample );
        // soccer::set_player( M_bowner_id, _nearest_player, _sample );
        soccer::set_player( _nearest_pid, _nearest_player, _sample );
        if ( _nearest_pid != M_bowner_id )
        {
            std::cout << "Nearest_pid=" << _nearest_pid << " <> bowner_id="  << M_bowner_id << " => SWAP!!" << std::endl;
            swap_players( _nearest_pid, M_bowner_id, _sample );
        }

        auto _parent_sample_id_it = M_predecessor_map.find( u );
        if ( _parent_sample_id_it != M_predecessor_map.end() )
        {
            // Make additional swap in mapping
            std::cout << "Neighbor sample ids=[ ";
            std::for_each( _parent_sample_id_it->second.begin(),
                           _parent_sample_id_it->second.end(),
                           [] ( const formation_formation_sample_id_type sid ) { std::cout << sid << " "; } );
            std::cout << "] nearest_pid=" << _nearest_pid << std::endl;

            player_iterator_type _player_begin, _player_end;
            boost::tie( _player_begin, _player_end ) = get_players( _sample );


            soccer::modeling::player_matching_cost< FormationSampleStorage >
                    _player_matching_cost_fn( _sample,
                                              *M_storage,
                                              _parent_sample_id_it->second.begin(),
                                              _parent_sample_id_it->second.end() );

            typedef boost::filter_iterator< std::function< bool(const player_id_descriptor_type) >, player_iterator_type > player_filter_iterator_type;
            auto _not_is_bowner_pid = [&] ( const player_id_descriptor_type pid ) { return pid != M_bowner_id; };
            player_id_map_type _player_id_map;
            _player_id_map.insert( std::make_pair( M_bowner_id, M_bowner_id ) );
            boost::associative_property_map< player_id_map_type > _player_id_pmap( _player_id_map );

            auto _cost = soccer::modeling::formation_sample_matching( player_filter_iterator_type( _not_is_bowner_pid, _player_begin, _player_end ),
                                                                      player_filter_iterator_type( _not_is_bowner_pid, _player_end, _player_end ),
                                                                      _player_matching_cost_fn,
                                                                      _player_id_pmap );
            apply_matchings( _sample, _player_id_pmap );

            std::cout << "ball=(" << soccer::get_x( _ball_position ) << "," << soccer::get_y( _ball_position ) << ")";
            std::cout << " nearest_pid=" << _nearest_pid;
            std::cout << " Matchings(cost=" << _cost << ")=";
            std::for_each( _player_id_map.begin(),
                           _player_id_map.end(),
                           [] ( const typename player_id_map_type::value_type kvp1 )
                                {
                                std::cout << "(" << kvp1.first << "," << kvp1.second << ")";
                                }
                          );

            // soccer::modeling::remove_player_crossings_in_matching( _player_id_map )
            std::cout << std::endl;

        }
    }

    template < typename Edge, typename Graph >
    void examine_edge(Edge e, const Graph & g)
    {
        std::clog << " Examine edge=(" << boost::source( e, g ) << "," <<
                     boost::target( e, g ) << ") ";
        auto itfns = M_predecessor_map.find( boost::target( e, g ) );
        if ( itfns != M_predecessor_map.end() )
        {
            std::clog << "[Adding a neighbor sample_id=" << boost::source( e, g ) << " for sample_id=" << boost::target( e, g ) << "]";
            itfns->second.insert( boost::source( e, g ) );
        }
    }

    template < typename Vertex, typename Graph >
    void finish_vertex(Vertex v, const Graph & g)
    {
        std::clog << std::endl << "Finished sample " << v << std::endl;
        M_predecessor_map.erase(v);
    }
};


namespace detail {
    BOOST_MPL_HAS_XXX_TRAIT_DEF(Triangulation_data_structure);
}

/*!
 * \brief The make_offensive_sample_impl struct
 * \note to circumvent partial specialization issues
 */
template < typename Triangulation >
struct make_offensive_sample_from_triangulation_impl
{
    typedef typename soccer::formation_sample_id_type< Triangulation >::type formation_formation_sample_id_type;
    typedef typename soccer::formation_sample_type< Triangulation >::type formation_sample_type;
    typedef typename soccer::player_id_type< formation_sample_type >::type player_id_descriptor_type;

    /* static void apply( Triangulation& storage,
                       formation_sample_type bowner_id )
    {
        apply( storage, bowner_id );
    }*/


    static void apply( Triangulation& storage/* typename ::soccer::CGAL::triangulation_formation_sample_storage_generator< InfoType, CoordinateType >::type& storage*/,
                       player_id_descriptor_type bowner_id )
    {
        // TODO Make sure that vertex has info and that info is a sample!!!

        auto _dt_graph = soccer::CGAL::make_finite_delaunay_triangulation_graph( storage );
        const auto _start_vertex = *( boost::vertices( _dt_graph ).first );
        /* typedef std::map< player_id_descriptor_type, player_id_descriptor_type > player_id_map;
        typedef std::map< formation_formation_sample_id_type, player_id_map > sample_player_id_map;
        sample_player_id_map _sample_player_id_map;
        boost::associative_property_map< sample_player_id_map > _sample_player_id_pmap( _sample_player_id_map ); */
        boost::breadth_first_search( _dt_graph,
                                     _start_vertex,
                                     boost::visitor( offensive_sample_generator< Triangulation >( storage, bowner_id )) );
    }

};


template < typename FormationSampleStorage >
struct make_offensive_sample_impl
{
    typedef typename soccer::formation_sample_id_type< FormationSampleStorage >::type formation_sample_id_t;
    typedef typename soccer::formation_sample_iterator_type< FormationSampleStorage >::type formation_sample_iterator_t;
    typedef typename soccer::formation_sample_type< FormationSampleStorage >::type formation_sample_t;
    typedef typename soccer::formation_sample_proxy< formation_sample_t > formation_sample_proxy_t;
    typedef typename soccer::player_id_type< formation_sample_t >::type player_id_t;

    typedef typename soccer::CGAL::triangulation_formation_sample_storage_generator< formation_sample_t >::type delaunay_triangulation_2_type;
    typedef std::map< typename delaunay_triangulation_2_type::Vertex_handle,
                      formation_sample_id_t > dt_vertex_to_sample_id_map_type;


    static void
    apply( FormationSampleStorage& storage,
           const player_id_t bowner_id )
    {

        typedef soccer::always_add_formation_sample_storage_policy< formation_sample_proxy_t > storage_policy_type;

        auto _triangulation_storage = soccer::CGAL::make_triangulation_formation_sample_storage< formation_sample_proxy_t >();

        storage_policy_type _storage_policy( _triangulation_storage );

        auto _formation_sample_copy_fn = [&] ( const formation_sample_id_t sid )
            {
                formation_sample_proxy_t _proxy_sample( soccer::get_sample( sid, storage ) );
                _storage_policy( _proxy_sample );
            };

        formation_sample_iterator_t _sample_begin, _sample_end;
        boost::tie( _sample_begin, _sample_end ) = soccer::get_samples( storage );
        std::copy( _sample_begin,
                   _sample_end,
                   boost::make_function_output_iterator( _formation_sample_copy_fn )
                 );

        assert( soccer::num_samples( _triangulation_storage ) == soccer::num_samples( storage ) );

        // Create triangulation
        make_offensive_sample_from_triangulation_impl< decltype(_triangulation_storage) >::apply( _triangulation_storage, bowner_id );

        // Copy result back? Why not take a non-const reference to info and modify in-place!!!!
    }
};

template < typename FormationSampleStorage >
void
make_offensive_sample( FormationSampleStorage& storage,
                       typename soccer::player_id_type<
                                typename soccer::formation_sample_type< FormationSampleStorage >::type
                            >::type bowner_id )
{
    // BOOST_CONCEPT_ASSERT(( FormationSampleStorageConcept< FormationSampleStorage > ));

    typedef typename boost::mpl::if_< detail::has_Triangulation_data_structure<FormationSampleStorage>,
                                        make_offensive_sample_from_triangulation_impl< FormationSampleStorage >,
                                        make_offensive_sample_impl< FormationSampleStorage >
                                    >::type impl_type;

    impl_type::apply( storage, bowner_id );
}



//
// Storage modifiers
//
/*!
* \brief Formation storage modifier to enhance existing samples
*/
template < typename Kernel,
           typename TriangulationDataStructure,
           typename FormationSampleIdScoreEvaluator >
struct formation_sample_central_vertices
    : public std::unary_function< void, ::CGAL::Triangulation_2< Kernel, TriangulationDataStructure > > {

    typedef ::CGAL::Triangulation_2< Kernel, TriangulationDataStructure > triangulation_2_type;
    typedef typename soccer::formation_sample_iterator_type< triangulation_2_type >::type formation_sample_iterator_type;
    typedef typename soccer::formation_sample_id_type< triangulation_2_type >::type formation_sample_id_type;

    typedef soccer::CGAL::finite_vertices_circulator< triangulation_2_type > finite_vertices_circulator_type;

    FormationSampleIdScoreEvaluator M_score_evaluator;
    formation_sample_central_vertices( FormationSampleIdScoreEvaluator score_evaluator )
        : M_score_evaluator( score_evaluator )
    {

    }

   void operator()( triangulation_2_type& storage )
   {
       std::set< formation_sample_id_type > _visited_convex_hull_vertices;
       auto _is_infinite = [&] ( typename triangulation_2_type::Vertex_handle vh )
                            { return storage.is_infinite( vh ) || _visited_convex_hull_vertices.find( vh ); };
        auto _is_infinite_or_visited = [&] ( typename triangulation_2_type::Vertex_handle vh )
                                { return _is_infinite( vh ) || _visited_convex_hull_vertices.find( vh ) != _visited_convex_hull_vertices.end(); };
       auto itvch = finite_vertices_circulator_type( _is_infinite_or_visited,
                                                     storage.incident_vertices( storage.infinite_vertex() ) );
       auto end_itvch = itvch;
       do {
            auto _convex_hull_vertex = *itvch;
            assert( storage.is_infinite( _convex_hull_vertex ));

            // Do operations on convex hull vertex
            auto itv = finite_vertices_circulator_type( _is_infinite,
                                                        storage.incident_vertices( _convex_hull_vertex ) );
            auto end_itv = itv;
            double _convex_hull_vertex_score = M_score_evaluator( _convex_hull_vertex );
            bool _best_scores_found  = true;
            do {
                auto _convex_hull_incident_vertex_score = M_score_evaluator( *itv );
                if ( _convex_hull_incident_vertex_score < _convex_hull_vertex_score )
                {
                    _best_scores_found = false;
                    break;
                }
            } while( itv != end_itv );

            if ( _best_scores_found )
            {
                // TODO Extrapolate new sample from neighbor samples
            }

            itvch++;
        } while ( itvch != end_itvch );

   }
};




template < typename FormationSampleStorage >
struct formation_sample_storage_bfs
    : public boost::default_bfs_visitor
{
    // BOOST_CONCEPT_ASSERT(( FormationSampleStorageConcept< FormationSampleStorage > ));
    typedef typename soccer::formation_sample_id_type< FormationSampleStorage >::type formation_formation_sample_id_t;
    typedef typename soccer::formation_sample_type< FormationSampleStorage >::type formation_sample_t;
    typedef typename soccer::player_id_type< formation_sample_t >::type player_id_descriptor_t;
    typedef typename soccer::player_iterator_type< formation_sample_t >::type player_iterator_t;

    typedef std::map< player_id_descriptor_t, player_id_descriptor_t > player_id_map_type;

    FormationSampleStorage* M_storage;
    player_id_descriptor_t M_bowner_id;


    std::map< formation_formation_sample_id_t, std::set< formation_formation_sample_id_t > > M_predecessor_map;



    formation_sample_storage_bfs( FormationSampleStorage& storage,
                                  const player_id_descriptor_t bowner_id )
        : M_storage( &storage )
        , M_bowner_id( bowner_id )
    {

    }

    template < typename Vertex, typename Graph >
    void discover_vertex(Vertex , const Graph & ) const
    {

    }

    template < typename Edge, typename Graph >
    void tree_edge(Edge e, const Graph & g)
    {
        std::clog << "Tree edge=(" << boost::source( e, g ) << "," <<
                     boost::target( e, g ) << ") ";
        // save predecessor
        std::set< formation_formation_sample_id_t > _neighbor_samples;
        _neighbor_samples.insert( boost::source( e, g ) );
        M_predecessor_map[ boost::target( e, g ) ] = _neighbor_samples;
    }

    template < typename Vertex, typename Graph >
    void examine_vertex(Vertex u, const Graph & g)
    {
        const auto _sample_id = u;
        std::clog << std::endl << "Examining sample " << _sample_id << std::endl;

        auto& _sample = get_sample( _sample_id, *M_storage );
        auto _ball_position = soccer::get_position( soccer::get_ball( _sample ) );
        auto _nearest_pid = nearest_player_id( _ball_position, _sample );
        auto _nearest_player = get_player( _nearest_pid, _sample );

        // TODO Decide which player id position should be discarded
        // auto _pid_to_discard = M_player_id_discarder( _sample );

        // set nearest player id position to the ball position
        soccer::set_position( _ball_position, _nearest_player );

        // auto _tmp_player = soccer::get_player( M_bowner_id, _sample );
        // soccer::set_player( M_bowner_id, _nearest_player, _sample );
        soccer::set_player( _nearest_pid, _nearest_player, _sample );
        if ( _nearest_pid != M_bowner_id )
        {
            std::cout << "Nearest_pid=" << _nearest_pid << " <> bowner_id="  << M_bowner_id << " => SWAP!!" << std::endl;
            swap_players( _nearest_pid, M_bowner_id, _sample );
        }

        auto _parent_sample_id_it = M_predecessor_map.find( u );
        if ( _parent_sample_id_it != M_predecessor_map.end() )
        {
            // Make additional swap in mapping
            std::cout << "Neighbor sample ids=[ ";
            std::for_each( _parent_sample_id_it->second.begin(),
                           _parent_sample_id_it->second.end(),
                           [] ( const formation_formation_sample_id_t sid ) { std::cout << sid << " "; } );
            std::cout << "] nearest_pid=" << _nearest_pid << std::endl;

            player_iterator_t _player_begin, _player_end;
            boost::tie( _player_begin, _player_end ) = get_players( _sample );


            soccer::modeling::player_matching_cost< FormationSampleStorage >
                    _player_matching_cost_fn( _sample,
                                              *M_storage,
                                              _parent_sample_id_it->second.begin(),
                                              _parent_sample_id_it->second.end() );

            typedef boost::filter_iterator< std::function< bool(const player_id_descriptor_t) >, player_iterator_t > player_filter_iterator_type;
            auto _not_is_bowner_pid = [&] ( const player_id_descriptor_t pid ) { return pid != M_bowner_id; };
            player_id_map_type _player_id_map;
            _player_id_map.insert( std::make_pair( M_bowner_id, M_bowner_id ) );
            boost::associative_property_map< player_id_map_type > _player_id_pmap( _player_id_map );

            auto _cost = soccer::modeling::formation_sample_matching( player_filter_iterator_type( _not_is_bowner_pid, _player_begin, _player_end ),
                                                                      player_filter_iterator_type( _not_is_bowner_pid, _player_end, _player_end ),
                                                                      _player_matching_cost_fn,
                                                                      _player_id_pmap );
            apply_matchings( _sample, _player_id_pmap );

            std::cout << "ball=(" << soccer::get_x( _ball_position ) << "," << soccer::get_y( _ball_position ) << ")";
            std::cout << " nearest_pid=" << _nearest_pid;
            std::cout << " Matchings(cost=" << _cost << ")=";
            std::for_each( _player_id_map.begin(),
                           _player_id_map.end(),
                           [] ( const typename player_id_map_type::value_type kvp1 )
                                {
                                std::cout << "(" << kvp1.first << "," << kvp1.second << ")";
                                }
                          );

            // soccer::modeling::remove_player_crossings_in_matching( _player_id_map )
            std::cout << std::endl;

        }
    }

    template < typename Edge, typename Graph >
    void examine_edge(Edge e, const Graph & g)
    {
        std::clog << " Examine edge=(" << boost::source( e, g ) << "," <<
                     boost::target( e, g ) << ") ";
        auto itfns = M_predecessor_map.find( boost::target( e, g ) );
        if ( itfns != M_predecessor_map.end() )
        {
            std::clog << "[Adding a neighbor sample_id=" << boost::source( e, g ) << " for sample_id=" << boost::target( e, g ) << "]";
            itfns->second.insert( boost::source( e, g ) );
        }
    }

    template < typename Vertex, typename Graph >
    void finish_vertex(Vertex v, const Graph & g)
    {
        std::clog << std::endl << "Finished sample " << v << std::endl;
        M_predecessor_map.erase(v);
    }
};


// }
#endif // SOCCER_FORMATION_SAMPLE_STORAGE_TRANSFORM_HPP
