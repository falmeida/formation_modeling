#ifndef SOCCER_BOOST_GEOMETRY_GEOMETRIES_ADAPTED_STD_PAIR_AS_POINT_HPP
#define SOCCER_BOOST_GEOMETRY_GEOMETRIES_ADAPTED_STD_PAIR_AS_POINT_HPP

#include <cstddef>

#include <boost/geometry.hpp>

#include <utility>
#include <tuple>

namespace boost { namespace geometry { namespace traits {

    // std::pair< T, T> point adaptor
    template < typename CoordinateType >
    struct tag< std::pair< CoordinateType, CoordinateType > > { typedef point_tag type; };

    template < typename CoordinateType >
    struct dimension< std::pair< CoordinateType, CoordinateType > > : boost::mpl::int_<2> {};

    template < typename CoordinateType >
    struct coordinate_type< std::pair< CoordinateType, CoordinateType > > { typedef CoordinateType type; };

    // TODO What if another kind of coordinate system is required
    template < typename CoordinateType >
    struct coordinate_system< std::pair< CoordinateType, CoordinateType > > { typedef boost::geometry::cs::cartesian type; };


    template <typename CoordinateType>
    struct access<std::pair< CoordinateType, CoordinateType >, 0 >
    {
        static inline CoordinateType get(std::pair< CoordinateType, CoordinateType> const& point)
        {
            return point.first;
        }
        static inline void set(std::pair< CoordinateType, CoordinateType>& point,
                               CoordinateType const& value)
        {
            point.first = value;
        }
    };

    template <typename CoordinateType>
    struct access<std::pair< CoordinateType, CoordinateType >, 1>
    {
        static inline CoordinateType get(std::pair< CoordinateType, CoordinateType> const& point)
        {
            return point.second;
        }
        static inline void set( std::pair< CoordinateType, CoordinateType>& point,
                                CoordinateType const& value)
        {
            point.second = value;
        }
    };

}}}

#endif // SOCCER_BOOST_GEOMETRY_GEOMETRIES_ADAPTED_STD_PAIR_AS_POINT_HPP
