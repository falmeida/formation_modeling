#ifndef RCSSLOGPLAYER_DISP_INFO_H
#define RCSSLOGPLAYER_DISP_INFO_H

#include "rcsslogplayer_soccer_model.h"

#include <rcsslogplayer/types.h>

#include <soccer/state_traits.hpp>

//! Soccer types registration
#include <soccer/register/state.hpp>
#include <soccer/register/ball.hpp>
#include <soccer/register/player.hpp>

#include <soccer/core/access.hpp>


#include <boost/iterator/counting_iterator.hpp>

#include <map>
#include <algorithm>
#include <functional>
#include <iterator>
#include <cassert>

namespace rcss {
namespace rcg {    

    typedef std::pair< float, float > position_type;
    typedef std::pair< float, float > velocity_type;
    typedef boost::counting_iterator< unsigned short > player_iterator_type;

    //
    // BallT Free Functions
    //
    std::pair< float, float >
    get_position ( const rcss::rcg::BallT& ball ) { return std::make_pair( ball.x_, ball.y_ ); }

    std::pair< float, float >
    get_velocity( const rcss::rcg::BallT& ball ) { return std::make_pair( ball.vx_, ball.vy_ ); }

    void
    set_position ( const std::pair< float, float >& pos, rcss::rcg::BallT& ball ) { ball.x_ = pos.first; ball.y_ = pos.second; }

    void
    set_velocity( const std::pair< float, float >& vel, rcss::rcg::BallT& ball ) { ball.vx_ = vel.first; ball.vy_ = vel.second; }

    //
    // PlayerT Free Functions
    //
    std::pair< float, float >
    get_position ( const rcss::rcg::PlayerT& player ) { return std::make_pair( player.x_, player.y_ ); }

    std::pair< float, float >
    get_velocity( const rcss::rcg::PlayerT& player ) { return std::make_pair( player.vx_, player.vy_ ); }

    rcss::rcg::Side
    get_side( const rcss::rcg::PlayerT& player ) { return player.side(); }

    Int16
    get_number( const rcss::rcg::PlayerT& player ) { return player.unum_; }

    void
    set_position ( const std::pair< float, float >& pos, rcss::rcg::PlayerT& player ) { player.x_ = pos.first; player.y_ = pos.second; }

    void
    set_velocity( const std::pair< float, float >& vel, rcss::rcg::PlayerT& player ) { player.vx_ = vel.first; player.vy_ = vel.second; }

    void
    set_side( const rcss::rcg::Side& side, rcss::rcg::PlayerT& player ) { assert( side != NEUTRAL ); player.side_ = side == LEFT ? 'l' : 'r'; }

    void
    set_number( const Int16& number, rcss::rcg::PlayerT& player ) { player.unum_ = number; }

    //
    // DispInfoT
    //
    BallT get_ball( const rcss::rcg::DispInfoT& state ) { return state.show_.ball_; }
    void set_ball( const BallT& ball, rcss::rcg::DispInfoT& state ) { state.show_.ball_ = ball; }

    std::pair< player_iterator_type, player_iterator_type >
    get_players( const rcss::rcg::DispInfoT&  )
    {
        return std::make_pair( player_iterator_type(0), player_iterator_type( 22 ) );
    }

    PlayerT get_player( const int pid, const rcss::rcg::DispInfoT& state )
    {
        assert( pid >= 0 && pid < 11 * 2 );

        return state.show_.player_[pid];
    }
    void set_player( const int pid,
                     const PlayerT& player,
                     rcss::rcg::DispInfoT& state )
    {
        assert( pid >= 0 && pid < 11 * 2 );

        state.show_.player_[pid] = player;
    }

    Int32 get_time( const DispInfoT& state ) { return state.show_.time_; }
    void set_time( const Int32 time, DispInfoT& state ) { state.show_.time_ = time; }

    PlayMode get_playmode( const DispInfoT& state ) { return state.pmode_; }
    void set_playmode( const PlayMode pmode, DispInfoT& state ) { state.pmode_ = pmode; }
}
}

// Register state traits of rcss::rcg::DispInfoT
/* namespace soccer { namespace traits {
SOCCER_DETAIL_SPECIALIZE_STATE_TRAITS( rcss::rcg::DispInfoT, \
                                       rcss::rcg::BallT, \
                                       std::size_t, \
                                       rcss::rcg::PlayerT, \
                                       rcss::rcg::ShowInfoTPlayerIterator )
}}*/
SOCCER_REGISTER_DYNAMIC_BALL_GET_SET_FREE(rcss::rcg::BallT, \
                                          rcss::rcg::position_type, rcss::rcg::get_position, rcss::rcg::set_position, \
                                          rcss::rcg::velocity_type, rcss::rcg::get_velocity, rcss::rcg::set_velocity)

SOCCER_REGISTER_DYNAMIC_PLAYER_GET_SET_FREE(rcss::rcg::PlayerT, \
                                            rcss::rcg::position_type, rcss::rcg::get_position, rcss::rcg::set_position, \
                                            rcss::rcg::velocity_type, rcss::rcg::get_velocity, rcss::rcg::set_velocity, \
                                            rcss::rcg::Side, rcss::rcg::get_side, rcss::rcg::set_side, \
                                            rcss::rcg::Int16, rcss::rcg::get_number, rcss::rcg::set_number )

SOCCER_REGISTER_STATE_GET_SET_FREE(rcss::rcg::DispInfoT, \
                                   rcss::rcg::BallT, rcss::rcg::get_ball, rcss::rcg::set_ball, \
                                   int, rcss::rcg::player_iterator_type, rcss::rcg::get_players, \
                                   rcss::rcg::PlayerT, rcss::rcg::get_player, rcss::rcg::set_player, \
                                   rcss::rcg::UInt32, rcss::rcg::get_time, rcss::rcg::set_time, \
                                   rcss::rcg::PlayMode, rcss::rcg::get_playmode, rcss::rcg::set_playmode )

namespace soccer {

bool is_game_stopped( const rcss::rcg::DispInfoT& state );

bool is_setplay_mode( const rcss::rcg::DispInfoT& state );

const rcss::rcg::TeamT& get_left_team( const rcss::rcg::DispInfoT& state );
const rcss::rcg::TeamT& get_right_team( const rcss::rcg::DispInfoT& state );

void set_left_team( const rcss::rcg::TeamT& team, rcss::rcg::DispInfoT& state );
void set_right_team( const rcss::rcg::TeamT& team, rcss::rcg::DispInfoT& state );

rcss::rcg::Side opposite_side( const rcss::rcg::Side side );

/*
/// DispInfoT (aka State)
const rcss::rcg::BallT& get_ball( const rcss::rcg::DispInfoT& state );


std::pair< rcss::rcg::player_iterator_type, rcss::rcg::player_iterator_type>
get_players( const rcss::rcg::DispInfoT& state );


const rcss::rcg::PlayerT& get_player( const size_t player_id, const rcss::rcg::DispInfoT& state );


int get_time( const rcss::rcg::DispInfoT& state );

void set_ball( rcss::rcg::BallT ball, rcss::rcg::DispInfoT& state );

void set_player( int pid, rcss::rcg::PlayerT player, rcss::rcg::DispInfoT& state );


/// BallT
rcsc::Vector2D get_velocity( const rcss::rcg::BallT& ball );
float get_vx( const rcss::rcg::BallT& ball );
float get_vy( const rcss::rcg::BallT& ball );

rcsc::Vector2D get_position( const rcss::rcg::BallT& ball );
float get_x( const rcss::rcg::BallT& ball );
float get_y( const rcss::rcg::BallT& ball );

double get_speed( const rcss::rcg::BallT& ball );

void set_position( rcss::rcg::BallT& ball, const rcsc::Vector2D& pos);
void set_x( const float x, rcss::rcg::BallT& ball );
void set_y( const float y, rcss::rcg::BallT& ball );

void set_velocity( rcss::rcg::BallT& ball, const rcsc::Vector2D& vel);
void set_vx( const float x, rcss::rcg::BallT& ball );
void set_vy( const float y, rcss::rcg::BallT& ball );

/// PlayerT
int get_number( const rcss::rcg::PlayerT& player );

rcss::rcg::Side
get_side( const rcss::rcg::PlayerT& player );

rcsc::Vector2D get_velocity( const rcss::rcg::PlayerT& player );
float get_vx( const rcss::rcg::PlayerT& player );
float get_vy( const rcss::rcg::PlayerT& player );

rcsc::Vector2D get_position( const rcss::rcg::PlayerT& player );
float get_x( const rcss::rcg::PlayerT& player );
float get_y( const rcss::rcg::PlayerT& player );

double get_speed( const rcss::rcg::PlayerT& player );

void set_position( rcss::rcg::PlayerT& player, const rcsc::Vector2D& pos);
void set_x( const float x, rcss::rcg::PlayerT& player );
void set_y( const float y, rcss::rcg::PlayerT& player );

void set_velocity( rcss::rcg::PlayerT& player, const rcsc::Vector2D& vel );
void set_vx( const float x, rcss::rcg::PlayerT& player );
void set_vy( const float y, rcss::rcg::PlayerT& player );
 */


/// State Modifiers (transformations)
template <typename SoccerModel>
struct vertical_symmetry_state {
    SoccerModel const* M_soccer_model;
    vertical_symmetry_state( const SoccerModel& soccer_model )
        : M_soccer_model( &soccer_model )
    {

    }

    template <typename State>
    bool operator()( State& state )
    {
        // BOOST_CONCEPT_ASSERT(( StaticStateConcept<State> ));
        // HACK Non-generic access to position coordinate
        auto _new_ball = get_ball( state );
        auto _new_ball_pos = get_position( _new_ball );
        const auto _ball_y = get_y( _new_ball_pos );
        if ( soccer::model::in_upper_field( _ball_y, *M_soccer_model ) )
        {
            const auto _ball_offset_y = fabs( _ball_y - soccer::model::pitch_top_left_y( *M_soccer_model ) );
            const auto _new_ball_y = soccer::model::pitch_top_left_y( *M_soccer_model ) +
                                     soccer::model::pitch_width( *M_soccer_model ) -
                                     _ball_offset_y;
            set_y( _new_ball_y, _new_ball_pos );
        }
        else
        {
            const auto _ball_offset_y = fabs( soccer::model::pitch_top_left_y( *M_soccer_model ) +
                                              soccer::model::pitch_width( *M_soccer_model) -
                                              _ball_y );
            const auto _new_ball_y = soccer::model::pitch_top_left_y( *M_soccer_model ) +
                                     _ball_offset_y;
            set_y( _new_ball_y, _new_ball_pos );
        }

        set_position( _new_ball_pos, _new_ball );
        // Invert vertical velocity
        auto _new_ball_vel = get_velocity( _new_ball );
        set_y( -get_y( _new_ball_vel ), _new_ball_vel );
        soccer::set_velocity( _new_ball_vel, _new_ball );

        soccer::set_ball( _new_ball, state );

        // Invert players y-coordinates
        typename player_iterator_type< State >::type _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( state );
        for ( auto it = _player_begin; it != _player_end ; it++ )
        {
            auto _player = get_player( *it, state );
            if ( soccer::model::in_upper_field( _player.y_, *M_soccer_model ) )
            {
                const auto _player_pos = get_position( _player );
                const auto _player_y = get_y( _player_pos );
                const auto _player_offset_y = fabs( _player_y - soccer::model::pitch_top_left_y( *M_soccer_model ) );
                const auto _new_player_y = soccer::model::pitch_top_left_y( *M_soccer_model ) +
                                           soccer::model::pitch_width( *M_soccer_model ) -
                                           _player_offset_y;
                set_y( _new_player_y, _player );
            }
            else
            {
                const auto _player_offset_y = fabs( soccer::model::pitch_top_left_y( *M_soccer_model ) +
                                                    soccer::model::pitch_width( *M_soccer_model) -
                                                    _player.y_ );
                const auto _new_player_y = soccer::model::pitch_top_left_y( *M_soccer_model ) +
                                           _player_offset_y;
                set_y( _new_player_y, _player );
            }

            auto _player_vel = get_velocity( _player );
            const auto _player_vy = get_y( _player_vel );
            // Invert vertical velocity
            set_y( -_player_vy, _player_vel );
            set_velocity( _player, _player_vel );

            set_player( *it, _player, state );
        }
    }
};


template <typename SoccerModel>
struct exchange_team_sides_state {
    SoccerModel const* M_soccer_model;
    exchange_team_sides_state( const SoccerModel& soccer_model )
        : M_soccer_model( &soccer_model )
    {

    }

    bool operator()( rcss::rcg::DispInfoT& state )
    {
        // BOOST_CONCEPT_ASSERT(( StaticStateConcept<State> ));
        // HACK Non-generic access to position coordinate
        // HACK Non-generic (using robocup 2d soccer model)
        state.show_.ball_.x_ = -state.show_.ball_.x_;
        state.show_.ball_.vx_ = -state.show_.ball_.vx_;

        auto tmp_team = state.team_[0];
        state.team_[0] = state.team_[1];
        state.team_[1] = tmp_team;

        // Convert right team in left team
        for ( auto i = 0u; i < rcss::rcg::MAX_PLAYER; i++ )
        {
            const auto _left_pid = i;
            const auto _right_pid = i + rcss::rcg::MAX_PLAYER;

            const auto tmp = state.show_.player_[_left_pid];

            state.show_.player_[_left_pid] = state.show_.player_[_right_pid];
            state.show_.player_[_left_pid].x_ = -state.show_.player_[_left_pid].x_;
            state.show_.player_[_left_pid].vx_ = -state.show_.player_[_left_pid].vx_;
            state.show_.player_[_left_pid].side_ = 'l';

            state.show_.player_[_right_pid] = tmp;
            state.show_.player_[_right_pid].x_ = -tmp.x_;
            state.show_.player_[_right_pid].vx_ = -tmp.vx_;
            state.show_.player_[_right_pid].side_ = 'r';
        }
    }

    template <typename State>
    bool operator()( State& state )
    {
        auto _new_ball = soccer::get_ball( state );
        auto _new_ball_pos = get_position( _new_ball );
        const auto _ball_x = get_x( _new_ball_pos );
        if ( soccer::model::in_left_midfield( _ball_x, *M_soccer_model ) )
        {
            const auto _ball_offset_x = fabs( _ball_x - soccer::model::pitch_top_left_x( *M_soccer_model ) );
            const auto _new_ball_x = soccer::model::pitch_top_left_x( *M_soccer_model ) +
                                     soccer::model::pitch_width( *M_soccer_model ) -
                                     _ball_offset_x;
            set_x( _new_ball_x, _new_ball_pos );
        }
        else
        {
            const auto _ball_offset_x = fabs( soccer::model::pitch_top_left_x( *M_soccer_model ) +
                                              soccer::model::pitch_length( *M_soccer_model) -
                                              _ball_x );
            const auto _new_ball_x = soccer::model::pitch_top_left_x( *M_soccer_model ) +
                          _ball_offset_x;
            set_x( _new_ball_x, _new_ball_pos );
        }
        set_position( _new_ball_pos, _new_ball );
        // Invert ball velocity
        const auto _new_ball_vel = get_velocity( _new_ball );
        set_x( get_x( _new_ball_vel ), _new_ball_vel );
        set_velocity( _new_ball_vel, _new_ball );
        set_ball( _new_ball, state );

        // Switch teams
        auto tmp_team = get_left_team( state );
        set_left_team( get_right_team( state ), state );
        set_right_team( tmp_team, state );

        // Convert right team in left team
        typename player_iterator_type< State >::type _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( state );
        for ( auto itp = _player_begin; itp != _player_end; _player_end++ )
        {
            // HACK non generic
            auto _new_pid = (*itp + 11) % (11 * 2);
            auto _new_player_side = *itp < 11  ? 'l' : 'r';
            auto _new_player = get_player( _new_pid, state );
            auto _new_player_pos = get_position( _new_player );
            auto _new_player_vel = get_velocity( _new_player );

            // TODO Non-generic => Should user field model info
            set_x( -get_x( _new_player_pos ), _new_player_pos );
            set_x( -get_x( _new_player_vel ), _new_player_vel );
            set_position( _new_player_pos, _new_player );
            set_velocity( _new_player_vel, _new_player );

            set_side( _new_player_side, _new_player );
            set_player( *itp, _new_player, state );
        }
    }
};

} // end namespace soccer


#endif // RCSSLOGPLAYER_DISP_INFO_H
