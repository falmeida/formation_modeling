#ifndef FORMATION_SAMPLE_STORAGE_DEBUG_HPP
#define FORMATION_SAMPLE_STORAGE_DEBUG_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "CGAL/cgal_triangulation_formation_sample_storage.hpp"

// CGAL Triangulations as models of the boost graph concept
#include <CGAL/boost/graph/graph_traits_Delaunay_triangulation_2.h>

#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graphviz.hpp>

#include <boost/tuple/tuple.hpp>

// debug collected sample data information
template < typename Kernel,
           typename TriangulationDataStructure >
void debug( const CGAL::Delaunay_triangulation_2< Kernel, TriangulationDataStructure >& _triangulation )
{
    typedef CGAL::Delaunay_triangulation_2< Kernel, TriangulationDataStructure > triangulation_2_type;
    typedef typename soccer::CGAL::Is_finite< triangulation_2_type > FiniteFilter;
    typedef typename boost::filtered_graph< triangulation_2_type, FiniteFilter, FiniteFilter > Finite_triangulation;
    typedef typename boost::graph_traits< Finite_triangulation >::vertex_descriptor vertex_descriptor;
    typedef typename boost::graph_traits< Finite_triangulation >::vertex_iterator vertex_iterator;
    typedef typename boost::graph_traits< Finite_triangulation >::edge_descriptor edge_descriptor;
    typedef typename boost::graph_traits < Finite_triangulation >::vertices_size_type vertices_size_type;

    // The BGL makes use of indices associated to the vertices
    // We use a std::map to store the index
    typedef std::map<vertex_descriptor, int > VertexIndexMap;
    // A std::map is not a property map, because it is not lightweight
    typedef boost::associative_property_map<VertexIndexMap> VertexIdPropertyMap;

    FiniteFilter is_finite( _triangulation );
    Finite_triangulation _finite_triangulation( _triangulation, is_finite, is_finite );

    VertexIndexMap vertex_id_map;
    vertex_iterator first_vertex, vit, ve;
    // Associate indices to the vertices
    int index = 0;
    // boost::tie assigns the first and second element of the std::pair
    // returned by boost::vertices to the variables vit and ve
    boost::tie(vit,ve) = boost::vertices(_finite_triangulation); first_vertex = vit;
    for( ; vit!=ve; ++vit, index++ )
    {
        vertex_descriptor vd = *vit;
        vertex_id_map[vd]= index;
        vd->id()= index;
    }

    // Write formation modeling in GraphViz format
    // Each vertex is a state (collected from one game log or merged from N game logs)
    // Each edge is an association between 2 different states
    auto _vertex_property_writer
            =  [&] ( std::ostream& out, vertex_descriptor vd )
                {
                    const auto _sample = soccer::get_info( vd->info() );
                    const auto& state = soccer::modeling::get_state( _sample );
                    // TODO Should also output a concise game log id if multiple times are used
                    // TODO Use a color map for each game log to fill the vertex background color
                    out << "[width=.5, height=.5, label=\"("
                        << vd->id() + 1
                        << ","
                        << soccer::get_time( state ) << ")\"";
                    /*out << ",";
                    out << "pos=\""
                        << (int) soccer::get_x( soccer::get_ball( state ) ) + 52.5 * 10
                        << ","
                        << (int) soccer::get_y( soccer::get_ball( state ) ) + 34 * 10 << "\""; */
                    out << "]";
                };
    auto _edge_property_writer
            = [&] ( std::ostream& out, edge_descriptor ed )
             {
                 out << "[label=\"";
                 // out << weight_map[ed];
                 out << "\"]";
                 // out << "[weight=" << weight_map[ed] << "]"; // TODO use distances
             };
    auto _graph_property_writer
            = [&] ( std::ostream& out )
            {
                // Calculate average score of formation samples
                out << "label=\"Formation Model\"" << std::endl;
                out << "graph [bgcolor=white,layout=sfdp,overlap=scale,splines=line,sep=0.2]" << std::endl;
                out << "node [shape=circle color=black]" << std::endl;
                out << "edge [style=solid]" << std::endl;
            };
    std::stringstream ss_graph_viz_filename;
    ss_graph_viz_filename << "graphs/formation_model.dot";
    std::ofstream ofs( ss_graph_viz_filename.str() );
    boost::write_graphviz( ofs,
                           _finite_triangulation,
                           _vertex_property_writer,
                           _edge_property_writer,
                           _graph_property_writer );
}



template < typename FormationSampleStorage,
           typename FormationSampleEvaluator >
void debug( const FormationSampleStorage& _storage,
            FormationSampleEvaluator _sample_evaluator,
            std::ostream& os = std::cout )
{
    typedef typename soccer::formation_sample_iterator_type< FormationSampleStorage >::type formation_sample_iterator_type;
    formation_sample_iterator_type _sample_begin, _sample_end;
    boost::tie( _sample_begin, _sample_end ) = soccer::get_samples( _storage );
    os << "vid,score" << std::endl;
    os << "---------" << std::endl;
    for ( auto its = _sample_begin; its != _sample_end ; its++ )
    {
        const auto _sample = soccer::get_sample( *its, _storage );
        const auto _score = _sample_evaluator( _sample );
        os << *its << "," << _score << " ";
        os << _sample << std::endl;
    }

}

#endif // FORMATION_SAMPLE_STORAGE_DEBUG_HPP
