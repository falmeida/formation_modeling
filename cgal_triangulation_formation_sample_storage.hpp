#ifndef CGAL_TRIANGULATION_FORMATION_SAMPLE_STORAGE_HPP
#define CGAL_TRIANGULATION_FORMATION_SAMPLE_STORAGE_HPP

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_2.h>
// #include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_vertex_base_with_id_2.h>
// #include <CGAL/Triangulation_face_base_with_info_2.h> //!< not used

#include <CGAL/boost/graph/graph_traits_Delaunay_triangulation_2.h>
#include <boost/graph/filtered_graph.hpp>

#include <boost/iterator/iterator_adaptor.hpp>
#include <boost/iterator/filter_iterator.hpp>

#include <iostream>

namespace custom_cgal {

    template < typename Triangulation >
    struct finite_vertices_circulator
        : public boost::filter_iterator< std::function< bool( typename Triangulation::Vertex_handle ) >,
                                                        typename Triangulation::Vertex_circulator >
    {
        private:
            typedef boost::filter_iterator< std::function< bool( typename Triangulation::Vertex_handle ) >,
                                            typename Triangulation::Vertex_circulator >
                        base_type;
        public:
            finite_vertices_circulator()
                : base_type()
            {

            }

            finite_vertices_circulator( const Triangulation& _triangulation,
                                        typename Triangulation::Vertex_circulator _it )
                : base_type( [&] ( const typename Triangulation::Vertex_handle vh)
                                    { return _triangulation.is_infinite( vh ); }
                                    , _it )
            {

            }

            finite_vertices_circulator( std::function< bool( typename Triangulation::Vertex_handle ) > _pred,
                                        typename Triangulation::Vertex_circulator _it )
                : base_type( _pred, _it )
            {

            }
    };

    template < typename Triangulation >
    struct finite_edges_circulator
        : public boost::filter_iterator< std::function< bool( typename Triangulation::Edge ) >,
                                                        typename Triangulation::Edge_circulator >
    {
        private:
            typedef boost::filter_iterator< std::function< bool( typename Triangulation::Edge ) >,
                                            typename Triangulation::Edge_circulator >
                        base_type;
        public:
            finite_edges_circulator()
                : base_type()
            {

            }

            finite_edges_circulator( const Triangulation& _triangulation,
                                     typename Triangulation::Edge_circulator _it )
                : base_type( [&] ( const typename Triangulation::Edge ed)
                                    { return _triangulation.is_infinite( ed ); }
                                    , _it )
            {

            }

            finite_edges_circulator( std::function< bool( typename Triangulation::Edge ) > _pred,
                                     typename Triangulation::Edge_circulator _it )
                : base_type( _pred, _it )
            {

            }
    };


    template < typename Triangulation >
    class finite_vertices_iterator
      : public boost::iterator_adaptor< finite_vertices_iterator<Triangulation>, // Derived
                                        typename Triangulation::Finite_vertices_iterator, // Base
                                        typename Triangulation::Vertex_handle, // Value
                                        boost::forward_traversal_tag, // Traversal type
                                        typename Triangulation::Vertex_handle, // Reference
                                        boost::use_default > // Difference
    {
        private:
            struct enabler {};

            typedef boost::iterator_adaptor< finite_vertices_iterator<Triangulation>,
                                             typename Triangulation::Finite_vertices_iterator,
                                             typename Triangulation::Vertex_handle,
                                             boost::forward_traversal_tag,
                                             typename Triangulation::Vertex_handle,
                                             boost::use_default > base_type;
        public:
            finite_vertices_iterator()
                : base_type( )
            {

            }

            explicit finite_vertices_iterator(const typename Triangulation::Finite_vertices_iterator p)
                : base_type(p)
            {

            }

        private:
            friend class boost::iterator_core_access;

            typename base_type::reference
            dereference() const
            {
                return this->base();
            }
    };

    template < typename Triangulation >
    struct finite_vertices_iterator_generator {
        typedef finite_vertices_iterator< Triangulation > type;
    };

    // Helper unary predicate (filter) that checks whether a vertex or edge is infinite in a triangulation
    /*!
     * \brief The Is_finite struct
     */
    template <typename Triangulation>
    struct Is_finite {
        const Triangulation* M_triangulation;

        Is_finite()
            : M_triangulation( NULL )
        {

        }

        Is_finite( const Triangulation& _triangulation )
            : M_triangulation( &_triangulation )
        {

        }

        template <typename VertexOrEdge>
        bool operator()(const VertexOrEdge& voe) const
        {
            assert ( M_triangulation != NULL );
            return ! M_triangulation->is_infinite(voe);
        }
    };

    /*!
     * \brief The Triangulation_vertex_base_with_id_and_info_2 class
     */
    template < typename Id_, typename Info_, typename GT,
               typename Vb = CGAL::Triangulation_vertex_base_2<GT> >
    class Triangulation_vertex_base_with_id_and_info_2
      : public Vb
    {
      Info_ _info;
      Id_ _id;

    public:
        typedef typename Vb::Face_handle                   Face_handle;
        typedef typename Vb::Point                         Point;
        typedef Info_                                      Info;

        template < typename TDS2 >
        struct Rebind_TDS {
        typedef typename Vb::template Rebind_TDS<TDS2>::Other          Vb2;
        typedef Triangulation_vertex_base_with_id_and_info_2< Id_, Info, GT, Vb2>   Other;
        };

        Triangulation_vertex_base_with_id_and_info_2()
        : Vb() {}

        Triangulation_vertex_base_with_id_and_info_2(const Point & p)
        : Vb(p) {}

        Triangulation_vertex_base_with_id_and_info_2(const Point & p, Face_handle c)
        : Vb(p, c) {}

        Triangulation_vertex_base_with_id_and_info_2(Face_handle c)
        : Vb(c) {}

        const Info& info() const { return _info; }
        Info&       info()       { return _info; }

        Id_         id() const { return _id; }
        Id_&        id()       { return _id; }
    };

    template < typename Info = void*,
               typename CoordinateDescriptor = float >
    struct delaunay_triangulation_2d_with_info_generator {
        typedef CGAL::Simple_cartesian<CoordinateDescriptor>  kernel;
        typedef Triangulation_vertex_base_with_id_and_info_2< int, Info, kernel > triangulation_vertex_base;
        typedef CGAL::Triangulation_face_base_2< kernel > triangulation_face_base;
        typedef CGAL::Triangulation_data_structure_2< triangulation_vertex_base,
                                                      triangulation_face_base
                                                    > triangulation_data_structure;

        typedef CGAL::Delaunay_triangulation_2< kernel,
                                                triangulation_data_structure
                                              > type;
    };

    template < typename Info = void*,
               typename CoordinateDescriptor = float >
    typename delaunay_triangulation_2d_with_info_generator< Info, CoordinateDescriptor >::type
    make_delaunay_triangulation_2d_with_info() {
        return typename delaunay_triangulation_2d_with_info_generator< Info, CoordinateDescriptor >::type();
    }


    template < typename Kernel,
               typename TriangulationDataStructure >
    struct finite_delaunay_triangulation_graph_generator {
        typedef CGAL::Delaunay_triangulation_2< Kernel, TriangulationDataStructure > delaunay_triangulation_2_type;
        typedef typename boost::filtered_graph< delaunay_triangulation_2_type,
                                                Is_finite< delaunay_triangulation_2_type >,
                                                Is_finite< delaunay_triangulation_2_type > > type;
    };

    template < typename Kernel,
               typename TriangulationDataStructure >
    typename finite_delaunay_triangulation_graph_generator< Kernel, TriangulationDataStructure >::type
    make_finite_delaunay_triangulation_graph( CGAL::Delaunay_triangulation_2< Kernel, TriangulationDataStructure >& triangulation )
    {
        typedef CGAL::Delaunay_triangulation_2< Kernel, TriangulationDataStructure > delaunay_triangulation_2_type;
        typedef typename finite_delaunay_triangulation_graph_generator< Kernel, TriangulationDataStructure >::type finite_delaunay_triangulation_graph_type;
        return finite_delaunay_triangulation_graph_type( triangulation,
                                                         Is_finite< delaunay_triangulation_2_type >( triangulation ),
                                                         Is_finite< delaunay_triangulation_2_type >( triangulation ) );
    }


} // end namespace custom_cgal


namespace soccer {


/*!
 * \brief Get sample stored in vertex
 * \param v The vertex in which the sample is stored
 */
template < typename Id_, typename Info_, typename GT, typename Vb >
Info_&
get_sample( custom_cgal::Triangulation_vertex_base_with_id_and_info_2< Id_, Info_, GT, Vb>& v )
{
    return v.info();
}

/*!
 * \brief Get sample stored in vertex
 * \param v The vertex in which the sample is stored
 */
template < typename Id_, typename Info_, typename GT, typename Vb >
const Info_&
get_sample( const custom_cgal::Triangulation_vertex_base_with_id_and_info_2< Id_, Info_, GT, Vb>& v )
{
    return v.info();
}

//
// Formation sample storage policy implementation for CGAL::Triangulation_2
//
template < typename Kernel,
           typename TriangulationDataStructure >
std::pair<
           custom_cgal::finite_vertices_iterator< typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure > >,
           custom_cgal::finite_vertices_iterator< typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure > >
         >
get_samples( const CGAL::Triangulation_2< Kernel, TriangulationDataStructure >& _triangulation )
{
    typedef typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure > triangulation_2_type;
    typedef typename custom_cgal::finite_vertices_iterator< triangulation_2_type > finite_vertices_iterator_type;
    finite_vertices_iterator_type _begin( _triangulation.finite_vertices_begin() );
    finite_vertices_iterator_type _end( _triangulation.finite_vertices_end() );
    return std::make_pair( _begin, _end );
}

template < typename Kernel,
           typename TriangulationDataStructure >
std::size_t
num_samples( const CGAL::Triangulation_2< Kernel, TriangulationDataStructure >& _triangulation )
{
    return _triangulation.number_of_vertices();
}

template < typename Kernel,
           typename TriangulationDataStructure >
typename TriangulationDataStructure::Vertex::Info&
get_sample( typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure >::Vertex_handle _vh,
            const CGAL::Triangulation_2< Kernel, TriangulationDataStructure >& _triangulation )
{
    typedef typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure > triangulation_2_type;
    auto itf = std::find( custom_cgal::finite_vertices_iterator< triangulation_2_type >( _triangulation.finite_vertices_begin() ),
                          custom_cgal::finite_vertices_iterator< triangulation_2_type >( _triangulation.finite_vertices_end() ),
                          _vh );
    assert( itf != custom_cgal::finite_vertices_iterator< triangulation_2_type >( _triangulation.finite_vertices_end() ) );

    return (*itf)->info();
}

template < typename Kernel,
           typename TriangulationDataStructure >
typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure >::Vertex_handle
add_sample( const typename TriangulationDataStructure::Vertex::Info& _sample,
            CGAL::Triangulation_2< Kernel, TriangulationDataStructure >& _triangulation )
{
    typename Kernel::Point _focal_point( get_x( get_ball( _sample ) ),
                                         get_y( get_ball( _sample ) ));
    auto vh = _triangulation.insert( _focal_point );
    vh->info() = _sample;
    return vh;
}

template < typename Kernel,
           typename TriangulationDataStructure >
typename TriangulationDataStructure::Vertex::Info
remove_sample( typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure >::Vertex_handle _vh,
               CGAL::Triangulation_2< Kernel, TriangulationDataStructure >& _triangulation )
{
    typedef typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure > triangulation_2_type;
    auto itf = std::find( custom_cgal::finite_vertices_iterator< triangulation_2_type >( _triangulation.finite_vertices_begin() ),
                          custom_cgal::finite_vertices_iterator< triangulation_2_type >( _triangulation.finite_vertices_end() ),
                          _vh );
    assert( itf != custom_cgal::finite_vertices_iterator< triangulation_2_type >( _triangulation.finite_vertices_end() ) );

    _triangulation.remove( *itf );

    return (*itf)->info();
}

template < typename Kernel,
           typename TriangulationDataStructure >
typename TriangulationDataStructure::Vertex_handle
nearest_sample_id( typename Kernel::FT x,
                   typename Kernel::FT y,
                   CGAL::Delaunay_triangulation_2< Kernel, TriangulationDataStructure >& _triangulation )
{
    typedef typename CGAL::Triangulation_2< Kernel, TriangulationDataStructure > triangulation_2_type;
    typename Kernel::Point_2 _query_pt( x, y );

    auto _nearest_vertex = _triangulation.nearest_vertex( _query_pt );
    return _nearest_vertex;
}


} // end namespace soccer

// Output vertex handle of triangulation
template < typename Kernel,
           typename TriangulationDataStructure >
inline
std::ostream& operator<<( std::ostream& os,
                          const typename CGAL::Triangulation_2<Kernel,TriangulationDataStructure>::Vertex_handle vh )
{
    // HACK Vertex must have id() defined
    os << vh->id() << std::endl;
    return os;
}

template < typename Id_,
           typename Info_,
           typename GT,
           typename Vb = CGAL::Triangulation_vertex_base_2<GT>
         >
std::ostream& operator<<( std::ostream& os,
                          const CGAL::internal::CC_iterator<
                                    CGAL::Compact_container<
                                        custom_cgal::Triangulation_vertex_base_with_id_and_info_2< Id_, Info_, GT, Vb >
                                                            >, false >& val )
{
    os << val->id();
    return os;
}

#endif // CGAL_TRIANGULATION_FORMATION_SAMPLE_STORAGE_HPP
