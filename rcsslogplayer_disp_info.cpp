#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "rcsslogplayer_disp_info.h"

#include <cassert>

namespace rcss {
namespace rcg {

ShowInfoTPlayerIterator::ShowInfoTPlayerIterator()
{

}

ShowInfoTPlayerIterator::ShowInfoTPlayerIterator( const size_t player_index = 0 )
    : M_player_index( player_index )
{

}

unsigned short ShowInfoTPlayerIterator::operator*() const
{
    return M_player_index;
}

void ShowInfoTPlayerIterator::operator++()
{
    M_player_index++;
}

void ShowInfoTPlayerIterator::operator--()
{
    M_player_index--;
}

ShowInfoTPlayerIterator ShowInfoTPlayerIterator::operator++(int)
{
    ShowInfoTPlayerIterator result(*this);   // make a copy for result
    ++(*this);              // Now use the prefix version to do the work
    return result;          // return the copy (the old) value.
}

ShowInfoTPlayerIterator ShowInfoTPlayerIterator::operator--(int)
{
    ShowInfoTPlayerIterator result(*this);   // make a copy for result
    --(*this);              // Now use the prefix version to do the work
    return result;          // return the copy (the old) value.
}

bool ShowInfoTPlayerIterator::operator==( const ShowInfoTPlayerIterator& other ) const
{
    return M_player_index == other.M_player_index;
}

bool ShowInfoTPlayerIterator::operator!=( const ShowInfoTPlayerIterator& other ) const
{
    return !( *this == other );
}


} // end namespace rcg
} // end namespace rcss

namespace soccer {

/// DispInfoT

const rcss::rcg::BallT& get_ball( const rcss::rcg::DispInfoT& state )
{
    return state.show_.ball_;
}

std::pair< rcss::rcg::player_iterator_type, rcss::rcg::player_iterator_type >
get_players( const rcss::rcg::DispInfoT&  )
{
    return rcss::rcg::get_players();
}

const rcss::rcg::PlayerT& get_player( const size_t player_id, const rcss::rcg::DispInfoT& state )
{
    assert( player_id < rcss::rcg::MAX_PLAYER * 2 );
    return state.show_.player_[player_id];
}

int get_time( const rcss::rcg::DispInfoT& state )
{
    return state.show_.time_;
}

const rcss::rcg::TeamT& get_left_team( const rcss::rcg::DispInfoT& state )
{
    return state.team_[0];
}

const rcss::rcg::TeamT& get_right_team( const rcss::rcg::DispInfoT& state )
{
    return state.team_[1];
}

void set_left_team( const rcss::rcg::TeamT& team, rcss::rcg::DispInfoT& state )
{
    state.team_[0] = team;
}

void set_right_team( const rcss::rcg::TeamT& team, rcss::rcg::DispInfoT& state )
{
    state.team_[1] = team;
}

void set_ball( rcss::rcg::BallT ball, rcss::rcg::DispInfoT& state )
{
    state.show_.ball_ = ball;
}

void set_player( int pid, rcss::rcg::PlayerT player, rcss::rcg::DispInfoT& state )
{
    state.show_.player_[pid] = player;
}

bool is_game_stopped( const rcss::rcg::DispInfoT& state )
{
    return state.pmode_ != rcss::rcg::PM_PlayOn;
}

bool is_setplay_mode( const rcss::rcg::DispInfoT& state )
{
    return state.pmode_ == rcss::rcg::PM_GoalKick_Left ||
           state.pmode_ == rcss::rcg::PM_GoalKick_Right ||
           state.pmode_ == rcss::rcg::PM_FreeKick_Left ||
           state.pmode_ == rcss::rcg::PM_FreeKick_Right||
           state.pmode_ == rcss::rcg::PM_IndFreeKick_Left ||
           state.pmode_ == rcss::rcg::PM_IndFreeKick_Right ||
           state.pmode_ == rcss::rcg::PM_CornerKick_Left ||
           state.pmode_ == rcss::rcg::PM_CornerKick_Right ||
           state.pmode_ == rcss::rcg::PM_KickOff_Left ||
           state.pmode_ == rcss::rcg::PM_KickOff_Right ||
           state.pmode_ == rcss::rcg::PM_KickIn_Left ||
           state.pmode_ == rcss::rcg::PM_KickIn_Right;
}



/// BallT
rcsc::Vector2D get_velocity( const rcss::rcg::BallT& ball)
{
    return rcsc::Vector2D( ball.vx_, ball.vy_ );
}

float get_vx( const rcss::rcg::BallT& ball )
{
    return ball.vx_;
}

float get_vy( const rcss::rcg::BallT& ball )
{
    return ball.vy_;
}

rcsc::Vector2D get_position( const rcss::rcg::BallT& ball)
{
    return rcsc::Vector2D( ball.x_, ball.y_ );
}

float get_x( const rcss::rcg::BallT& ball )
{
    return ball.x_;
}

float get_y( const rcss::rcg::BallT& ball )
{
    return ball.y_;
}


double get_speed( const rcss::rcg::BallT& ball )
{
    return std::sqrt( std::pow( ball.vx_, 2.0 ) + std::pow( ball.vy_, 2.0 ) );
}

void set_position( rcss::rcg::BallT& ball, const rcsc::Vector2D& pos)
{
    ball.x_ = pos.x;
    ball.y_ = pos.y;
}

void set_x( const float x, rcss::rcg::BallT& ball )
{
    ball.x_ = x;
}

void set_y( const float y, rcss::rcg::BallT& ball )
{
    ball.y_ = y;
}

void set_velocity( rcss::rcg::BallT& ball, const rcsc::Vector2D& vel)
{
    ball.vx_ = vel.x;
    ball.vy_ = vel.y;
}

void set_vx( const float vx, rcss::rcg::BallT& ball )
{
    ball.vx_ = vx;
}

void set_vy( const float vy, rcss::rcg::BallT& ball )
{
    ball.vy_ = vy;
}

/// PlayerT
int
get_number( const rcss::rcg::PlayerT& player )
{
    return player.unum_;
}

rcss::rcg::Side
get_side( const rcss::rcg::PlayerT& player )
{
    return player.side();
}

rcsc::Vector2D get_velocity( const rcss::rcg::PlayerT& player)
{
    return rcsc::Vector2D( player.vx_, player.vy_ );
}

float get_vx( const rcss::rcg::PlayerT& player )
{
    return player.vx_;
}

float get_vy( const rcss::rcg::PlayerT& player )
{
    return player.vy_;
}

rcsc::Vector2D get_position( const rcss::rcg::PlayerT& player)
{
    return rcsc::Vector2D( player.x_, player.y_ );
}

float get_x( const rcss::rcg::PlayerT& player )
{
    return player.x_;
}

float get_y( const rcss::rcg::PlayerT& player )
{
    return player.y_;
}

double get_speed( const rcss::rcg::PlayerT& player)
{
    return std::sqrt( std::pow( player.vx_, 2.0 ) + std::pow( player.vy_, 2.0 ) );
}

void set_position( rcss::rcg::PlayerT& player, const rcsc::Vector2D& pos)
{
    player.x_ = pos.x;
    player.y_ = pos.y;
}

void set_x( const float x, rcss::rcg::PlayerT& player )
{
    player.x_ = x;
}

void set_y( const float y, rcss::rcg::PlayerT& player )
{
    player.y_ = y;
}

void set_velocity( rcss::rcg::PlayerT& player, const rcsc::Vector2D& vel )
{
    player.vx_ = vel.x;
    player.vy_ = vel.y;
}

void set_vx( const float vx, rcss::rcg::PlayerT& player )
{
    player.vx_ = vx;
}

void set_vy( const float vy, rcss::rcg::PlayerT& player )
{
    player.vy_ = vy;
}

rcss::rcg::Side opposite_side( const rcss::rcg::Side side )
{
    assert( side != rcss::rcg::NEUTRAL );

    return side == rcss::rcg::LEFT
            ? rcss::rcg::RIGHT
            : rcss::rcg::LEFT;
}

}
