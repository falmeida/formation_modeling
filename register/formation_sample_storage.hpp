#ifndef SOCCER_FORMATION_SAMPLE_STORAGE_REGISTER_HPP
#define SOCCER_FORMATION_SAMPLE_STORAGE_REGISTER_HPP

#include <soccer/register/object.hpp>

#ifndef DOXYGEN_NO_SPECIALIZATIONS

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_TAG_TRAITS(FormationSampleStorage) \
    SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(FormationSampleStorage, soccer::formation_sample_storage_tag)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_TRAITS(FormationSampleStorage, FormationSample) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(formation_sample_type, FormationSampleStorage, FormationSample)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ID_TRAITS(FormationSampleStorage, FormationSampleId) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(formation_sample_id_type, FormationSampleStorage, FormationSampleId)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ITERATOR_TRAITS(FormationSampleStorage, FormationSampleIterator) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(formation_sample_iterator_type, FormationSampleStorage, FormationSampleIterator)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_TRAITS(FormationSampleStorage, FormationSample, \
                                                                 FormationSampleId, FormationSampleIterator) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_TAG_TRAITS(FormationSampleStorage) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ID_TRAITS(FormationSampleStorage, FormationSampleId) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_TRAITS(FormationSampleStorage, FormationSample) \
        SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ITERATOR_TRAITS(FormationSampleStorage, FormationSampleIterator)

#endif // DOXYGEN_NO_SPECIALIZATIONS

//
// FORMATION SAMPLE TYPE TRAITS
//
#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ACCESS_METHOD_GET(FormationSampleStorage, FormationSampleId, FormationSample, Get) \
        SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_WITH_KEY(formation_sample_access, FormationSampleStorage, FormationSampleId, FormationSample, Get)

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ACCESS_FREE_GET(FormationSampleStorage, FormationSampleId, FormationSample, Get) \
        SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_WITH_KEY(formation_sample_access, FormationSampleStorage, FormationSampleId, FormationSample, Get)

//
// FORMATION SAMPLE ID ITERATOR TYPE TRAITS
//
#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ITERATORS_ACCESS_METHOD_GET(FormationSampleStorage, FormationSampleIterator, Get) \
    template<> struct formation_sample_iterators_access<FormationSampleStorage> \
    { \
        static inline std::pair< FormationSampleIterator, FormationSampleIterator > \
        get(FormationSampleStorage const& obj) { return  obj.Get(); } \
    };

#define SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ITERATORS_ACCESS_FREE_GET(FormationSampleStorage, FormationSampleIterator, Get) \
    template<> struct formation_sample_iterators_access<FormationSampleStorage> \
    { \
        static inline std::pair< FormationSampleIterator, FormationSampleIterator > \
        get(FormationSampleStorage const& obj) { return Get( obj ); } \
    };

//
// FORMATION SAMPLE STORAGE REGISTRATION
//
#define SOCCER_REGISTER_FORMATION_SAMPLE_STORAGE_GET_SET_METHOD(FormationSampleStorage, \
                                                                FormationSample, GetSample, \
                                                                FormationSampleId, FormationSampleIterator, GetSamples) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_TRAITS(FormationSampleStorage, FormationSample, \
                                                             FormationSampleId, FormationSampleIterator) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ACCESS_METHOD_GET(FormationSampleStorage, FormationSampleId, FormationSample, GetSample ) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ITERATORS_ACCESS_METHOD_GET(FormationSampleStorage, FormationSampleIterator, GetSamples) \
    SOCCER_TRAITS_NAMESPACE_END

#define SOCCER_REGISTER_FORMATION_SAMPLE_STORAGE_GET_SET_FREE(FormationSampleStorage, \
                                                              FormationSample, GetSample, \
                                                              FormationSampleId, FormationSampleIterator, GetSamples) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_TRAITS(FormationSampleStorage, FormationSample, \
                                                             FormationSampleId, FormationSampleIterator) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ACCESS_FREE_GET(FormationSampleStorage, FormationSampleId, FormationSample, GetSample ) \
    SOCCER_DETAIL_SPECIALIZE_FORMATION_SAMPLE_STORAGE_SAMPLE_ITERATORS_ACCESS_FREE_GET(FormationSampleStorage, FormationSampleIterator, GetSamples) \
    SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_FORMATION_SAMPLE_STORAGE_REGISTER_HPP
