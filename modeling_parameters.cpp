#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "modeling_parameters.h"

using namespace boost;
namespace po = boost::program_options;

namespace soccer {
namespace modeling {

modeling_parameters::modeling_parameters()
    : M_options("Modeling Options")
{
    M_options.add_options()
        ("help,h", "produce help message")
        ("version,v", "display the current software version")
        ("team_name_regex,t", po::value<std::string>(&M_teamname_regex)->default_value(""),
            "relevant team regex used to determine relevant team side in log files")
        ("logfile_name_regex,l", po::value<std::string>(&M_logfile_name_regex)->default_value("\\.rcg(\\.gz)?$"),
            "logfile regex used to determine relevant log files to analyze (optional). defaults to \".rcg.gz\"")
        ("logfile_search_dir,I", po::value< std::vector< std::string > >(&M_logfile_search_paths),
            "base directories to search for logfiles")
        ("logfiles,i", po::value< std::vector< std::string > >( &M_logfiles_paths ),
            "set of absolute or relative paths to logfiles to use in the modeling (optional). Combined with other log files found during search.")
        ("recursive_lookup,r", po::value< bool >(&M_recursive_lookup)->default_value(true),
            "list of base directories to search for logfiles")
        ("output_formation_file,o", po::value< std::string >(&M_output_formation_file)->default_value("formation.conf"),
            "name of the output file to create for the modeled formation. defaults to formation.conf")
        ("input_formation_file,f", po::value< std::vector< std::string > >(&M_input_formation_paths),
            "name of input formation files to check similarity against.")
    ;

}

bool modeling_parameters::parse( const std::string& config_file_path )
{
    po::variables_map vm;
    po::store( po::parse_config_file<char>( config_file_path.c_str(), M_options ), vm);
    po::notify(vm);

    return check_parameters( vm );
}

bool modeling_parameters::parse(int argc, char **argv )
{
    po::variables_map vm;
    po::store( po::parse_command_line( argc, argv, M_options ), vm);
    po::notify(vm);
}

bool modeling_parameters::check_parameters( const boost::program_options::variables_map& vm )
{
    if ( vm.count("help") )
    {
        std::cout << M_options << std::endl;
        return false;
    }

    if ( vm.count("version") )
    {
        std::cout << "Version " << PACKAGE_VERSION << std::endl;
        return false;
    }

    if ( !vm.count("team_name_regex") )
    {
        std::cout << "Teamname regex must be specified" << std::endl;
        return false;
    }

    if ( !vm.count("logfile_search_dir") && !vm.count("logfiles") )
    {
        std::cout << "Must specify an option for \"logfile_search_dir\" or \"logfiles\"" << std::endl;
        return false;
    }

    return true;
}

const std::string& modeling_parameters::teamNameRegex() const
{
    return M_teamname_regex;
}

const std::string& modeling_parameters::outputFormationFile() const
{
    return M_output_formation_file;
}

const std::string& modeling_parameters::logfileNameRegex() const
{
    return M_logfile_name_regex;
}

const std::vector< std::string >& modeling_parameters::logFileSearchPaths() const
{
    return M_logfile_search_paths;
}

const std::vector< std::string>& modeling_parameters::logFilesPaths() const
{
    return M_logfiles_paths;
}

const std::vector< std::string>& modeling_parameters::inputFormationPaths() const
{
    return M_input_formation_paths;
}

bool modeling_parameters::recursiveLookup() const
{
    return M_recursive_lookup;
}

modeling_parameters::ModelingStrategy modeling_parameters::modelingStrategy() const
{
    return  M_modeling_strategy.compare("offensive") == 0
                ? Offensive
                : Defensive;
}


}
}


std::ostream& operator<<( std::ostream& os, const soccer::modeling::modeling_parameters& _modeling_params )
{
    os << "Output formation file: " << _modeling_params.outputFormationFile() << std::endl;

    os << "Teamname regex: " << _modeling_params.teamNameRegex() << std::endl;

    os << "Logfile regex: " << _modeling_params.logfileNameRegex() << std::endl;

    os << "Recursive lookup: " << (_modeling_params.recursiveLookup() ? "true" : "false") << std::endl;

    auto _print_string_container_fn = [&] ( const std::string& title,
                                              std::vector< std::string >::const_iterator _it_begin,
                                              std::vector< std::string >::const_iterator _it_end )
                                        {
                                                os << title << ": [";
                                                std::for_each( _it_begin, _it_end, [&] ( const std::string& s ) { os << s << " "; } );
                                                os << "]";
                                        };

    _print_string_container_fn( "Logfile search directories",
                                _modeling_params.logFileSearchPaths().begin(),
                                _modeling_params.logFileSearchPaths().end() );
    os << std::endl;

    _print_string_container_fn( "Specific logfiles to use",
                                _modeling_params.logFilesPaths().begin(),
                                _modeling_params.logFilesPaths().end() );
    os << std::endl;

    _print_string_container_fn( "Input formation file paths",
                                _modeling_params.inputFormationPaths().begin(),
                                _modeling_params.inputFormationPaths().end() );
    os << std::endl;

    return os;
}
