#ifndef SOCCER_FORMATION_SAMPLE_STORAGE_TAG_HPP
#define SOCCER_FORMATION_SAMPLE_STORAGE_TAG_HPP

#include <boost/mpl/assert.hpp>

namespace soccer {
namespace traits {


} // end namespace traits

template <typename FormationSampleStorage>
struct formation_sample_storage_tag
{
    BOOST_MPL_ASSERT_MSG
        (
            false,
            NOT_IMPLEMENTED_FOR_THIS_FORMATION_SAMPLE_STORAGE_TYPE, (types<FormationSampleStorage>)
        );
};

} // end namespace soccer

#endif // SOCCER_FORMATION_SAMPLE_STORAGE_TAG_HPP
