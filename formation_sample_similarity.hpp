#ifndef SOCCER_FORMATION_SAMPLE_SIMILARITY_HPP
#define SOCCER_FORMATION_SAMPLE_SIMILARITY_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "formation_sample_concept.hpp"

#include "sample_matching.h"

#include <soccer/core/access.hpp>
#include "Core/access.hpp"

#include <rcsc/formation/formation_dt.h>

#include <boost/property_map/property_map.hpp>
#include <boost/tuple/tuple.hpp>

#include <type_traits>

#include <cassert>

namespace soccer {

    template < typename FormationSample,
               typename PlayerIdMappingCostFunction,
               typename PlayerIdWritableMap >
    auto similarity( const ::rcsc::formation::SampleData& _sample1,
                     const FormationSample& _sample2,
                     PlayerIdMappingCostFunction _cost_fn,
                     PlayerIdWritableMap _player_pmap ) -> long
    {
        /* auto _cost_fn = [&] ( const int pid1, const sample_player_id_type pid2 )
                            {
                                const auto _diff_x = _player_positions.at(pid1).x - soccer::get_x( soccer::get_player( pid2, _sample ) );
                                const auto _diff_y = _player_positions.at(pid1).y - soccer::get_y( soccer::get_player( pid2, _sample ) );
                                const auto _player_dist2 = std::pow( _diff_x, 2 ) + std::pow( _diff_y, 2 );
                                const auto _player_dist = std::sqrt( _player_dist2 );
                                return _player_dist;
                            };
        auto _sample_matching_cost = soccer::modeling::inter_state_player_matching( _rcsc_formation_player_ids, _rcsc_formation_player_ids + 11,
                                                                                    _player_sample_begin, _player_sample_end,
                                                                                    _cost_fn,
                                                                                    _player_pmap ); */
    }


    template < typename FormationSampleStorage,
               // typename FormationSampleCostFunction,
               typename SampleMatchingCostMap >
    auto
    similarity( const ::rcsc::FormationDT& _formation,
                const FormationSampleStorage& _storage,
                // FormationSampleCostFunction _cost_fn,
                SampleMatchingCostMap _sample_cost_map
              ) -> long // typename std::iterator_traits< typename decltype(soccer::get_samples(FormationSampleStorage{}))::first_type >::value_type
    {
        typedef typename formation_sample_iterator_type< FormationSampleStorage >::type formation_sample_iterator_t;
        typedef typename formation_sample_id_type< FormationSampleStorage >::type formation_sample_id_t;
        typedef typename formation_sample_type< FormationSampleStorage >::type formation_sample_t;
        typedef typename player_iterator_type< formation_sample_t >::type formation_sample_player_iterator_t;
        typedef typename player_id_type< formation_sample_t >::type formation_sample_player_id_t;

        typedef long /* decltype( FormationSampleCostFunction( int{}, sample_player_id_type{} ) )*/ score_type;

        typedef std::pair< typename soccer::formation_sample_id_type< ::rcsc::formation::SampleDataSet >::type,
                           formation_sample_id_t
                        > formation_sample_pair_type;
        typedef std::map< typename player_id_type< ::rcsc::formation::SampleData >::type ,
                          formation_sample_player_id_t
                        > player_id_map_type;
        BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< SampleMatchingCostMap, formation_sample_pair_type > ));
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< SampleMatchingCostMap >::value_type,
                                                  score_type >::value ),
                                  "SampleMatchingCostMap value type mismatch" );

        formation_sample_iterator_t _begin_sample, _end_sample;
        boost::tie( _begin_sample, _end_sample ) = get_samples( _storage );


        // rcsc::Formation player_iterators
        static const int _rcsc_formation_player_ids[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        auto _total_cost = 0.0;
        for ( auto its = _begin_sample; its != _end_sample ; its++ )
        {
            const auto& _sample = get_sample( *its, _storage );

            // Calculate best matching between players using a cost function
            std::vector< ::rcsc::Vector2D > _player_positions;
            const auto _ball = get_ball( _sample );
            const auto _ball_position = get_position( _ball );
            ::rcsc::Vector2D _focal_point( get_x( _ball_position ), get_y( _ball_position ) );
            _formation.getPositions( _focal_point, _player_positions );

            // HACK No method available to find the nearest sample in rcsc::Formation.
            // Using internal data (triangulation vertex id). Assuming sample_id = Triangulation.vertex_id
            auto _rcsc_formation_nearest_vertex = _formation.triangulation().findNearestVertex( _focal_point );
            assert( _rcsc_formation_nearest_vertex );

            player_id_map_type _player_map;
            boost::associative_property_map< player_id_map_type > _player_pmap( _player_map );
            formation_sample_player_iterator_t _player_sample_begin, _player_sample_end;
            boost::tie( _player_sample_begin, _player_sample_end ) = get_players( _sample );

            auto _cost_fn = [&] ( const int pid1, const formation_sample_player_id_t pid2 )
                                {
                                    const auto _p2 = get_player( pid2, _sample );
                                    auto _p2_position = soccer::get_position( _p2 );
                                    const auto _diff_x = _player_positions.at(pid1).x - get_x( _p2_position );
                                    const auto _diff_y = _player_positions.at(pid1).y - get_y( _p2_position );
                                    const auto _player_dist2 = std::pow( _diff_x, 2 ) + std::pow( _diff_y, 2 );
                                    const auto _player_dist = std::sqrt( _player_dist2 );
                                    return _player_dist;
                                };
            auto _sample_matching_cost = soccer::modeling::formation_sample_matching( _rcsc_formation_player_ids, _rcsc_formation_player_ids + 11,
                                                                                        _player_sample_begin, _player_sample_end,
                                                                                        _cost_fn,
                                                                                        _player_pmap );
            boost::put( _sample_cost_map,
                        std::make_pair( _rcsc_formation_nearest_vertex->id(), *its ),
                        _sample_matching_cost );

            std::cout << "Sample_ids(" << _rcsc_formation_nearest_vertex->id() << "," << (*its)->id() << ")"
                      << " ball" << _focal_point
                      << " matchings(cost=" << _sample_matching_cost << ")=[";
            std::for_each( _player_map.begin(),
                           _player_map.end(),
                           [&] ( const typename player_id_map_type::value_type& kvp )
                                { std::cout << "(" << kvp.first << "," << kvp.second << ")"; } );
            std::cout << "]" << std::endl;

            _total_cost += _sample_matching_cost;
        }

        return _total_cost;
    }

} // end namespace soccer

#endif // SOCCER_FORMATION_SAMPLE_SIMILARITY_HPP
