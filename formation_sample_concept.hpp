#ifndef SOCCER_FORMATION_SAMPLE_CONCEPT_HPP
#define SOCCER_FORMATION_SAMPLE_CONCEPT_HPP

#include "formation_sample_traits.hpp"

#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(FormationSampleConcept,(FS))
{
    // typedef formation_sample_traits< FS > formation_sample_traits_t;
    typedef typename player_id_type< FS >::type player_id_type;
    typedef typename player_type< FS >::type player_type;
    typedef typename ball_type< FS >::type ball_type;

    BOOST_CONCEPT_USAGE(FormationSampleConcept)
    {
        const_constraints( fs );
    }

private:
    void const_constraints( const FS& const_fs )
    {
        get_ball( const_fs );
        get_players( const_fs );
        get_player( _pid, const_fs );
    }

protected:
    FS fs;
    ball_type _ball;
    player_id_type _pid;
    player_type _player;
};


BOOST_concept(MutableFormationSampleConcept,(FS))
    : public FormationSampleConcept<FS>
{
     // typedef formation_sample_traits< FS > formation_sample_traits_t;
    typedef typename player_id_type< FS >::type player_id_type;
    typedef typename player_type< FS >::type player_type;
    typedef typename ball_type< FS >::type ball_type;

    BOOST_CONCEPT_USAGE(MutableFormationSampleConcept)
    {
        set_player( FormationSampleConcept<FS>::_pid,
                    FormationSampleConcept<FS>::_player,
                    FormationSampleConcept<FS>::fs );
        set_ball( FormationSampleConcept<FS>::_ball,
                  FormationSampleConcept<FS>::fs );
    }
};


} // end namespace soccer

#endif // SOCCER_FORMATION_SAMPLE_CONCEPT_HPP
