#ifndef RCSC_FORMATION_SAMPLE_DATASET_ADAPTOR_HPP
#define RCSC_FORMATION_SAMPLE_DATASET_ADAPTOR_HPP

#include <rcsc/formation/sample_data.h>

#include "formation_sample.hpp"

#include <soccer/object.hpp>
#include <soccer/geometry.hpp>

#include <boost/iterator/transform_iterator.hpp>
#include <boost/iterator/counting_iterator.hpp>

#include <utility>
#include <functional>

namespace rcsc {
namespace formation {

// HACK Should not need to add this here!!! Bad coding in SampleDataSet::addData => Checks for existNearData
static const double RCSC_FORMATION_SAMPLE_MAX_DIST2_THR = rcsc::formation::SampleDataSet::NEAR_DIST_THR * rcsc::formation::SampleDataSet::NEAR_DIST_THR;

}}

namespace soccer {

//
// rcsc::formation::SampleDataSet as model of FormationSampleStorage
//
std::pair< boost::transform_iterator< std::function< int(const rcsc::formation::SampleData&) >,
                                      rcsc::formation::SampleDataSet::DataCont::const_iterator >,
           boost::transform_iterator< std::function< int(const rcsc::formation::SampleData&) >,
                                      rcsc::formation::SampleDataSet::DataCont::const_iterator >
         >
get_samples( const rcsc::formation::SampleDataSet& _sample_dataset )
{
    typedef boost::transform_iterator< std::function< int(const rcsc::formation::SampleData&) >,
                                       rcsc::formation::SampleDataSet::DataCont::const_iterator
                                     > iterator_type;
    auto _get_sample_id_fn = [] ( const rcsc::formation::SampleData& sample ) -> int
        {
            typedef size_t result_type;
            return sample.index_;
        };
    return std::make_pair( iterator_type( _sample_dataset.dataCont().begin(), _get_sample_id_fn ),
                           iterator_type( _sample_dataset.dataCont().end(), _get_sample_id_fn ) );
}

const rcsc::formation::SampleData&
get_sample( int sample_id, const rcsc::formation::SampleDataSet& _sample_dataset )
{
    auto _sample_ptr = _sample_dataset.data( sample_id );
    assert( _sample_ptr != NULL );

    return *_sample_ptr;
}

rcsc::formation::SampleData&
get_sample( int sample_id, rcsc::formation::SampleDataSet& _sample_dataset )
{
    // HACK removing constness due to unavailable method to sample data!!
    // const SampleData * data( const size_t idx ) const;
    auto _sample_ptr = const_cast< rcsc::formation::SampleData* >( _sample_dataset.data( sample_id ) );
    assert( _sample_ptr != NULL );

    return *_sample_ptr;
}

}

namespace rcsc {
namespace formation {
    typedef soccer::static_object< rcsc::Vector2D > ball_type;
    typedef soccer::static_object< rcsc::Vector2D > player_type;
    typedef std::size_t player_id_type;
    typedef boost::counting_iterator< std::size_t > player_iterator_type;

    ball_type get_ball( const SampleData& sample ) { return ball_type( sample.ball_ ); }
    void set_ball( ball_type const& ball, SampleData& sample ) { sample.ball_ = ball.M_position; }

    player_type get_player( player_id_type const pid, const SampleData& sample ) { return player_type( sample.players_.at( pid ) );}
    void set_player( player_id_type const pid, player_type const& player, SampleData& sample ) { sample.players_[pid] = player.M_position; }

    std::pair< player_iterator_type, player_iterator_type >
    get_players( const SampleData& )
    {
        return std::make_pair( player_iterator_type( 0u ), player_iterator_type( 11u ) );
    }

    // HACK to prevent error in macro usage!! Bad coding => Make macro usage for modular
    void* get_info( const SampleData& ) { assert( false ); return NULL; }
    void set_info( void* , SampleData& ) { assert( false ); }
}
}

SOCCER_REGISTER_FORMATION_SAMPLE_GET_SET_FREE( rcsc::formation::SampleData, \
                                               rcsc::formation::ball_type, rcsc::formation::get_ball, rcsc::formation::set_ball, \
                                               rcsc::formation::player_id_type, rcsc::formation::player_iterator_type, rcsc::formation::get_players, \
                                               rcsc::formation::player_type, rcsc::formation::get_player, rcsc::formation::set_player, \
                                               void*, rcsc::formation::get_info, rcsc::formation::set_info)

#endif // RCSC_FORMATION_SAMPLE_DATASET_ADAPTOR_HPP
